<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" session="false"%>
<%@ taglib prefix="str" uri="http://stripes.sourceforge.net/stripes.tld"%>
<str:useActionBean id="ass" beanclass="lv.nmc.webs.cat.assessActionBean" />

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>CAT v2.0 - Start test</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">


        <link href="../../pub/css/bootstrap.css" rel="stylesheet">
        <link href="../../pub/css/main.css" rel="stylesheet">

        <style type="text/css">
            body {
                padding-top: 60px;
                padding-bottom: 40px;
            }
        </style>
        <link href="../../pub/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="./js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        
    </head>

    <body>

   

        <div class="container">

            <!-- Main hero unit for a primary marketing message or call to action -->
            <div class="hero-unit" style='height: 110px; font-family: Arial'>
                <h3 style="font-family: Arial">Test commencing!</h3>

                <p style="font-family: Arial">Your test includes <b>${ass.testDetails.questionCount}</b> questions in <b>${ass.structCount}</b> categories. <br />
                    ${ass.testDetails.practCount} practical task(s) assigned <br />
                    ${ass.testDetails.simCount} simulator task(s) assigned <br />



                    <span id="redE">  Click "Begin test" when you are ready!</span></p>

              <!--  <p><a href="../display-${ass.ticket}/${ass.email}-0.jht" class="btn btn-primary btn-large">Begin test &raquo;</a></p> -->
            </div>

            <!-- Example row of columns -->
            <div class="row">
                
                <c:if test="${!empty ass.providedFunctions}">
                <div class="span4">               
                    
                    
                    <h3>Functions</h3>

                    <ul>
                        <c:forEach items="${ass.providedFunctions}" var="fns" varStatus="state">
                            <li>${fns.functionName}</li>
                        </c:forEach>
                       
                    </ul>


                </div>
                </c:if>
                
                <c:if test="${!empty ass.providedCompetences}">
                <div class="span4">

                    <h3>Competences</h3>

                    <ul>
                        <c:forEach items="${ass.providedCompetences}" var="fns" varStatus="state">
                            <li>${fns.competenceName}</li>

                        </c:forEach>
                    </ul>

                </div>
                </c:if>
                <c:if test="${!empty ass.providedSubjects}">
                <div class="span4">
                    <h3>Subjects</h3>
                    <ul>
                        <c:forEach items="${ass.providedSubjects}" var="fns" varStatus="state">
                            <li>${fns.subjectName}</li>
                        </c:forEach>
                    </ul>
                </div>
                </c:if>
                <c:if test="${!empty ass.providedStcw}">
 <div class="span4">
                    <h3>STCW</h3>
                    <ul>
                        <c:forEach items="${ass.providedStcw}" var="fns" varStatus="state">
                            <li>${fns.stcwName}</li>
                        </c:forEach>
                    </ul>
                </div>
                </c:if>

            </div>




        </div> <!-- /container -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="../../../pub/js/jquery.js"></script>
        <script src="../../../pub/js/bootstrap-transition.js"></script>
        <script src="../../../pub/js/bootstrap-alert.js"></script>
        <script src="../../../pub/js/bootstrap-modal.js"></script>
        <script src="../../../pub/js/bootstrap-dropdown.js"></script>
        <script src="../../../pub/js/bootstrap-scrollspy.js"></script>
        <script src="../../../pub/js/bootstrap-tab.js"></script>
        <script src="../../../pub/js/bootstrap-tooltip.js"></script>
        <script src="../../../pub/js/bootstrap-popover.js"></script>
        <script src="../../../pub/js/bootstrap-button.js"></script>
        <script src="../../../pub/js/bootstrap-collapse.js"></script>
        <script src="../../../pub/js/bootstrap-carousel.js"></script>
        <script src="../../../pub/js/bootstrap-typeahead.js"></script>

    </body>
</html>
