<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" session="false"%>
<%@ taglib prefix="str" uri="http://stripes.sourceforge.net/stripes.tld"%>
<%@ taglib prefix="js" uri="/WEB-INF/tlds/javascript.tld"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<str:useActionBean id="rep" beanclass="lv.nmc.webs.cat.reportActionBean" />
<c:set var="fpres" value="Not assigned" />
<c:set var="cpres" value="Not assigned" />
<c:set var="spres" value="Not assigned" />
<c:set var="stpres" value="Not assigned" />
<c:set var="ppres" value="Not assigned" />
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>CAT Report</title>    
    </head>
    <style type="text/css">
        body {
            margin-top: 0px;
            margin-left: 0px;
        }

        td.logos {
            vertical-align: top;
            background-image: url(../img/ship.png);
            background-size: 800px 120px;
            background-repeat: no-repeat;
            height: 125px;
            font-size: 28px;   
            line-height: 150%;
            padding-top: 15px;
        }
        
        td {
            border: 1px solid silver;
        }
    </style>
    <body>
        <table width="800px" border="1" align="center" style="border-collapse: collapse;">
            <tr>
                             
                <td class="logos" colspan="3">
                    <str:link beanclass="lv.nmc.webs.cat.reportActionBean" event="pract">
                        <str:param name="email" value="${rep.email}" />
                        <str:param name="ticketString"  value="${rep.ticketString}" />
                        <img src="../img/logo.png" alt="" width="64" style="border: 0px"/>
                    </str:link>
                    CAT tests report</td>  
               
            </tr>
             <tr>
                <td style="vertical-align: top;"><h3 align="left" style="color: silver">Trainee: </h3></td>               
                <td style="vertical-align: top;" colspan="2">
                    Name Surname: ${rep.client.nameSurname} <br />
                    Person code: ${rep.client.personCode} <br />
                    E-mail: ${rep.client.email}
                </td>                    
                
            </tr>
            <tr>
                <td style="vertical-align: top;"><h3 align="left">Functions: </h3></td>               
                <td style="vertical-align: top;" colspan="2">
                    <ul>
                        
                        
                        <c:forEach items="${rep.structs}" var="struct" varStatus="state">
                            
                            <c:if test="${struct.functionId ne null}">
                                 <c:set var="fpres" value="" />
                                <li>${struct.functionName}</li>
                            </c:if>
                            
                        </c:forEach>
                        <c:out value="${pageScope.fpres}" />
                    </ul>
                </td>                    
                
            </tr>

            <tr>
                <td style="vertical-align: top;"><h3 align="left">Competences:</h3></td>               
                <td style="vertical-align: top;" colspan="2">
                    <ul>
                        
                        <c:forEach items="${rep.structs}" var="struct" varStatus="state">
                            
                            <c:if test="${struct.competenceId ne null}">
                                <c:set var="cpres" value="" />
                                <li>${struct.competenceName}</li>
                            </c:if>
                            
                        </c:forEach>
                        <c:out value="${pageScope.cpres}" />
                        
                    </ul>
                </td>                    
               
            </tr>
            <tr>
                <td style="vertical-align: top;"><h3 align="left">Subjects:</h3></td>               
                <td style="vertical-align: top;" colspan="2">
                     <ul>
                        
                        <c:forEach items="${rep.structs}" var="struct" varStatus="state">
                            
                            <c:if test="${struct.subjectId ne null}">
                                <c:set var="fpres" value="" />
                                <li>${struct.subjectName}</li>
                            </c:if>
                            
                        </c:forEach>
                        <c:out value="${pageScope.spres}" />
                        
                    </ul>
                </td>                    
               
            </tr>
            
            <tr>
                <td style="vertical-align: top;"><h3 align="left">STCW</h3></td>               
                <td style="vertical-align: top;" colspan="2">
                     <ul>
                        
                        <c:forEach items="${rep.structs}" var="struct" varStatus="state">
                            
                            <c:if test="${struct.stcwId ne null}">
                                <c:set var="stpres" value="" />
                                <li>${struct.stcwName}</li>
                            </c:if>
                            
                        </c:forEach>
                        <c:out value="${pageScope.stpres}" />
                        
                    </ul>
                </td>                    
               
            </tr>
            
            <tr>
                <td style="vertical-align: top;"><h3 align="left">CUSTOM POOL</h3></td>               
                <td style="vertical-align: top;" colspan="2">
                     <ul>
                        
                        <c:forEach items="${rep.structs}" var="struct" varStatus="state">
                            
                            <c:if test="${struct.poolId ne null}">
                               <c:set var="ppres" value="" />
                                <li>${struct.poolName}</li>
                            </c:if>
                            
                        </c:forEach>
                        <c:out value="${pageScope.ppres}" />
                        
                    </ul>
                </td>                    
               
            </tr>
            
            <tr>
                <td colspan="3" style="vertical-align: top; font-weight: bold">
                    Question count: <span style="color: red;">${rep.qSize}</span>
                </td>
            </tr>
            
             <tr>
                <td colspan="3" style="vertical-align: top; font-weight: bold">
                    Right answers: <span style="color: red;">${rep.rightAnswers}</span> (<fmt:formatNumber maxFractionDigits="0" value="${rep.rightAnswers*100/rep.qSize}" />%)
                </td>
            </tr>
            
            

        </table>
    </body>
</html>
