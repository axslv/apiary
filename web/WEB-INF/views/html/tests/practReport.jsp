<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" session="false"%>
<%@ taglib prefix="str" uri="http://stripes.sourceforge.net/stripes.tld"%>
<%@ taglib prefix="js" uri="/WEB-INF/tlds/javascript.tld"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<str:useActionBean id="rep" beanclass="lv.nmc.webs.cat.reportActionBean" />

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>CAT Report</title>    
    </head>
    <style type="text/css">
        body {
            margin-top: 0px;
            margin-left: 0px;
        }

        td.logos {
            vertical-align: top;
            background-image: url(../img/ship.png);
            background-size: 800px 120px;
            background-repeat: no-repeat;
            height: 125px;
            font-size: 28px;   
            line-height: 150%;
            padding-top: 15px;
        }

        td {
            border: 1px solid silver;
        }
    </style>
    <body>
        <table width="800px" border="1" align="center" style="border-collapse: collapse;">
            <tr>

                <td class="logos" colspan="3">
                    <str:link beanclass="lv.nmc.webs.cat.reportActionBean" event="tests">
                        <str:param name="email" value="${rep.email}" />
                        <str:param name="ticketString"  value="${rep.ticketString}" />
                        <img src="../img/logo.png" alt="" width="64" style="border: 0px"/>
                    </str:link>
                    CAT Practical tasks report</td>  

            </tr>
            <tr>
                <td style="vertical-align: top;"><h3 align="left" style="color: silver">Trainee: </h3></td>               
                <td style="vertical-align: top;" colspan="2">
                    Name Surname: ${rep.client.nameSurname} <br />
                    Person code: ${rep.client.personCode} <br />
                    E-mail: ${rep.client.email}
                </td>                    

            </tr>

            <c:forEach items="${rep.tasksFromMap}" var="skmap"> 

                <c:set var="impMark" value="0" />
                <c:set var="defMark" value="0" />
                <c:set var="minMark" value="0" />

                <tr>
                    <td style="vertical-align: middle;"><h3 align="left">TASK ID: ${skmap}</h3></td>               
                    <td style="vertical-align: middle;" colspan="2">
                        Task title: ${rep.skillMap[skmap][0].taskTitle}
                    </td>                    

                </tr>
                <tr>
                    <td style="vertical-align: middle;"></td> 
                    <td style="vertical-align: middle;"></td> 


                </tr>
                <tr>
                    <td style="vertical-align: middle;"><h3 align="left">Instructor's comment: </h3></td> 
                    <td style="vertical-align: top;">${rep.commentMap[skmap]}</td> 


                </tr>
                <tr>
                    <td colspan="2" style="border-bottom: 2px solid black">
                        <table style="border-collapse: collapse" width="95%" align="center">
                            <tr>
                                <th>Objective</th>
                                <th>Importance</th>
                                <th>Mark</th>
                            </tr>

                            <c:forEach items="${rep.skillMap[skmap]}" var="task">
                                <c:set var="impMark" value="${pageScope.impMark + task.importance*task.mark}" />
                                <c:set var="defMark" value="${pageScope.defMark + task.importance*3}" />
                                <c:set var="minMark" value="${pageScope.minMark + task.importance}" />
                                <tr>
                                    <td><c:out value="${task.skillTitle}" /></td>
                                    <td><c:out value="${task.importance}" /></td> 
                                    <td><c:out value="${task.mark}" /></td>                            
                                </tr>
                            </c:forEach>
                            <tr>
                                <td colspan="3">
                                    <ul>
                                        <li>User totals = <b>${pageScope.impMark}</b> (<span style="color: red; font-weight: bold;"><fmt:formatNumber value="${pageScope.impMark*100/pageScope.defMark}" maxFractionDigits="1" />%</span>)</li>
                                        <li>Best result: <b>${pageScope.defMark}</b> (<span style="color: red; font-weight: bold;">100%</span>)</li>
                                        <li>Worst case: <b>${pageScope.minMark}</b> (<span style="color: red; font-weight: bold;"><fmt:formatNumber value="${pageScope.minMark*100/pageScope.defMark}" maxFractionDigits="1" />%</span>)</li>
                                    </ul>
                                </td>

                            </tr>
                        </table>
                        <br />

                    </td>
                </tr>


            </c:forEach>



        </table>
    </body>
</html>
