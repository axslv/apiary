<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" session="false"%>
<%@ taglib prefix="str" uri="http://stripes.sourceforge.net/stripes.tld"%>
<%@ taglib prefix="js" uri="/WEB-INF/tlds/javascript.tld"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<str:useActionBean id="ts" beanclass="lv.nmc.webs.cat.assessActionBean" />


<!DOCTYPE HTML>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>CAT v2.0 - Tasks</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>


        <!-- Le styles -->
        <link href="../../../pub/css/bootstrap.css" rel="stylesheet"/>
        <style type="text/css">
            body {
                padding-top: 60px;
                padding-bottom: 40px;
            }
        </style>


        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="./js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="./ico/apple-touch-icon-144-precomposed.png"/>
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="./ico/apple-touch-icon-114-precomposed.png"/>
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="./ico/apple-touch-icon-72-precomposed.png"/>
        <link rel="apple-touch-icon-precomposed" href="./ico/apple-touch-icon-57-precomposed.png"/>
        <link rel="shortcut icon" href="./ico/favicon.png"/>
       
        <script src="../pub/js/jquery.js"></script>
    </head>

    <body>

        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="brand" id ="title" href="#">Test ended!</a>
                    <div class="nav-collapse collapse">

                        <form class="navbar-form pull-right">    

                          
                        </form>
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>

        <div class="container">

            <!-- Main hero unit for a primary marketing message or call to action -->
            <div class="hero-unit">
                <h2>Thank you!</h2>

                <p>Your test has ended. We will evaluate your results soon.</p>
                
                

            </div>



            <!-- Example row of columns -->
            <div class="row">

                <c:forEach items="${ts.answers}" var="answers" varStatus="state">
                    <div class="span4">
                        <h2><img src="../pub/img/check.png" alt="" id="${ts.alet[state.index]}" style="display: none" /> Answer ${ts.alet[state.index]}</h2>

                        <p><c:out value="${answers.answerTitle}" /> </p><p><a class="btn" onclick="selectAnswer('${ts.alet[state.index]}');
                return false" href="#">Select </a></p>


                    </div>
                </c:forEach>


            </div>
            <hr /> 



            <footer>
                <p>&copy; Novikontas 2013</p>
            </footer>

        </div> <!-- /container -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <script src="../pub/js/bootstrap-transition.js"></script>
        <script src="../pub/js/bootstrap-alert.js"></script>
        <script src="../pub/js/bootstrap-modal.js"></script>
        <script src="../pub/js/bootstrap-dropdown.js"></script>
        <script src="../pub/js/bootstrap-scrollspy.js"></script>
        <script src="../pub/js/bootstrap-tab.js"></script>
        <script src="../pub/js/bootstrap-tooltip.js"></script>
        <script src="../pub/js/bootstrap-popover.js"></script>
        <script src="../pub/js/bootstrap-button.js"></script>
        <script src="../pub/js/bootstrap-collapse.js"></script>
        <script src="../pub/js/bootstrap-carousel.js"></script>
        <script src="../pub/js/bootstrap-typeahead.js"></script>

    </body>
</html>
