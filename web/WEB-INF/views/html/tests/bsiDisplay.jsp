<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" session="false"%>
<%@ taglib prefix="str" uri="http://stripes.sourceforge.net/stripes.tld"%>
<%@ taglib prefix="js" uri="/WEB-INF/tlds/javascript.tld"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<str:useActionBean id="ts" beanclass="lv.nmc.webs.cat.assessActionBean" />


<!DOCTYPE HTML>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>CAT v2.0 - Tasks</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>


        <!-- Le styles -->
        <link href="../../pub/css/bootstrap.css" rel="stylesheet"/>
        

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="./js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="./ico/apple-touch-icon-144-precomposed.png"/>
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="./ico/apple-touch-icon-114-precomposed.png"/>
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="./ico/apple-touch-icon-72-precomposed.png"/>
        <link rel="apple-touch-icon-precomposed" href="./ico/apple-touch-icon-57-precomposed.png"/>
        <link rel="shortcut icon" href="./ico/favicon.png"/>
      
        <script src="../pub/js/jquery.js"></script>
        <style type="text/css">
            body {
                padding-left: 5px;
                padding-right: 6px;
            }
        </style>
    </head>

    <body>

        <div class="navbar navbar-inverse navbar-fixed-top">
          
        </div>


        <div class="container">

            <!-- Main hero unit for a primary marketing message or call to action -->
            <div class="hero-unit" style="font-family: Arial">
                <h2 style="font-family: Arial">Question No <c:out value="${ts.step + 1}" /> of ${ts.qcount - 1}</h2>

                <p>${ts.currentQuestion.questionTitle}</p>

            </div>



            <!-- Example row of columns -->
            <div class="row">

                <c:forEach items="${ts.answers}" var="answers" varStatus="state">
                    <div class="span4" style="font-family: Arial">
                        <h2 style="font-family: Arial"><img src="../pub/img/check.png" alt="" id="${ts.alet[state.index]}" style="display: none" /> Answer ${ts.alet[state.index]}</h2>

                        <p style="font-family: Arial"><c:out value="${answers.answerTitle}" />

                    </div>
                </c:forEach>


            </div>


        </div> <!-- /container -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <script src="../pub/js/bootstrap-transition.js"></script>
        <script src="../pub/js/bootstrap-alert.js"></script>
        <script src="../pub/js/bootstrap-modal.js"></script>
        <script src="../pub/js/bootstrap-dropdown.js"></script>
        <script src="../pub/js/bootstrap-scrollspy.js"></script>
        <script src="../pub/js/bootstrap-tab.js"></script>
        <script src="../pub/js/bootstrap-tooltip.js"></script>
        <script src="../pub/js/bootstrap-popover.js"></script>
        <script src="../pub/js/bootstrap-button.js"></script>
        <script src="../pub/js/bootstrap-collapse.js"></script>
        <script src="../pub/js/bootstrap-carousel.js"></script>
        <script src="../pub/js/bootstrap-typeahead.js"></script>

    </body>
</html>
