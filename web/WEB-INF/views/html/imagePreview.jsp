<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="str" uri="http://stripes.sourceforge.net/stripes.tld"%>

<str:useActionBean id="image" beanclass="lv.nmc.webs.cat.mediaViewActionBean" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
    <center> 
        <str:errors />
        <img src="http://jnlp.novikontas.lv/media/${image.media.mediaLocation}" style="height: 125px" alt="" />
    </center>
    </body>
</html>
