<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" session="false"%>
<%@ taglib prefix="str" uri="http://stripes.sourceforge.net/stripes.tld"%>
<str:useActionBean id="special" beanclass="lv.nmc.webs.val.certEntryActionBean" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Certificate Authenticity Verification</title>
        <script type="text/javascript" src="http://apps.novikontas.lv/apiary/pub/js/functions.js"></script>
        <script type="text/javascript" src="http://apps.novikontas.lv/apiary/pub/js/prototype.js"></script>
        <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
</head>
    <body>
        
         <div class="container">
      <div class="header clearfix">
       <!-- <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" class="active"><a href="#">Home</a></li>
            <li role="presentation"><a href="#">About</a></li>
            <li role="presentation"><a href="#">Contact</a></li>
          </ul>
        </nav> /-->
        <h3 class="text-muted"><img src="http://apps.novikontas.lv/apiary/img/nv64.png" />Certificate Authenticity Verification</h3>
      </div>

      <div class="jumbotron">
        
          
        <h1>Certificate Authenticity Verification</h1>
        <form method="post" action="#" onsubmit="return false" id="certcheck">
            Certificate ID: <input type="text" name="certNr" id="certNr" /> (As seen on certificate)<br />
            Date of issue: <input type="text" name="dateEnd" id="dateEnd" /> Format: dd/mm/yyyy (As seen on certificate)
            <input type="hidden" name="_eventName" value="checkCert" id="_eventName" />
        </form>
        <p><a class="btn btn-lg btn-success" href="#" role="button" onclick="genericAjaxRequest('disp', 'index.html', $('certcheck').serialize(true))">Check certificate</a>
        </p>
      </div>

      <div class="row marketing">
        <div class="col-lg-6">
          <div id="disp"></div>
        </div>

        <div class="col-lg-6">
         
        </div>
      </div>

    

    </div> <!-- /container -->

        
    </body>
</html>
