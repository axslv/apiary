<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="str" uri="http://stripes.sourceforge.net/stripes.tld"%>
<str:useActionBean id="special" beanclass="lv.nmc.webs.val.certEntryActionBean" />
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:set var="color" value="red" />
<c:if test="${special.certState eq 'Confirmed'}">
    <c:set var="color" value="green" />
</c:if>

Certificate is <span style="text-transform: uppercase; color: ${color}">${special.certState}</span>

<c:if test="${special.certState eq 'Confirmed'}">
    <h3>Certificate details:</h3>

    Name, Surname:  ${fn:replace(special.cert.nameSurname, '*', '')}<br />
    Date of birth: ${special.birthDate}<br />
    Course name: ${special.cert.courseName}<br />
    Old certificate ID: ${special.cert.oldCertNr}<br />
    Old issuer: ${special.cert.oldIssuer}<br />
    Old date issued: ${special.cert.oldDateIssued}

</c:if>

<c:if test="${special.certState eq 'NOT Confirmed'}">
    Please check inserted data. In case your certificate is not found please contact us on <a href="mailto:verify@novikontas.lv">verify@novikontas.lv</a>
    <br /><br /><span style="font-size: 10px">Note: certificates issued before year 2011 are added manually and may not be found through the website verification system yet, 
        contact verify@novikontas.lv for more information.
    </span>
</c:if>

