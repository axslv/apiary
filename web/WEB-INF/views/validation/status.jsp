<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="str" uri="http://stripes.sourceforge.net/stripes.tld"%>
<str:useActionBean id="special" beanclass="lv.nmc.webs.val.certEntryActionBean" />
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:set var="color" value="red" />
<c:if test="${special.certState eq 'Confirmed'}">
    <c:set var="color" value="green" />
</c:if>

Certificate Authenticity <span style="text-transform: uppercase; color: ${color}">${special.certState}</span>

<c:if test="${special.certState eq 'Confirmed'}">
    <h3>Certificate data:</h3>
    Name, Surname:  ${fn:replace(special.cert.nameSurname, '*', '')}<br />
    Date of birth: ${special.birthDate}<br />
    Course name: ${special.cert.courseName}<br />
    Training started: ${special.cert.dateStart}<br />
    Training ended: ${special.cert.issueDate}<br />    
</c:if>

<c:if test="${special.certState eq 'NOT Confirmed'}">
    Please check inserted data. In case your certificate is not found please contact us on <a href="mailto:verify@novikontas.lv">verify@novikontas.lv</a>
    <br /><br /><span style="font-size: 10px">Note: certificates issued before year 2011 are added manually and may not be found through the website verification system yet. Contact <a href="mailto:verify@novikontas.lv">verify@novikontas.lv</a> for more information.
    </span>
</c:if>

${fn:replace(string1, 'first', 'second')}