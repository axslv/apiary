var queryDict = {}
location.search.substr(1).split("&").forEach(function(item) { queryDict[item.split("=")[0]] = item.split("=")[1] })

function sendError(text) {
    $('#textAlert').text(text);
    $('#textAlert').fadeIn();
    setTimeout(function() {
        $('#textAlert').fadeOut();
    }, 2500);
}

$('#cog').click(function() {
    $('#chooser').toggle();
});

$('#print').click(function() {
    $('.hideme').hide();
    window.print();
    setTimeout(function() {
        $('.hideme').show();
    }, 1000);
});



jQuery(document).bind("keyup keydown", function(e) {
    if (e.ctrlKey && e.keyCode == 80) {
        $('.hideme').hide();
        setTimeout(function() {
            $('.hideme').show();
        }, 1000);
    }
});

$("li").click(function() {
    path = $(this).attr('id');
    $("#drop").attr("src", "cert/" + path + ".jpg");

    var json = "js/" + path + ".json";
    console.log(json);
    $.getJSON(json, {
            format: "json"
        })
        .done(function(data) {
            $('#export').val("");
            $.each(data, function(i) {
                var item = data[i];
                $.each(item, function(it) {
                    var newItem = item[it];
                    var newStyle = newItem.id + "|" + newItem.text + "|" + newItem.top + "|" + newItem.left + "|" + newItem.size + "|";
                    newStyle.trim();
                    $('#export').val($('#export').val() + newStyle);
                    $("#Import").trigger("click");
                    //      console.log(newStyle);
                });

            });
        })
        .fail(function() {
            console.log("no preset");
        });

});

$('#moreText').click(function() {
    var text = $('#AddText').val(),
        randstuff = String.fromCharCode(65 + Math.floor(Math.random() * 26)),
        customID = randstuff + Date.now();
    console.log(customID);
    console.log(text);
    if (text === '') {
        //alert('Input Text.');
        $('#AddText').addClass('bg-danger');
        sendError('Input Text.');
        setTimeout(function() {
            $('#AddText').removeClass('bg-danger');
        }, 2500);
    } else {
        if (Math.floor($('#textSize').val()) == $('#textSize').val() && $.isNumeric($('#textSize').val())) {
            var fontSize = $('#textSize').val();
            $('body').append("<div class='dragThis' id='" + customID + "' style='font-size: " + fontSize + "px;'>" + text + "</div>");
            $('.dragThis').draggable();
        } else {
            sendError('Size should be a number.');
        }
    }
});


$('.dragThis').draggable({
    zIndex: 100,
    drag: function() {
        //     var offset = $(this).offset();
        //      var xPos = Math.round(offset.left);
        //      var yPos = Math.round(offset.top);
        //      $("#x").val(xPos);
        //    $('#y').val(yPos);


        var position = $(this).position();
        var xPos = Math.round(position.left);
        var yPos = Math.round(position.top);
    }
});

$('#trashCAN').click(function() {
    $('.dragThis').remove();
});

$('#trashCAN').droppable({
    drop: function(event, ui) {
        trashThis(ui.draggable);
    }
});


function trashThis(trash) {
    trash.remove();
}

$('#save').click(function() {
    var pos = $('.dragThis').position();
    //console.log(pos);
    var elems = $('.dragThis');
    var N = elems.length;
    $('#export').val("");
    $('.dragThis').each(function() {
        var insideText = $(this).text(),
            pos = $(this).position(),
            pLeft = Math.round(pos.left),
            pTop = Math.round(pos.top),
            saveID = $(this).attr('id'),
            size = $(this).css('fontSize');
        console.log('font size : ' + size);
        console.log(insideText + '| Top = ' + pTop + ' Left = ' + pLeft + ' | id = ' + saveID);
        $('#export').val($('#export').val() + saveID + "|" + insideText + "|" + pTop + "|" + pLeft + "|" + size + "|");
        //  $('export').val(insideText);

        //  console.log(saveID+"|"+insideText+"|"+pTop+"|"+pLeft);
        //  Cookies.set(saveID, pRight+","+pLeft+","+insideText+"",{ expires: 7, path: '' });
    });

    console.log(N);
});


$('#Import').click(function() {
    var result = $('#export').val().split('|');
    $('.dragThis').remove();
    while (result.length) {
        if (result[0] == "") {
            return;
        }
        var start = result.splice(0, 5),
            id = start[0],
            top = start[2],
            left = start[3];
        size = start[4]
        console.log(size);
        //console.log(withTag);
        $('body').append("<div class='dragThis' style='top:" + top + "px;left:" + left + "px;font-size: " + size + ";' id='" + start[0] + "'>" + start[1] + "</div>");
        //  $('#'+id).css("top: "+top+"px ;left: "+left+"px ;");
        //  console.log($("#" +  id));

        $('.dragThis').draggable();
        console.log(start);
    }
});



function load_cert(json) {

    console.log(json);
}