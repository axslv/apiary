<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" session="false"%>
<%@ taglib prefix="str" uri="http://stripes.sourceforge.net/stripes.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<str:useActionBean id="sp" beanclass="lv.nmc.webs.paymentActionBean" />

<html>
    <head>
        <title>Paradnieki - Novikontas</title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    </head>
    <body>
        
        
        <table width="90%" border="1">
            <c:forEach items="${sp.possibleDates}" var="curDate" varStatus="state">
                <tr>
                    <td><span style="font-size:22px; font-weight: bold;">${curDate}</span></td>
                    <td>
                        <table width="100%" border="1">
                            <tr>
                                <td>Name, Surname</td>
                                <td>Person code</td>
                                <td>Course name></td>
                                <td>Date start</td>
                                <td>Date end</td>
                                <td>Payment date</td>
                                <td>Price</td>
                            </tr>
                            
                            <c:set var="plist" value="${sp.uberMap[curDate]}" />
                            
                            <c:forEach items="${plist}" var="payment">
                                <tr>
                                    <td>${payment.nameSurname}</td>
                                    <td>${payment.personCode}</td>
                                    <td>${payment.courseName}</td>
                                    <td>${payment.dateStart}</td>
                                    <td>${payment.dateEnd}</td>
                                    <td>${payment.paymentDate}</td>
                                    <td>${payment.price}</td>
                                </tr>
                            </c:forEach>
                            
                        </table>
                    </td>
                </tr>
            </c:forEach>
        </table>
        
    </body>