/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.dao;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import lv.nmc.batis.mappers.library.LibraryMapper;
import lv.nmc.entities.BookEntry;
import org.apache.ibatis.session.SqlSession;

/**
 *
 * @author mj
 */
public class BookDao {

    private SqlSession session = LibraryDataFactory.getSqlSessionFactory().openSession();
    private List<BookEntry> results = new ArrayList<BookEntry>();
    private BookEntry output = new BookEntry();
    
  
  
    public void executeSelect(String queryId, BookEntry query) {
        try {

            LibraryMapper sm = session.getMapper(LibraryMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{BookEntry.class});
            results = (List<BookEntry>) md.invoke(sm, query);

        } catch (NoSuchMethodException ex) {
            System.out.println("No such method. " + queryId);
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeUpdate(String queryId, BookEntry query) {
        try {

            LibraryMapper sm = session.getMapper(LibraryMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{BookEntry.class});
            Object res = md.invoke(sm, query);
            
            try {
                results = (List<BookEntry>)res;
            } catch(Exception e) {
                
            }
            
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeInsert(String queryId, BookEntry query) {
        try {

            LibraryMapper sm = session.getMapper(LibraryMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{BookEntry.class});

            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
        } finally {
            session.close();
        }
    }

    /**
     * @return the results
     */
    public List<BookEntry> getResults() {
        return results;
    }

    /**
     * @return the output
     */
    public BookEntry getOutput() {
        return output;
    }
}
