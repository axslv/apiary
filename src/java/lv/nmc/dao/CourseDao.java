/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.dao;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import lv.nmc.batis.mappers.CourseMapper;
import lv.nmc.entities.CourseEntry;
import lv.nmc.entities.CourseIncomeQuery;
import org.apache.ibatis.session.SqlSession;

/**
 *
 * @author mj
 */
public class CourseDao {

    private SqlSession session = MainDbFactory.getSqlSessionFactory().openSession();
    private List<CourseEntry> results = new ArrayList<CourseEntry>();
    private CourseEntry output = new CourseEntry();
    
    public static List<CourseEntry> coursesByDepartments(CourseIncomeQuery cq) throws Exception {
        List<CourseEntry> ls = new ArrayList<CourseEntry>();
        SqlSession session = MainDbFactory.getSqlSessionFactory().openSession();        
        try {
            CourseMapper cm = session.getMapper(CourseMapper.class);
            ls = cm.coursesByDepartments(cq);
        } finally {
            session.close();
        }       
        
        return ls;
    }
    
    
  
    public void executeSelect(String queryId, CourseEntry query) {
        try {

            CourseMapper sm = session.getMapper(CourseMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{CourseEntry.class});
            results = (List<CourseEntry>) md.invoke(sm, query);

        } catch (NoSuchMethodException ex) {
            System.out.println("No such method. " + queryId);
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeUpdate(String queryId, CourseEntry query) {
        try {

            CourseMapper sm = session.getMapper(CourseMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{CourseEntry.class});
            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
        } finally {
            session.close();
        }

    }

    public void executeInsert(String queryId, CourseEntry query) {
        try {

            CourseMapper sm = session.getMapper(CourseMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{CourseEntry.class});

            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
        } finally {
            session.close();
        }
    }

    /**
     * @return the results
     */
    public List<CourseEntry> getResults() {
        return results;
    }

    /**
     * @return the output
     */
    public CourseEntry getOutput() {
        return output;
    }
}
