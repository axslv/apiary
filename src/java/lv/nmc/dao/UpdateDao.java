/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.dao;

import lv.nmc.dao.n3.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import lv.nmc.batis.mappers.UpdateMapper;
import lv.nmc.entities.UpdateEntry;
import org.apache.ibatis.session.SqlSession;

/**
 *
 * @author mj
 */
public class UpdateDao {

    private SqlSession session = NregDbFactory.getSqlSessionFactory().openSession();
    private List<UpdateEntry> results = new ArrayList<UpdateEntry>();
    private UpdateEntry output = new UpdateEntry();

    public void executeSelect(String queryId, UpdateEntry query) {
        try {

            UpdateMapper sm = session.getMapper(UpdateMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{UpdateEntry.class});
            results = (List<UpdateEntry>) md.invoke(sm, query);

        } catch (NoSuchMethodException ex) {
            System.out.println("No such method. " + queryId);
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeUpdate(String queryId, UpdateEntry query) {
        try {

            UpdateMapper sm = session.getMapper(UpdateMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{UpdateEntry.class});
            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeInsert(String queryId, UpdateEntry query) {
        try {

            UpdateMapper sm = session.getMapper(UpdateMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{UpdateEntry.class});

            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
        } finally {
            session.close();
        }
    }

    /**
     * @return the results
     */
    public List<UpdateEntry> getResults() {
        return results;
    }

    /**
     * @return the output
     */
    public UpdateEntry getOutput() {
        return output;
    }
}
