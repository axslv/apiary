/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.dao.sys;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import lv.nmc.batis.mappers.sys.BackupMapper;
import lv.nmc.entities.sys.BackupEntry;
import org.apache.ibatis.session.SqlSession;

/**
 *
 * @author mj
 */
public class BackupDao {

    private SqlSession session = SysDbFactory.getSqlSessionFactory().openSession();
    private List<BackupEntry> results = new ArrayList<BackupEntry>();
    private BackupEntry output = new BackupEntry();
    
  
  
    public void executeSelect(String queryId, BackupEntry query) {
        try {

            BackupMapper sm = session.getMapper(BackupMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{BackupEntry.class});
            results = (List<BackupEntry>) md.invoke(sm, query);

        } catch (NoSuchMethodException ex) {
            System.out.println("No such method. " + queryId);
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeUpdate(String queryId, BackupEntry query) {
        try {

            BackupMapper sm = session.getMapper(BackupMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{BackupEntry.class});
            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
        } finally {
            session.close();
        }

    }

    public void executeInsert(String queryId, BackupEntry query) {
        try {

            BackupMapper sm = session.getMapper(BackupMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{BackupEntry.class});

            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
        } finally {
            session.close();
        }
    }

    /**
     * @return the results
     */
    public List<BackupEntry> getResults() {
        return results;
    }

    /**
     * @return the output
     */
    public BackupEntry getOutput() {
        return output;
    }
}
