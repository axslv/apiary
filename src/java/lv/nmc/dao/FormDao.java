/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.dao;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import lv.nmc.batis.mappers.form.FormMapper;
import lv.nmc.dao.n3.NregDbFactory;
import lv.nmc.entities.form.FormData;
import org.apache.ibatis.session.SqlSession;

/**
 *
 * @author mj
 */
public class FormDao {

    private SqlSession session = NregDbFactory.getSqlSessionFactory().openSession();
    private List<FormData> results = new ArrayList<FormData>();
    private FormData output = new FormData();
    
  
  
    public void executeSelect(String queryId, FormData query) {
        try {

            FormMapper sm = session.getMapper(FormMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{FormData.class});
            results = (List<FormData>) md.invoke(sm, query);

        } catch (NoSuchMethodException ex) {
            System.out.println("No such method. " + queryId);
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeUpdate(String queryId, FormData query) {
        try {

            FormMapper sm = session.getMapper(FormMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{FormData.class});
            Object res = md.invoke(sm, query);
            
            try {
                results = (List<FormData>)res;
            } catch(Exception e) {
                
            }
            
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeInsert(String queryId, FormData query) {
        try {

            FormMapper sm = session.getMapper(FormMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{FormData.class});

            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
        } finally {
            session.close();
        }
    }

    /**
     * @return the results
     */
    public List<FormData> getResults() {
        return results;
    }

    /**
     * @return the output
     */
    public FormData getOutput() {
        return output;
    }
}
