/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.dao;

import lv.nmc.dao.n3.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import lv.nmc.batis.mappers.EmailMapper;
import lv.nmc.entities.EmailEntry;
import org.apache.ibatis.session.SqlSession;


public class EmailDao {

    private SqlSession session = NregDbFactory.getSqlSessionFactory().openSession();
    private List<EmailEntry> results = new ArrayList<EmailEntry>();
    private EmailEntry output = new EmailEntry();

    public void executeSelect(String queryId, EmailEntry query) {
        try {

            EmailMapper sm = session.getMapper(EmailMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{EmailEntry.class});
            results = (List<EmailEntry>) md.invoke(sm, query);

        } catch (NoSuchMethodException ex) {
            System.out.println("No such method. " + queryId);
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeUpdate(String queryId, EmailEntry query) {
        try {

            EmailMapper sm = session.getMapper(EmailMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{EmailEntry.class});
            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeInsert(String queryId, EmailEntry query) {
        try {

            EmailMapper sm = session.getMapper(EmailMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{EmailEntry.class});

            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
        } finally {
            session.close();
        }
    }

    /**
     * @return the results
     */
    public List<EmailEntry> getResults() {
        return results;
    }

    /**
     * @return the output
     */
    public EmailEntry getOutput() {
        return output;
    }
}
