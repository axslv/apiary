/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.dao;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import lv.nmc.batis.mappers.ClientMapper;
import lv.nmc.entities.ClientEntry;
import org.apache.ibatis.session.SqlSession;

/**
 *
 * @author mj
 */
public class ClientDao {

    private SqlSession session = MainDbFactory.getSqlSessionFactory().openSession();
    private List<ClientEntry> results = new ArrayList<ClientEntry>();
    private ClientEntry output = new ClientEntry();
    
  
  
    public void executeSelect(String queryId, ClientEntry query) {
        try {

            ClientMapper sm = session.getMapper(ClientMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{ClientEntry.class});
            results = (List<ClientEntry>) md.invoke(sm, query);

        } catch (NoSuchMethodException ex) {
            System.out.println("No such method. " + queryId);
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeUpdate(String queryId, ClientEntry query) {
        try {

            ClientMapper sm = session.getMapper(ClientMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{ClientEntry.class});
            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
        } finally {
            session.close();
        }

    }

    public void executeInsert(String queryId, ClientEntry query) {
        try {

            ClientMapper sm = session.getMapper(ClientMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{ClientEntry.class});

            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
        } finally {
            session.close();
        }
    }

    /**
     * @return the results
     */
    public List<ClientEntry> getResults() {
        return results;
    }

    /**
     * @return the output
     */
    public ClientEntry getOutput() {
        return output;
    }
}
