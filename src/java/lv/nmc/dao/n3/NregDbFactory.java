/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.dao.n3;

import java.io.Reader;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

/**
 *
 * @author jm
 */
public class NregDbFactory {

protected static final SqlSessionFactory FACTORY;
	 
	    static {
	        try {
	            Reader reader = Resources.getResourceAsReader("NDatabase.xml");
	            FACTORY = new SqlSessionFactoryBuilder().build(reader);
	        } catch (Exception e){
	            throw new RuntimeException("Fatal Error.  Cause: " + e, e);
	        }
	    }
            
          public static SqlSessionFactory getSqlSessionFactory() {
	        return FACTORY;
	    }      
}
