/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.dao.n3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import lv.nmc.batis.mappers.n3.SmsMapper;
import lv.nmc.entities.n3.Sms;
import org.apache.ibatis.session.SqlSession;

/**
 *
 * @author mj
 */
public class SmsDao {

    private SqlSession session = NregDbFactory.getSqlSessionFactory().openSession();
    private List<Sms> results = new ArrayList<Sms>();
    private Sms output = new Sms();
    
  
  
    public void executeSelect(String queryId, Sms query) {
        try {

            SmsMapper sm = session.getMapper(SmsMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{Sms.class});
            results = (List<Sms>) md.invoke(sm, query);

        } catch (NoSuchMethodException ex) {
            System.out.println("No such method. " + queryId);
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeUpdate(String queryId, Sms query) {
        try {

            SmsMapper sm = session.getMapper(SmsMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{Sms.class});
            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeInsert(String queryId, Sms query) {
        try {

            SmsMapper sm = session.getMapper(SmsMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{Sms.class});

            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
        } finally {
            session.close();
        }
    }

    /**
     * @return the results
     */
    public List<Sms> getResults() {
        return results;
    }

    /**
     * @return the output
     */
    public Sms getOutput() {
        return output;
    }
}
