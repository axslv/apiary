/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.dao.n3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import lv.nmc.batis.mappers.n3.CollegeMapper;
import lv.nmc.entities.n3.col.CollegeEntry;
import org.apache.ibatis.session.SqlSession;

/**
 *
 * @author mj
 */
public class CollegeDao {

    private SqlSession session = NregDbFactory.getSqlSessionFactory().openSession(true);
    private List<CollegeEntry> results = new ArrayList<CollegeEntry>();
    private CollegeEntry output = new CollegeEntry();
    
  
  
    public void executeSelect(String queryId, CollegeEntry query) {
        try {

            CollegeMapper sm = session.getMapper(CollegeMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{CollegeEntry.class});
            results = (List<CollegeEntry>) md.invoke(sm, query);

        } catch (NoSuchMethodException ex) {
            System.out.println("No such method. " + queryId);
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc..");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeUpdate(String queryId, CollegeEntry query) {
        try {

            CollegeMapper sm = session.getMapper(CollegeMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{CollegeEntry.class});
            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
            ex.printStackTrace();
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeInsert(String queryId, CollegeEntry query) {
        try {

            CollegeMapper sm = session.getMapper(CollegeMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{CollegeEntry.class});

            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
        } finally {
            session.close();
        }
    }

    /**
     * @return the results
     */
    public List<CollegeEntry> getResults() {
        return results;
    }

    /**
     * @return the output
     */
    public CollegeEntry getOutput() {
        return output;
    }
}
