/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.dao.n3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import lv.nmc.batis.mappers.n3.RegistrationMapper;
import lv.nmc.entities.n3.RegistrationEntry;
import org.apache.ibatis.session.SqlSession;

/**
 *
 * @author mj
 */
public class RegistrationDao {

    private SqlSession session = NregDbFactory.getSqlSessionFactory().openSession();
    private List<RegistrationEntry> results = new ArrayList<RegistrationEntry>();
    private RegistrationEntry output = new RegistrationEntry();
    
    
  
    public void executeSelect(String queryId, RegistrationEntry query) {
        try {

            RegistrationMapper sm = session.getMapper(RegistrationMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{RegistrationEntry.class});
            results = (List<RegistrationEntry>) md.invoke(sm, query);

        } catch (NoSuchMethodException ex) {
            ex.printStackTrace();
            System.out.println("No such method. " + queryId);
         //   throw  new NoSuchMethodException("No such method!");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeUpdate(String queryId, RegistrationEntry query) {
        try {

            RegistrationMapper sm = session.getMapper(RegistrationMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{RegistrationEntry.class});
            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            ex.printStackTrace();
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeInsert(String queryId, RegistrationEntry query) {
        try {

            RegistrationMapper sm = session.getMapper(RegistrationMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{RegistrationEntry.class});

            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            ex.printStackTrace();
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }
    }

    /**
     * @return the results
     */
    public List<RegistrationEntry> getResults() {
        return results;
    }

    /**
     * @return the output
     */
    public RegistrationEntry getOutput() {
        return output;
    }
}
