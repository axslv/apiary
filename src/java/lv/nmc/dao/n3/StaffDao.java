/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.dao.n3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import lv.nmc.batis.mappers.n3.StaffMapper;
import lv.nmc.entities.n3.StaffEntry;
import org.apache.ibatis.session.SqlSession;

/**
 *
 * @author mj
 */
public class StaffDao {

    private SqlSession session = NregDbFactory.getSqlSessionFactory().openSession();
    private List<StaffEntry> results = new ArrayList<StaffEntry>();
    private StaffEntry output = new StaffEntry();

    public void executeSelect(String queryId, StaffEntry query) {
        try {

            StaffMapper sm = session.getMapper(StaffMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{StaffEntry.class});
            results = (List<StaffEntry>) md.invoke(sm, query);

        } catch (NoSuchMethodException ex) {
            System.out.println("No such method. " + queryId);
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeUpdate(String queryId, StaffEntry query) {
        try {

            StaffMapper sm = session.getMapper(StaffMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{StaffEntry.class});
            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeInsert(String queryId, StaffEntry query) {
        try {

            StaffMapper sm = session.getMapper(StaffMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{StaffEntry.class});

            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
        } finally {
            session.close();
        }
    }

    /**
     * @return the results
     */
    public List<StaffEntry> getResults() {
        return results;
    }

    /**
     * @return the output
     */
    public StaffEntry getOutput() {
        return output;
    }
}
