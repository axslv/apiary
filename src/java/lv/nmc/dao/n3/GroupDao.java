/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.dao.n3;

import lv.nmc.dao.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import lv.nmc.batis.mappers.n3.GroupMapper;
import lv.nmc.entities.n3.GroupEntry;
import org.apache.ibatis.session.SqlSession;

/**
 *
 * @author mj
 */
public class GroupDao {

    private SqlSession session = NregDbFactory.getSqlSessionFactory().openSession();
    private List<GroupEntry> results = new ArrayList<GroupEntry>();
    private GroupEntry output = new GroupEntry();
    
    
  
    public void executeSelect(String queryId, GroupEntry query) {
        try {

            GroupMapper sm = session.getMapper(GroupMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{GroupEntry.class});
            results = (List<GroupEntry>) md.invoke(sm, query);

        } catch (NoSuchMethodException ex) {
            System.out.println("No such method. " + queryId);
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
            
        } finally {
            session.close();
        }

    }

    public void executeUpdate(String queryId, GroupEntry query) {
        try {
            GroupMapper sm = session.getMapper(GroupMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{GroupEntry.class});
            md.invoke(sm, query);
            session.commit();

        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();              
        }

    }

    public void executeInsert(String queryId, GroupEntry query) {
        try {

            GroupMapper sm = session.getMapper(GroupMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{GroupEntry.class});

            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }
    }

    /**
     * @return the results
     */
    public List<GroupEntry> getResults() {
        return results;
    }

    /**
     * @return the output
     */
    public GroupEntry getOutput() {
        return output;
    }
}
