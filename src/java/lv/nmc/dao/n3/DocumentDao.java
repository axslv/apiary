/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.dao.n3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import lv.nmc.batis.mappers.n3.DocumentMapper;
import lv.nmc.entities.n3.DocumentEntry;
import org.apache.ibatis.session.SqlSession;

/**
 *
 * @author mj
 */
public class DocumentDao {

    private SqlSession session = NregDbFactory.getSqlSessionFactory().openSession();
    private List<DocumentEntry> results = new ArrayList<DocumentEntry>();
    private DocumentEntry output = new DocumentEntry();
    
  
  
    public void executeSelect(String queryId, DocumentEntry query) {
        try {

            DocumentMapper sm = session.getMapper(DocumentMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{DocumentEntry.class});
            results = (List<DocumentEntry>) md.invoke(sm, query);

        } catch (NoSuchMethodException ex) {
            System.out.println("No such method. " + queryId);
            ex.printStackTrace();
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeUpdate(String queryId, DocumentEntry query) {
        try {

            DocumentMapper sm = session.getMapper(DocumentMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{DocumentEntry.class});
            Object res = md.invoke(sm, query);
            
            try {
                results = (List<DocumentEntry>)res;
            } catch(Exception e) {
                
            }
            
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeInsert(String queryId, DocumentEntry query) {
        try {

            DocumentMapper sm = session.getMapper(DocumentMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{DocumentEntry.class});

            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
        } finally {
            session.close();
        }
    }

    /**
     * @return the results
     */
    public List<DocumentEntry> getResults() {
        return results;
    }

    /**
     * @return the output
     */
    public DocumentEntry getOutput() {
        return output;
    }
}
