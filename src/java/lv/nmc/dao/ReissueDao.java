/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.dao;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import lv.nmc.batis.mappers.ReissueMapper;

import lv.nmc.entities.ReissueEntry;
import org.apache.ibatis.session.SqlSession;

/**
 *
 * @author mj
 */
public class ReissueDao {

    private SqlSession session = MainDbFactory.getSqlSessionFactory().openSession();
    private List<ReissueEntry> results = new ArrayList<ReissueEntry>();
    private ReissueEntry output = new ReissueEntry();
    
  
  
    public void executeSelect(String queryId, ReissueEntry query) {
        try {

            ReissueMapper sm = session.getMapper(ReissueMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{ReissueEntry.class});
            results = (List<ReissueEntry>) md.invoke(sm, query);

        } catch (NoSuchMethodException ex) {
            System.out.println("No such method. " + queryId);
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeUpdate(String queryId, ReissueEntry query) {
        try {

            ReissueMapper sm = session.getMapper(ReissueMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{ReissueEntry.class});
            Object res = md.invoke(sm, query);
            
            try {
                results = (List<ReissueEntry>)res;
            } catch(Exception e) {
                
            }
            
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void executeInsert(String queryId, ReissueEntry query) {
        try {

            ReissueMapper sm = session.getMapper(ReissueMapper.class);
            Method md = sm.getClass().getMethod(queryId, new Class[]{ReissueEntry.class});

            md.invoke(sm, query);
            session.commit();



        } catch (NoSuchMethodException ex) {
            System.out.println("No such method.");
        } catch (SecurityException ex) {
            System.out.println("Security exception");
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument exception");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
        } finally {
            session.close();
        }
    }

    /**
     * @return the results
     */
    public List<ReissueEntry> getResults() {
        return results;
    }

    /**
     * @return the output
     */
    public ReissueEntry getOutput() {
        return output;
    }
}
