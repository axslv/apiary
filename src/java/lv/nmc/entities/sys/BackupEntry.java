/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.entities.sys;

/**
 *
 * @author jm
 */
public class BackupEntry {
    private Integer recordId = 0;
    private String property, value, target, source;
    private String snapshotName, snapshotSource, snapshotDate;

    /**
     * @return the property
     */
    public String getProperty() {
        return property;
    }

    /**
     * @param property the property to set
     */
    public void setProperty(String property) {
        this.property = property;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the target
     */
    public String getTarget() {
        return target;
    }

    /**
     * @param target the target to set
     */
    public void setTarget(String target) {
        this.target = target;
    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * @return the snapshotName
     */
    public String getSnapshotName() {
        return snapshotName;
    }

    /**
     * @param snapshotName the snapshotName to set
     */
    public void setSnapshotName(String snapshotName) {
        this.snapshotName = snapshotName;
    }

    /**
     * @return the snapshotSource
     */
    public String getSnapshotSource() {
        return snapshotSource;
    }

    /**
     * @param snapshotSource the snapshotSource to set
     */
    public void setSnapshotSource(String snapshotSource) {
        this.snapshotSource = snapshotSource;
    }

    /**
     * @return the snapshotDate
     */
    public String getSnapshotDate() {
        return snapshotDate;
    }

    /**
     * @param snapshotDate the snapshotDate to set
     */
    public void setSnapshotDate(String snapshotDate) {
        this.snapshotDate = snapshotDate;
    }

    /**
     * @return the recordId
     */
    public Integer getRecordId() {
        return recordId;
    }

    /**
     * @param recordId the recordId to set
     */
    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }
    
}
