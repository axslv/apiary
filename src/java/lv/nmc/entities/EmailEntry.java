/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.entities;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author mj
 */
@XStreamAlias("EmailEntry")
public class EmailEntry {
    private String event;
    private String sendTo, sendFrom, emailSubject, emailText;
//server info
    private String key, value;

 
    public String getEvent() {
        return event;
    }

   
    public void setEvent(String event) {
        this.event = event;
    }


    public String getSendTo() {
        return sendTo;
    }


    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }


    public String getSendFrom() {
        return sendFrom;
    }


    public void setSendFrom(String sendFrom) {
        this.sendFrom = sendFrom;
    }


    public String getEmailSubject() {
        return emailSubject;
    }


    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }


    public String getEmailText() {
        return emailText;
    }


    public void setEmailText(String emailText) {
        this.emailText = emailText;
    }


    public String getKey() {
        return key;
    }


    public void setKey(String key) {
        this.key = key;
    }


    public String getValue() {
        return value;
    }


    public void setValue(String value) {
        this.value = value;
    }
    

  
   
}
