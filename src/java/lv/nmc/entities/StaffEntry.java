/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.entities;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 *
 * @author jm
 */
@XStreamAlias("StaffEntry")
public class StaffEntry implements Serializable {
    private String username, password, salt, alias, nameSurname, personCode, instructorType, department, evalEnd;
    private Integer staffId, bonusId, rewardId = 0;
    
    private Integer special, active = 0;
    private Double bonus, reward = 0.00;
    private String bonusStarts, rewardEnds, bonusEnds, rewardStarts = "";
    private String bonusRounded, message, event, passwordRe = "";
    
//private List<String> aliases =  new ArrayList();


    public StaffEntry() {
    }
        public StaffEntry(Integer staffEntry) {
            this.staffId = staffEntry;
    }
        
        
           public StaffEntry(Integer staffEntry, String event) {
            this.staffId = staffEntry;
            this.event = event;
    }
         
    
     public StaffEntry(Integer record, String bonusStarts, String bonusRounded) {
         this.bonusId = record;
         this.bonusStarts = bonusStarts;
         this.bonusRounded=bonusRounded;
    }  

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the salt
     */
    public String getSalt() {
        return salt;
    }

    /**
     * @param salt the salt to set
     */
    public void setSalt(String salt) {
        this.salt = salt;
    }


    /**
     * @return the staffId
     */
    public Integer getStaffId() {
        return staffId;
    }

    /**
     * @param staffId the staffId to set
     */
    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    /**
     * @return the alias
     */
    public String getAlias() {
        return alias;
    }

    /**
     * @param alias the alias to set
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /**
     * @return the special
     */
    public Integer getSpecial() {
        return special;
    }

    /**
     * @param special the special to set
     */
    public void setSpecial(Integer special) {
        this.special = special;
    }

    /**
     * @return the nameSurname
     */
    public String getNameSurname() {
        return nameSurname;
    }

    /**
     * @param nameSurname the nameSurname to set
     */
    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    /**
     * @return the personCode
     */
    public String getPersonCode() {
        return personCode;
    }

    /**
     * @param personCode the personCode to set
     */
    public void setPersonCode(String personCode) {
        this.personCode = personCode;
    }

    /**
     * @return the instructorType
     */
    public String getInstructorType() {
        return instructorType;
    }

    /**
     * @param instructorType the instructorType to set
     */
    public void setInstructorType(String instructorType) {
        this.instructorType = instructorType;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return the evalEnd
     */
    public String getEvalEnd() {
        return evalEnd;
    }

    /**
     * @param evalEnd the evalEnd to set
     */
    public void setEvalEnd(String evalEnd) {
        this.evalEnd = evalEnd;
    }

    /**
     * @return the active
     */
    public Integer getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Integer active) {
        this.active = active;
    }

    /**
     * @return the bonus
     */
    public Double getBonus() {
        return bonus;
    }

    /**
     * @param bonus the bonus to set
     */
    public void setBonus(Double bonus) {
        BigDecimal bd = BigDecimal.valueOf(bonus);
        BigDecimal ss = bd.setScale(2);
        this.bonus = ss.doubleValue();
        DecimalFormat df = new DecimalFormat(".00");
        bonusRounded = df.format(this.bonus);
    }
    
    public void setBonusRounded(String bonusRounded) {
        this.bonusRounded = bonusRounded;
    }
    
    /**
     * @return the bonusStarts
     */
    public String getBonusStarts() {
        return bonusStarts;
    }

    /**
     * @param bonusStarts the bonusStarts to set
     */
    public void setBonusStarts(String bonusStarts) {
        this.bonusStarts = bonusStarts;
    }

    /**
     * @return the bonusRounded
     */
    public String getBonusRounded() {
        return bonusRounded;
    }

    /**
     * @return the bonusId
     */
    public Integer getBonusId() {
        return bonusId;
    }

    /**
     * @param bonusId the bonusId to set
     */
    public void setBonusId(Integer bonusId) {
        this.bonusId = bonusId;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @return the passwordRe
     */
    public String getPasswordRe() {
        return passwordRe;
    }

    /**
     * @param passwordRe the passwordRe to set
     */
    public void setPasswordRe(String passwordRe) {
        this.passwordRe = passwordRe;
    }

    /**
     * @return the bonusEnds
     */
    public String getBonusEnds() {
        return bonusEnds;
    }

    /**
     * @param bonusEnds the bonusEnds to set
     */
    public void setBonusEnds(String bonusEnds) {
        this.bonusEnds = bonusEnds;
    }

    /**
     * @return the rewardId
     */
    public Integer getRewardId() {
        return rewardId;
    }

    /**
     * @param rewardId the rewardId to set
     */
    public void setRewardId(Integer rewardId) {
        this.rewardId = rewardId;
    }

    /**
     * @return the reward
     */
    public Double getReward() {
        return reward;
    }

    /**
     * @param reward the reward to set
     */
    public void setReward(Double reward) {
        this.reward = reward;
    }

    /**
     * @return the rewardStarts
     */
    public String getRewardStarts() {
        return rewardStarts;
    }

    /**
     * @param rewardStarts the rewardStarts to set
     */
    public void setRewardStarts(String rewardStarts) {
        this.rewardStarts = rewardStarts;
    }

    /**
     * @return the rewardEnds
     */
    public String getRewardEnds() {
        return rewardEnds;
    }

    /**
     * @param rewardEnds the rewardEnds to set
     */
    public void setRewardEnds(String rewardEnds) {
        this.rewardEnds = rewardEnds;
    }


   
}
