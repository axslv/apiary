/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.entities;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 *
 * @author jm
 */
@XStreamAlias("CourseEntry")
public class CourseEntry implements Serializable {

    private Integer courseId, domainId, departmentId, paymentId;
    private String courseName, domainTitle, departmentTitle;
    private String department, tjComment;
    private String groupAbbr;
    private String billPrice, currentPrice;
    private String priceFirstLvl;
    private String priceSecondLvl;
    private String description, descriptionLv, descriptionEn, pvn;
    private String certNr;
    private String address;
    private String room;
    private String certId;
    private String validity;
    private String auditor;
    private String stcwCodes;
    private Integer length;
    private Integer hours;
    private Integer rank;
    private String attAbbr;
    private String result;
    private Integer type;
    private Integer billRank, linkId;
    private String domain, exam, hoursDisp;
    private String event = "noEvent";
    

    public CourseEntry() {
    }

    /**
     * @return the courseId
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * @param courseId the courseId to set
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    /**
     * @return the courseName
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * @param courseName the courseName to set
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return the groupAbbr
     */
    public String getGroupAbbr() {
        return groupAbbr;
    }

    /**
     * @param groupAbbr the groupAbbr to set
     */
    public void setGroupAbbr(String groupAbbr) {
        this.groupAbbr = groupAbbr;
    }

    /**
     * @return the currentPrice
     */
    public String getCurrentPrice() {
        return currentPrice;
    }

    /**
     * @param currentPrice the currentPrice to set
     */
    public void setCurrentPrice(String currentPrice) {
        this.currentPrice = currentPrice;
    }

    /**
     * @return the priceFirstLvl
     */
    public String getPriceFirstLvl() {
        return priceFirstLvl;
    }

    /**
     * @param priceFirstLvl the priceFirstLvl to set
     */
    public void setPriceFirstLvl(String priceFirstLvl) {
        this.priceFirstLvl = priceFirstLvl;
    }

    /**
     * @return the priceSecondLvl
     */
    public String getPriceSecondLvl() {
        return priceSecondLvl;
    }

    /**
     * @param priceSecondLvl the priceSecondLvl to set
     */
    public void setPriceSecondLvl(String priceSecondLvl) {
        this.priceSecondLvl = priceSecondLvl;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the descriptionLv
     */
    public String getDescriptionLv() {
        return descriptionLv;
    }

    /**
     * @param descriptionLv the descriptionLv to set
     */
    public void setDescriptionLv(String descriptionLv) {
        this.descriptionLv = descriptionLv;
    }

    /**
     * @return the descriptionEn
     */
    public String getDescriptionEn() {
        return descriptionEn;
    }

    /**
     * @param descriptionEn the descriptionEn to set
     */
    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    /**
     * @return the certNr
     */
    public String getCertNr() {
        return certNr;
    }

    /**
     * @param certNr the certNr to set
     */
    public void setCertNr(String certNr) {
        this.certNr = certNr;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the room
     */
    public String getRoom() {
        return room;
    }

    /**
     * @param room the room to set
     */
    public void setRoom(String room) {
        this.room = room;
    }

    /**
     * @return the certId
     */
    public String getCertId() {
        return certId;
    }

    /**
     * @param certId the certId to set
     */
    public void setCertId(String certId) {
        this.certId = certId;
    }

    /**
     * @return the validity
     */
    public String getValidity() {
        return validity;
    }

    /**
     * @param validity the validity to set
     */
    public void setValidity(String validity) {
        this.validity = validity;
    }

    /**
     * @return the auditor
     */
    public String getAuditor() {
        return auditor;
    }

    /**
     * @param auditor the auditor to set
     */
    public void setAuditor(String auditor) {
        this.auditor = auditor;
    }

    /**
     * @return the stcwCodes
     */
    public String getStcwCodes() {
        return stcwCodes;
    }

    /**
     * @param stcwCodes the stcwCodes to set
     */
    public void setStcwCodes(String stcwCodes) {
        this.stcwCodes = stcwCodes;
    }

    /**
     * @return the length
     */
    public Integer getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(Integer length) {
        this.length = length;
    }

    /**
     * @return the hours
     */
    public Integer getHours() {
        return hours;
    }

    /**
     * @param hours the hours to set
     */
    public void setHours(Integer hours) {
        this.hours = hours;
        BigDecimal df = BigDecimal.valueOf(hours.doubleValue());
        DecimalFormat fmt = new DecimalFormat(".00");
        this.hoursDisp = fmt.format(df.multiply(new BigDecimal(0.1)));
    }

    /**
     * @return the rank
     */
    public Integer getRank() {
        return rank;
    }

    /**
     * @param rank the rank to set
     */
    public void setRank(Integer rank) {
        this.rank = rank;
    }


    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(String result) {
        this.result = result;
    }

    /**
     * @return the type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return the billRank
     */
    public Integer getBillRank() {
        return billRank;
    }

    /**
     * @param billRank the billRank to set
     */
    public void setBillRank(Integer billRank) {
        this.billRank = billRank;
    }

    /**
     * @return the domain
     */
    public String getDomain() {
        return domain;
    }

    /**
     * @param domain the domain to set
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     * @return the attAbbr
     */
    public String getAttAbbr() {
        return attAbbr;
    }

    /**
     * @param attAbbr the attAbbr to set
     */
    public void setAttAbbr(String attAbbr) {
        this.attAbbr = attAbbr;
    }

    /**
     * @return the exam
     */
    public String getExam() {
        return exam;
    }

    /**
     * @param exam the exam to set
     */
    public void setExam(String exam) {
        this.exam = exam;
    }

    /**
     * @return the linkId
     */
    public Integer getLinkId() {
        return linkId;
    }

    /**
     * @param linkId the linkId to set
     */
    public void setLinkId(Integer linkId) {
        this.linkId = linkId;
    }

    /**
     * @return the hoursDisp
     */
    public String getHoursDisp() {
        return hoursDisp;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @return the domainId
     */
    public Integer getDomainId() {
        return domainId;
    }

    /**
     * @param domainId the domainId to set
     */
    public void setDomainId(Integer domainId) {
        this.domainId = domainId;
    }

    /**
     * @return the departmentId
     */
    public Integer getDepartmentId() {
        return departmentId;
    }

    /**
     * @param departmentId the departmentId to set
     */
    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    /**
     * @return the domainTitle
     */
    public String getDomainTitle() {
        return domainTitle;
    }

    /**
     * @param domainTitle the domainTitle to set
     */
    public void setDomainTitle(String domainTitle) {
        this.domainTitle = domainTitle;
    }

    /**
     * @return the departmentTitle
     */
    public String getDepartmentTitle() {
        return departmentTitle;
    }

    /**
     * @param departmentTitle the departmentTitle to set
     */
    public void setDepartmentTitle(String departmentTitle) {
        this.departmentTitle = departmentTitle;
    }

    /**
     * @return the paymentId
     */
    public Integer getPaymentId() {
        return paymentId;
    }

    /**
     * @param paymentId the paymentId to set
     */
    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    /**
     * @return the tjComment
     */
    public String getTjComment() {
        return tjComment;
    }

    /**
     * @param tjComment the tjComment to set
     */
    public void setTjComment(String tjComment) {
        this.tjComment = tjComment;
    }

    /**
     * @return the billPrice
     */
    public String getBillPrice() {
        return billPrice;
    }

    /**
     * @param billPrice the billPrice to set
     */
    public void setBillPrice(String billPrice) {
        this.billPrice = billPrice;
    }

    /**
     * @return the pvn
     */
    public String getPvn() {
        return pvn;
    }

    /**
     * @param pvn the pvn to set
     */
    public void setPvn(String pvn) {
        this.pvn = pvn;
    }
}
