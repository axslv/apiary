/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mj
 */
public class CourseIncomeQuery {
    private List<Integer> courseIds = new ArrayList<Integer>();
    private List<String> domains = new ArrayList<String>();
    private List<String> departments = new ArrayList<String>();
    private List<String> courseNames = new ArrayList<String>();
    private String dateStart, dateEnd, currency, domain;
    
    
    public CourseIncomeQuery() {
        
    }

    /**
     * @return the courseIds
     */
    public List<Integer> getCourseIds() {
        return courseIds;
    }

    /**
     * @param courseIds the courseIds to set
     */
    public void setCourseIds(List<Integer> courseIds) {
        this.courseIds = courseIds;
    }

    /**
     * @return the dateStart
     */
    public String getDateStart() {
        return dateStart;
    }

    /**
     * @param dateStart the dateStart to set
     */
    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    /**
     * @return the dateEnd
     */
    public String getDateEnd() {
        return dateEnd;
    }

    /**
     * @param dateEnd the dateEnd to set
     */
    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the domains
     */
    public List<String> getDomains() {
        return domains;
    }

    /**
     * @param domains the domains to set
     */
    public void setDomains(List<String> domains) {
        this.domains = domains;
    }

    /**
     * @return the domain
     */
    public String getDomain() {
        return domain;
    }

    /**
     * @param domain the domain to set
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     * @return the departments
     */
    public List<String> getDepartments() {
        return departments;
    }

    /**
     * @param departments the departments to set
     */
    public void setDepartments(List<String> departments) {
        this.departments = departments;
    }

    /**
     * @return the courseNames
     */
    public List<String> getCourseNames() {
        return courseNames;
    }

    /**
     * @param courseNames the courseNames to set
     */
    public void setCourseNames(List<String> courseNames) {
        this.courseNames = courseNames;
    }

   

    
}
