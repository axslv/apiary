package lv.nmc.entities;



import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

@XStreamAlias("RegistrationEntry")
public class RegistrationEntry implements Serializable {
    private Integer registrationId, groupId, companyId, paymentId;
    private Integer certPrinted, regPrinted, finPrinted;
    private Integer banned;
    private Integer userId;
    private Integer courseId;
    private String nameSurname, company, address, phone, delayDate, crewComment, paymentsComment;
    private String personCode;
    private String dateStart, dateEnd, instructor;
    private String dateC;
    private String groupName, price, marineBook, rank, exam, certName, certNr;
    private Long delayTime;
    private String event = "noEvent";
    private boolean toRegister;
    private boolean certGiven;
    private Integer certGivenL;
    private Integer paymentsOkL;
    private boolean paymentsOk;


    
    public RegistrationEntry() {
        
    }

    /**
     * @return the registrationId
     */
    public Integer getRegistrationId() {
        return registrationId;
    }

    /**
     * @param registrationId the registrationId to set
     */
    public void setRegistrationId(Integer registrationId) {
        this.registrationId = registrationId;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the courseId
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * @param courseId the courseId to set
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    /**
     * @return the nameSurname
     */
    public String getNameSurname() {
        return nameSurname;
    }

    /**
     * @param nameSurname the nameSurname to set
     */
    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    /**
     * @return the personCode
     */
    public String getPersonCode() {
        return personCode;
    }

    /**
     * @param personCode the personCode to set
     */
    public void setPersonCode(String personCode) {
        this.personCode = personCode;
    }

    /**
     * @return the dateStart
     */
    public String getDateStart() {
        return dateStart;
    }

    /**
     * @param dateStart the dateStart to set
     */
    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the dateC
     */
    public String getDateC() {
        return dateC;
    }

    /**
     * @param dateC the dateC to set
     */
    public void setDateC(String dateC) {
        this.dateC = dateC;
    }

    /**
     * @return the groupId
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * @param groupId the groupId to set
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    /**
     * @return the price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * @return the marineBook
     */
    public String getMarineBook() {
        return marineBook;
    }

    /**
     * @param marineBook the marineBook to set
     */
    public void setMarineBook(String marineBook) {
        this.marineBook = marineBook;
    }

    /**
     * @return the rank
     */
    public String getRank() {
        return rank;
    }

    /**
     * @param rank the rank to set
     */
    public void setRank(String rank) {
        this.rank = rank;
    }

    /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(String company) {
        this.company = company;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the delayTime
     */
    public Long getDelayTime() {
        return delayTime;
    }

    /**
     * @param delayTime the delayTime to set
     */
    public void setDelayTime(Long delayTime) {
        this.delayTime = delayTime;
    }

    /**
     * @return the dateEnd
     */
    public String getDateEnd() {
        return dateEnd;
    }

    /**
     * @param dateEnd the dateEnd to set
     */
    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    /**
     * @return the instructor
     */
    public String getInstructor() {
        return instructor;
    }

    /**
     * @param instructor the instructor to set
     */
    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }

    /**
     * @return the exam
     */
    public String getExam() {
        return exam;
    }

    /**
     * @param exam the exam to set
     */
    public void setExam(String exam) {
        this.exam = exam;
    }

    /**
     * @return the certPrinted
     */
    public Integer getCertPrinted() {
        return certPrinted;
    }

    /**
     * @param certPrinted the certPrinted to set
     */
    public void setCertPrinted(Integer certPrinted) {
        this.certPrinted = certPrinted;
    }

    /**
     * @return the banned
     */
    public Integer getBanned() {
        return banned;
    }

    /**
     * @param banned the banned to set
     */
    public void setBanned(Integer banned) {
        this.banned = banned;
    }

    /**
     * @return the delayDate
     */
    public String getDelayDate() {
        return delayDate;
    }

    /**
     * @param delayDate the delayDate to set
     */
    public void setDelayDate(String delayDate) {
        this.delayDate = delayDate;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @return the regPrinted
     */
    public Integer getRegPrinted() {
        return regPrinted;
    }

    /**
     * @param regPrinted the regPrinted to set
     */
    public void setRegPrinted(Integer regPrinted) {
        this.regPrinted = regPrinted;
    }

    /**
     * @return the finPrinted
     */
    public Integer getFinPrinted() {
        return finPrinted;
    }

    /**
     * @param finPrinted the finPrinted to set
     */
    public void setFinPrinted(Integer finPrinted) {
        this.finPrinted = finPrinted;
    }

    /**
     * @return the crewComment
     */
    public String getCrewComment() {
        return crewComment;
    }

    /**
     * @param crewComment the crewComment to set
     */
    public void setCrewComment(String crewComment) {
        this.crewComment = crewComment;
    }

    /**
     * @return the certName
     */
    public String getCertName() {
        return certName;
    }

    /**
     * @param certName the certName to set
     */
    public void setCertName(String certName) {
        this.certName = certName;
    }

    /**
     * @return the toRegister
     */
    public Boolean isToRegister() {
        return toRegister;
    }
    /**
     * @param toRegister the toRegister to set
     */
    public void setToRegister(Boolean toRegister) {
        this.toRegister = toRegister;
    }

    /**
     * @return the companyId
     */
    public Integer getCompanyId() {
        return companyId;
    }

    /**
     * @param companyId the companyId to set
     */
    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    /**
     * @return the certGiven
     */
    public boolean isCertGiven() {
        return certGiven;
    }

    /**
     * @param certGiven the certGiven to set
     */
    public void setCertGiven(boolean certGiven) {
        this.certGiven = certGiven;
        if (certGiven) {
            this.certGivenL = 1;
        }
    }

    /**
     * @return the certGivenL
     */
    public Integer getCertGivenL() {
        return certGivenL;
    }

    /**
     * @param certGivenL the certGivenL to set
     */
    public void setCertGivenL(Integer certGivenL) {
        this.certGivenL = certGivenL;
        if (certGivenL.equals(1)) {
            this.certGiven = true;
        }
    }
    
      /**
     * @return the paymentsOkL
     */
    public Integer getPaymentsOkL() {
        return paymentsOkL;
    }

    /**
     * @param paymentsOkL the paymentsOkL to set
     */
    public void setPaymentsOkL(Integer paymentsOkL) {
        this.paymentsOkL = paymentsOkL;
        if (new Integer(1).equals(paymentsOkL)) {
            this.paymentsOk = true;
        }
    }

    /**
     * @return the paymentsOk
     */
    public boolean isPaymentsOk() {
        return paymentsOk;
    }

    /**
     * @param paymentsOk the paymentsOk to set
     */
    public void setPaymentsOk(boolean paymentsOk) {
        this.paymentsOkL = 0;
        this.paymentsOk = paymentsOk;
        if (paymentsOk) {
            this.paymentsOkL = 1;
        }
    }

    /**
     * @return the paymentsComment
     */
    public String getPaymentsComment() {
        return paymentsComment;
    }

    /**
     * @param paymentsComment the paymentsComment to set
     */
    public void setPaymentsComment(String paymentsComment) {
        this.paymentsComment = paymentsComment;
    }

    /**
     * @return the paymentId
     */
    public Integer getPaymentId() {
        return paymentId;
    }

    /**
     * @param paymentId the paymentId to set
     */
    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    /**
     * @return the certNr
     */
    public String getCertNr() {
        return certNr;
    }

    /**
     * @param certNr the certNr to set
     */
    public void setCertNr(String certNr) {
        this.certNr = certNr;
    }




}
