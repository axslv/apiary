/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.entities;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author mj
 */
@XStreamAlias("CertEntry")
public class CertEntry {
    private String certNr, nameSurname, issueDate, validDate, courseName, dateStart, dateEnd;
    private String oldCertNr, oldIssuer, oldDateIssued, personCode;
    private Integer certId, courseId, groupId;

    /**
     * @return the certNr
     */
    public String getCertNr() {
        return certNr;
    }

    /**
     * @param certNr the certNr to set
     */
    public void setCertNr(String certNr) {
        this.certNr = certNr;
    }

    /**
     * @return the nameSurname
     */
    public String getNameSurname() {
        return nameSurname;
    }

    /**
     * @param nameSurname the nameSurname to set
     */
    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    /**
     * @return the issueDate
     */
    public String getIssueDate() {
        return issueDate;
    }

    /**
     * @param issueDate the issueDate to set
     */
    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * @return the validDate
     */
    public String getValidDate() {
        return validDate;
    }

    /**
     * @param validDate the validDate to set
     */
    public void setValidDate(String validDate) {
        this.validDate = validDate;
    }

    /**
     * @return the courseId
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * @param courseId the courseId to set
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    /**
     * @return the groupId
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * @param groupId the groupId to set
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    /**
     * @return the courseName
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * @param courseName the courseName to set
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /**
     * @return the dateStart
     */
    public String getDateStart() {
        return dateStart;
    }

    /**
     * @param dateStart the dateStart to set
     */
    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    /**
     * @return the certId
     */
    public Integer getCertId() {
        return certId;
    }

    /**
     * @param certId the certId to set
     */
    public void setCertId(Integer certId) {
        this.certId = certId;
    }

    /**
     * @return the dateEnd
     */
    public String getDateEnd() {
        return dateEnd;
    }

    /**
     * @param dateEnd the dateEnd to set
     */
    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    /**
     * @return the oldCertNr
     */
    public String getOldCertNr() {
        return oldCertNr;
    }

    /**
     * @param oldCertNr the oldCertNr to set
     */
    public void setOldCertNr(String oldCertNr) {
        this.oldCertNr = oldCertNr;
    }

    /**
     * @return the oldIssuer
     */
    public String getOldIssuer() {
        return oldIssuer;
    }

    /**
     * @param oldIssuer the oldIssuer to set
     */
    public void setOldIssuer(String oldIssuer) {
        this.oldIssuer = oldIssuer;
    }

    /**
     * @return the oldDateIssued
     */
    public String getOldDateIssued() {
        return oldDateIssued;
    }

    /**
     * @param oldDateIssued the oldDateIssued to set
     */
    public void setOldDateIssued(String oldDateIssued) {
        this.oldDateIssued = oldDateIssued;
    }

    /**
     * @return the personCode
     */
    public String getPersonCode() {
        return personCode;
    }

    /**
     * @param personCode the personCode to set
     */
    public void setPersonCode(String personCode) {
        this.personCode = personCode;
    }

    
}
