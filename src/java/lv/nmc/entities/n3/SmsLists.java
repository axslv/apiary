/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.entities.n3;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jm
 */
public class SmsLists {
    private List<String> ranks = new ArrayList<>();
    private List<String> ccodes = new ArrayList<>();

    /**
     * @return the ranks
     */
    public List<String> getRanks() {
        return ranks;
    }

    /**
     * @param ranks the ranks to set
     */
    public void setRanks(List<String> ranks) {
        this.ranks = ranks;
    }

    /**
     * @return the ccodes
     */
    public List<String> getCcodes() {
        return ccodes;
    }

    /**
     * @param ccodes the ccodes to set
     */
    public void setCcodes(List<String> ccodes) {
        this.ccodes = ccodes;
    }
    
}
