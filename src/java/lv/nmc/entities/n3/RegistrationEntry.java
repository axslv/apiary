package lv.nmc.entities.n3;



import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

@XStreamAlias("RegistrationEntry")
public class RegistrationEntry implements Serializable {
    private Integer registrationId, groupId, companyId, paymentId, documentId, recordId;
    private Integer certPrinted, regPrinted, finPrinted = 0;
    private Integer banned = 0;
    private Integer userId;
    private Integer courseId;
    private String nameSurname, company, address, phone, delayDate, examState, crewComment, paymentsComment, documentNr, documentName, issuedBy, issueDate, contractNr;
    private String personCode;
    private String dateStart, dateEnd, instructor, room, timeStart, validTill;
    private String dateC;
    private String groupName, price, cPrice, marineBook, rank, exam, certName="NO CERT", courseName;
    private Long delayTime;
    private String event = "noEvent";
    private boolean toRegister = true;
    private boolean certGiven = false;
    private Integer certGivenL = 0;
    private Integer paymentsOkL;
    private boolean paymentsOk;
    private String certDescriptionLv, certDescriptionEn, certHeaderLv, certHeaderEn, ljaTitle, stcwCodes, template, source;


    
    public RegistrationEntry() {
        
    }

    /**
     * @return the registrationId
     */
    public Integer getRegistrationId() {
        return registrationId;
    }

    /**
     * @param registrationId the registrationId to set
     */
    public void setRegistrationId(Integer registrationId) {
        this.registrationId = registrationId;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the courseId
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * @param courseId the courseId to set
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    /**
     * @return the nameSurname
     */
    public String getNameSurname() {
        return nameSurname;
    }

    /**
     * @param nameSurname the nameSurname to set
     */
    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    /**
     * @return the personCode
     */
    public String getPersonCode() {
        return personCode;
    }

    /**
     * @param personCode the personCode to set
     */
    public void setPersonCode(String personCode) {
        this.personCode = personCode;
    }

    /**
     * @return the dateStart
     */
    public String getDateStart() {
        return dateStart;
    }

    /**
     * @param dateStart the dateStart to set
     */
    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the dateC
     */
    public String getDateC() {
        return dateC;
    }

    /**
     * @param dateC the dateC to set
     */
    public void setDateC(String dateC) {
        this.dateC = dateC;
    }

    /**
     * @return the groupId
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * @param groupId the groupId to set
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    /**
     * @return the price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * @return the marineBook
     */
    public String getMarineBook() {
        return marineBook;
    }

    /**
     * @param marineBook the marineBook to set
     */
    public void setMarineBook(String marineBook) {
        this.marineBook = marineBook;
    }

    /**
     * @return the rank
     */
    public String getRank() {
        return rank;
    }

    /**
     * @param rank the rank to set
     */
    public void setRank(String rank) {
        this.rank = rank;
    }

    /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(String company) {
        this.company = company;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the delayTime
     */
    public Long getDelayTime() {
        return delayTime;
    }

    /**
     * @param delayTime the delayTime to set
     */
    public void setDelayTime(Long delayTime) {
        this.delayTime = delayTime;
    }

    /**
     * @return the dateEnd
     */
    public String getDateEnd() {
        return dateEnd;
    }

    /**
     * @param dateEnd the dateEnd to set
     */
    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    /**
     * @return the instructor
     */
    public String getInstructor() {
        return instructor;
    }

    /**
     * @param instructor the instructor to set
     */
    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }

    /**
     * @return the exam
     */
    public String getExam() {
        return exam;
    }

    /**
     * @param exam the exam to set
     */
    public void setExam(String exam) {
        this.exam = exam;
    }

    /**
     * @return the certPrinted
     */
    public Integer getCertPrinted() {
        return certPrinted;
    }

    /**
     * @param certPrinted the certPrinted to set
     */
    public void setCertPrinted(Integer certPrinted) {
        this.certPrinted = certPrinted;
    }

    /**
     * @return the banned
     */
    public Integer getBanned() {
        return banned;
    }

    /**
     * @param banned the banned to set
     */
    public void setBanned(Integer banned) {
        this.banned = banned;
    }

    /**
     * @return the delayDate
     */
    public String getDelayDate() {
        return delayDate;
    }

    /**
     * @param delayDate the delayDate to set
     */
    public void setDelayDate(String delayDate) {
        this.delayDate = delayDate;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @return the regPrinted
     */
    public Integer getRegPrinted() {
        return regPrinted;
    }

    /**
     * @param regPrinted the regPrinted to set
     */
    public void setRegPrinted(Integer regPrinted) {
        this.regPrinted = regPrinted;
    }

    /**
     * @return the finPrinted
     */
    public Integer getFinPrinted() {
        return finPrinted;
    }

    /**
     * @param finPrinted the finPrinted to set
     */
    public void setFinPrinted(Integer finPrinted) {
        this.finPrinted = finPrinted;
    }

    /**
     * @return the crewComment
     */
    public String getCrewComment() {
        return crewComment;
    }

    /**
     * @param crewComment the crewComment to set
     */
    public void setCrewComment(String crewComment) {
        this.crewComment = crewComment;
    }

    /**
     * @return the certName
     */
    public String getCertName() {
        return certName;
    }

    /**
     * @param certName the certName to set
     */
    public void setCertName(String certName) {
        this.certName = certName;
    }

    /**
     * @return the toRegister
     */
    public Boolean isToRegister() {
        return toRegister;
    }
    /**
     * @param toRegister the toRegister to set
     */
    public void setToRegister(Boolean toRegister) {
        this.toRegister = toRegister;
    }

    /**
     * @return the companyId
     */
    public Integer getCompanyId() {
        return companyId;
    }

    /**
     * @param companyId the companyId to set
     */
    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    /**
     * @return the certGiven
     */
    public boolean isCertGiven() {
        return certGiven;
    }

    /**
     * @param certGiven the certGiven to set
     */
    public void setCertGiven(boolean certGiven) {
        this.certGiven = certGiven;
        if (certGiven) {
            this.certGivenL = 1;
        }
    }

    /**
     * @return the certGivenL
     */
    public Integer getCertGivenL() {
        return certGivenL;
    }

    /**
     * @param certGivenL the certGivenL to set
     */
    public void setCertGivenL(Integer certGivenL) {
        this.certGivenL = certGivenL;
        if (certGivenL.equals(1)) {
            this.certGiven = true;
        }
    }
    
      /**
     * @return the paymentsOkL
     */
    public Integer getPaymentsOkL() {
        return paymentsOkL;
    }

    /**
     * @param paymentsOkL the paymentsOkL to set
     */
    public void setPaymentsOkL(Integer paymentsOkL) {
        this.paymentsOkL = paymentsOkL;
        if (new Integer(1).equals(paymentsOkL)) {
            this.paymentsOk = true;
        }
    }

    /**
     * @return the paymentsOk
     */
    public boolean isPaymentsOk() {
        return paymentsOk;
    }

    /**
     * @param paymentsOk the paymentsOk to set
     */
    public void setPaymentsOk(boolean paymentsOk) {
        this.paymentsOkL = 0;
        this.paymentsOk = paymentsOk;
        if (paymentsOk) {
            this.paymentsOkL = 1;
        }
    }

    /**
     * @return the paymentsComment
     */
    public String getPaymentsComment() {
        return paymentsComment;
    }

    /**
     * @param paymentsComment the paymentsComment to set
     */
    public void setPaymentsComment(String paymentsComment) {
        this.paymentsComment = paymentsComment;
    }

    /**
     * @return the paymentId
     */
    public Integer getPaymentId() {
        return paymentId;
    }

    /**
     * @param paymentId the paymentId to set
     */
    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    /**
     * @return the courseName
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * @param courseName the courseName to set
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /**
     * @return the room
     */
    public String getRoom() {
        return room;
    }

    /**
     * @param room the room to set
     */
    public void setRoom(String room) {
        this.room = room;
    }

    /**
     * @return the timeStart
     */
    public String getTimeStart() {
        return timeStart;
    }

    /**
     * @param timeStart the timeStart to set
     */
    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    /**
     * @return the examState
     */
    public String getExamState() {
        return examState;
    }

    /**
     * @param examState the examState to set
     */
    public void setExamState(String examState) {
        this.examState = examState;
    }

    /**
     * @return the cPrice
     */
    public String getcPrice() {
        return cPrice;
    }

    /**
     * @param cPrice the cPrice to set
     */
    public void setcPrice(String cPrice) {
        this.cPrice = cPrice;
    }

    /**
     * @return the documentId
     */
    public Integer getDocumentId() {
        return documentId;
    }

    /**
     * @param documentId the documentId to set
     */
    public void setDocumentId(Integer documentId) {
        this.documentId = documentId;
    }

    /**
     * @return the documentNr
     */
    public String getDocumentNr() {
        return documentNr;
    }

    /**
     * @param documentNr the documentNr to set
     */
    public void setDocumentNr(String documentNr) {
        this.documentNr = documentNr;
    }

    /**
     * @return the documentName
     */
    public String getDocumentName() {
        return documentName;
    }

    /**
     * @param documentName the documentName to set
     */
    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    /**
     * @return the validTill
     */
    public String getValidTill() {
        return validTill;
    }

    /**
     * @param validTill the validTill to set
     */
    public void setValidTill(String validTill) {
        this.validTill = validTill;
    }

    /**
     * @return the contractNr
     */
    public String getContractNr() {
        return contractNr;
    }

    /**
     * @param contractNr the contractNr to set
     */
    public void setContractNr(String contractNr) {
        this.contractNr = contractNr;
    }

    /**
     * @return the certDescriptionLv
     */
    public String getCertDescriptionLv() {
        return certDescriptionLv;
    }

    /**
     * @param certDescriptionLv the certDescriptionLv to set
     */
    public void setCertDescriptionLv(String certDescriptionLv) {
        this.certDescriptionLv = certDescriptionLv;
    }

    /**
     * @return the certDescriptionEn
     */
    public String getCertDescriptionEn() {
        return certDescriptionEn;
    }

    /**
     * @param certDescriptionEn the certDescriptionEn to set
     */
    public void setCertDescriptionEn(String certDescriptionEn) {
        this.certDescriptionEn = certDescriptionEn;
    }

    /**
     * @return the certHeaderLv
     */
    public String getCertHeaderLv() {
        return certHeaderLv;
    }

    /**
     * @param certHeaderLv the certHeaderLv to set
     */
    public void setCertHeaderLv(String certHeaderLv) {
        this.certHeaderLv = certHeaderLv;
    }

    /**
     * @return the certHeaderEn
     */
    public String getCertHeaderEn() {
        return certHeaderEn;
    }

    /**
     * @param certHeaderEn the certHeaderEn to set
     */
    public void setCertHeaderEn(String certHeaderEn) {
        this.certHeaderEn = certHeaderEn;
    }

    /**
     * @return the ljaTitle
     */
    public String getLjaTitle() {
        return ljaTitle;
    }

    /**
     * @param ljaTitle the ljaTitle to set
     */
    public void setLjaTitle(String ljaTitle) {
        this.ljaTitle = ljaTitle;
    }

    /**
     * @return the stcwCodes
     */
    public String getStcwCodes() {
        return stcwCodes;
    }

    /**
     * @param stcwCodes the stcwCodes to set
     */
    public void setStcwCodes(String stcwCodes) {
        this.stcwCodes = stcwCodes;
    }

    /**
     * @return the template
     */
    public String getTemplate() {
        return template;
    }

    /**
     * @param template the template to set
     */
    public void setTemplate(String template) {
        this.template = template;
    }

    /**
     * @return the issuedBy
     */
    public String getIssuedBy() {
        return issuedBy;
    }

    /**
     * @param issuedBy the issuedBy to set
     */
    public void setIssuedBy(String issuedBy) {
        this.issuedBy = issuedBy;
    }

    /**
     * @return the issueDate
     */
    public String getIssueDate() {
        return issueDate;
    }

    /**
     * @param issueDate the issueDate to set
     */
    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * @return the recordId
     */
    public Integer getRecordId() {
        return recordId;
    }

    /**
     * @param recordId the recordId to set
     */
    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }




}
