/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.entities.n3;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;


@XStreamAlias("Payment")

public class Payment implements Serializable {

    private Integer paymentId, userId, courseId, groupId, registrationId, advanceId, companyId, paidId, active = 1, issued, banned = 0;
    private String company, crewComment, courseComment, advComment, bank, billNr,
            advanceComment, ppComment, paymentSource, nameSurname, personCode, courseName, billDate, dateStart, domain;
    private String currency;
    private String displayPrice, paymentDate, dateEnd, advanceSum, paidSum, sumToPay, rank, certNr, banDate, list, vessel, auxInfo;
    private String event;
    private String voucher = "-";
    private Boolean crtIssued = false;

    //private String date


    /**
     * @return the paymentId
     */
    public Integer getPaymentId() {
        return paymentId;
    }

    /**
     * @param paymentId the paymentId to set
     */
    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the courseId
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * @param courseId the courseId to set
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    /**
     * @return the groupId
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * @param groupId the groupId to set
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(String company) {
        this.company = company;
    }

    /**
     * @return the crewComment
     */
    public String getCrewComment() {
        return crewComment;
    }

    /**
     * @param crewComment the crewComment to set
     */
    public void setCrewComment(String crewComment) {
        this.crewComment = crewComment;
    }

    /**
     * @return the registrationId
     */
    public Integer getRegistrationId() {
        return registrationId;
    }

    /**
     * @param registrationId the registrationId to set
     */
    public void setRegistrationId(Integer registrationId) {
        this.registrationId = registrationId;
    }

    /**
     * @return the courseComment
     */
    public String getCourseComment() {
        return courseComment;
    }

    /**
     * @param courseComment the courseComment to set
     */
    public void setCourseComment(String courseComment) {
        this.courseComment = courseComment;
    }

    /**
     * @return the bank
     */
    public String getBank() {
        return bank;
    }

    /**
     * @param bank the bank to set
     */
    public void setBank(String bank) {
        this.bank = bank;
    }

    /**
     * @return the voucher
     */
    public String getVoucher() {
        return voucher;
    }

    /**
     * @param voucher the voucher to set
     */
    public void setVoucher(String voucher) {  
               
        if (null == voucher) {
            this.voucher = "n/a";
            return;
        }
        
        voucher = voucher.replace(" ", ""); //BEWARE!!! Thin ice! It's not your usual space :D        
        this.voucher = ("00000000" + voucher).substring(voucher.length());
    }

    /**
     * @return the billNr
     */
    public String getBillNr() {
        return billNr;
    }

    /**
     * @param billNr the billNr to set
     */
    public void setBillNr(String billNr) {
        this.billNr = billNr;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the displayPrice
     */
    public String getDisplayPrice() {
        return displayPrice;
    }

    /**
     * @param displayPrice the displayPrice to set
     */
    public void setDisplayPrice(String displayPrice) {
        this.displayPrice = displayPrice;
    }

    /**
     * @return the advanceId
     */
    public Integer getAdvanceId() {
        return advanceId;
    }

    /**
     * @param advanceId the advanceId to set
     */
    public void setAdvanceId(Integer advanceId) {
        this.advanceId = advanceId;
    }

    /**
     * @return the advanceComment
     */
    public String getAdvanceComment() {
        return advanceComment;
    }

    /**
     * @param advanceComment the advanceComment to set
     */
    public void setAdvanceComment(String advanceComment) {
        this.advanceComment = advanceComment;
    }

    /**
     * @return the paymentDate
     */
    public String getPaymentDate() {
        return paymentDate;
    }

    /**
     * @param paymentDate the paymentDate to set
     */
    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    /**
     * @return the dateEnd
     */
    public String getDateEnd() {
        return dateEnd;
    }

    /**
     * @param dateEnd the dateEnd to set
     */
    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    /**
     * @return the advanceSum
     */
    public String getAdvanceSum() {
        return advanceSum;
    }

    /**
     * @param advanceSum the advanceSum to set
     */
    public void setAdvanceSum(String advanceSum) {
        this.advanceSum = advanceSum;
    }

    /**
     * @return the paymentSource
     */
    public String getPaymentSource() {
        return paymentSource;
    }

    /**
     * @param paymentSource the paymentSource to set
     */
    public void setPaymentSource(String paymentSource) {
        this.paymentSource = paymentSource;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @return the nameSurname
     */
    public String getNameSurname() {
        return nameSurname;
    }

    /**
     * @param nameSurname the nameSurname to set
     */
    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    /**
     * @return the personCode
     */
    public String getPersonCode() {
        return personCode;
    }

    /**
     * @param personCode the personCode to set
     */
    public void setPersonCode(String personCode) {
        this.personCode = personCode;
    }

    /**
     * @return the courseName
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * @param courseName the courseName to set
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /**
     * @return the companyId
     */
    public Integer getCompanyId() {
        return companyId;
    }

    /**
     * @param companyId the companyId to set
     */
    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    /**
     * @return the billDate
     */
    public String getBillDate() {
        return billDate;
    }

    /**
     * @param billDate the billDate to set
     */
    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    /**
     * @return the paidId
     */
    public Integer getPaidId() {
        return paidId;
    }

    /**
     * @param paidId the paidId to set
     */
    public void setPaidId(Integer paidId) {
        this.paidId = paidId;
    }

    /**
     * @return the paidSum
     */
    public String getPaidSum() {
        return paidSum;
    }

    /**
     * @param paidSum the paidSum to set
     */
    public void setPaidSum(String paidSum) {
        this.paidSum = paidSum;
    }

    /**
     * @return the advComment
     */
    public String getAdvComment() {
        return advComment;
    }

    /**
     * @param advComment the advComment to set
     */
    public void setAdvComment(String advComment) {
        this.advComment = advComment;
    }

    /**
     * @return the ppComment
     */
    public String getPpComment() {
        return ppComment;
    }

    /**
     * @param ppComment the ppComment to set
     */
    public void setPpComment(String ppComment) {
        this.ppComment = ppComment;
    }

    /**
     * @return the sumToPay
     */
    public String getSumToPay() {
        return sumToPay;
    }

    /**
     * @param sumToPay the sumToPay to set
     */
    public void setSumToPay(String sumToPay) {
        this.sumToPay = sumToPay;
    }

    /**
     * @return the active
     */
    public Integer getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Integer active) {
        this.active = active;
    }

    /**
     * @return the dateStart
     */
    public String getDateStart() {
        return dateStart;
    }

    /**
     * @param dateStart the dateStart to set
     */
    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    /**
     * @return the domain
     */
    public String getDomain() {
        return domain;
    }

    /**
     * @param domain the domain to set
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     * @return the issued
     */
    public Integer getIssued() {
        return issued;
    }

    /**
     * @param issued the issued to set
     */
    public void setIssued(Integer issued) {
        this.issued = issued;
        if (1 == issued) {
            this.crtIssued = true;
        }
    }

    /**
     * @return the crtIssued
     */
    public Boolean getCrtIssued() {
        return crtIssued;
    }

    /**
     * @param crtIssued the crtIssued to set
     */
    public void setCrtIssued(Boolean crtIssued) {
        this.crtIssued = crtIssued;

        if (crtIssued) {
            this.issued = 1;
            return;
        }
        this.issued = 0;

    }

    /**
     * @return the rank
     */
    public String getRank() {
        return rank;
    }

    /**
     * @param rank the rank to set
     */
    public void setRank(String rank) {
        this.rank = rank;
    }

    /**
     * @return the certNr
     */
    public String getCertNr() {
        return certNr;
    }

    /**
     * @param certNr the certNr to set
     */
    public void setCertNr(String certNr) {
        this.certNr = certNr;
    }
    /**
     * @return the banned
     */
    public Integer getBanned() {
        return banned;
    }

    /**
     * @return the banDate
     */
    public String getBanDate() {
        return banDate;
    }

    /**
     * @param banDate the banDate to set
     */
    public void setBanDate(String banDate) {
        this.banDate = banDate;
    }

    /**
     * @return the list
     */
    public String getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(String list) {
        this.list = list;
    }

    /**
     * @return the vessel
     */
    public String getVessel() {
        return vessel;
    }

    /**
     * @param vessel the vessel to set
     */
    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    /**
     * @return the auxInfo
     */
    public String getAuxInfo() {
        return auxInfo;
    }

    /**
     * @param auxInfo the auxInfo to set
     */
    public void setAuxInfo(String auxInfo) {
        this.auxInfo = auxInfo;
    }


  


}
