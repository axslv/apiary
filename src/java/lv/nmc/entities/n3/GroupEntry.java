/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.entities.n3;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jm
 */
@XStreamAlias("GroupEntry")
public class GroupEntry implements Serializable {

    private Integer courseId, groupId, instructorId, regPrintedL, finPrintedL, certPrintedL, instructorRecordId;
    private Integer assistant1, assistant2, assistant3, assistant4;
    private String assistant1Name, assistant2Name, assistant3Name, assistant4Name, instructorName;
    private boolean regPrinted, finPrinted, certPrinted;
    private String groupName, courseName, domain, department, dateStart, dateEnd, timeStart, room;
    private String event = "noEvent";
    private String instructorType;
    
    private List<Integer> courseIdsList = new ArrayList();
    

    public GroupEntry() {
    }

    /**
     * @return the courseId
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * @param courseId the courseId to set
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    /**
     * @return the groupId
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * @param groupId the groupId to set
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the domain
     */
    public String getDomain() {
        return domain;
    }

    /**
     * @param domain the domain to set
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return the dateStart
     */
    public String getDateStart() {
        return dateStart;
    }

    /**
     * @param dateStart the dateStart to set
     */
    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    /**
     * @return the dateEnd
     */
    public String getDateEnd() {
        return dateEnd;
    }

    /**
     * @param dateEnd the dateEnd to set
     */
    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @return the courseIdsList
     */
    public List<Integer> getCourseIdsList() {
        return courseIdsList;
    }

    /**
     * @return the courseName
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * @param courseName the courseName to set
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /**
     * @param courseIdsList the courseIdsList to set
     */
    public void setCourseIdsList(List<Integer> courseIdsList) {
        this.courseIdsList = courseIdsList;
    }

    /**
     * @return the regPrintedL
     */
    public Integer getRegPrintedL() {
        return regPrintedL;
    }

    /**
     * @param regPrintedL the regPrintedL to set
     */
    public void setRegPrintedL(Integer regPrintedL) {
        this.regPrintedL = regPrintedL;
        this.regPrinted = false;
        
        if (regPrintedL.equals(1)) {
            this.regPrinted = true;
        }
    }

    /**
     * @return the finPrintedL
     */
    public Integer getFinPrintedL() {
        return finPrintedL;
    }

    /**
     * @param finPrintedL the finPrintedL to set
     */
    public void setFinPrintedL(Integer finPrintedL) {
        this.finPrintedL = finPrintedL;
        this.finPrinted = false;
        
        if (finPrintedL.equals(1)) {
            this.finPrinted = true;
        }
    }

    /**
     * @return the certPrintedL
     */
    public Integer getCertPrintedL() {
        return certPrintedL;
    }

    /**
     * @param certPrintedL the certPrintedL to set
     */
    public void setCertPrintedL(Integer certPrintedL) {
        this.certPrintedL = certPrintedL;
        this.certPrinted = false;
        
        if (certPrintedL.equals(1)) {
            this.certPrinted = true;
        }
    }

    /**
     * @return the regPrinted
     */
    public boolean isRegPrinted() {
        return regPrinted;
    }

    /**
     * @param regPrinted the regPrinted to set
     */
    public void setRegPrinted(boolean regPrinted) {
        this.regPrinted = regPrinted;
        this.regPrintedL = 0;
        if (regPrinted) {
            this.regPrintedL = 1;
        }
    }

    /**
     * @return the finPrinted
     */
    public boolean isFinPrinted() {
        return finPrinted;
    }

    /**
     * @param finPrinted the finPrinted to set
     */
    public void setFinPrinted(boolean finPrinted) {
        this.finPrinted = finPrinted;
        this.finPrintedL = 0;
        if (finPrinted) {
            this.finPrintedL = 1;
        }
    }

    /**
     * @return the certPrinted
     */
    public boolean isCertPrinted() {
        return certPrinted;
    }

    /**
     * @param certPrinted the certPrinted to set
     */
    public void setCertPrinted(boolean certPrinted) {
        this.certPrinted = certPrinted;
        this.certPrintedL = 0;
        if (certPrinted) {
            this.certPrintedL = 1;
        }
    }

    /**
     * @return the instructorId
     */
    public Integer getInstructorId() {
        return instructorId;
    }

    /**
     * @param instructorId the instructorId to set
     */
    public void setInstructorId(Integer instructorId) {
        this.instructorId = instructorId;
    }

    /**
     * @return the instructorType
     */
    public String getInstructorType() {
        return instructorType;
    }

    /**
     * @param instructorType the instructorType to set
     */
    public void setInstructorType(String instructorType) {
        this.instructorType = instructorType;
    }

    /**
     * @return the assistant1
     */
    public Integer getAssistant1() {
        return assistant1;
    }

    /**
     * @param assistant1 the assistant1 to set
     */
    public void setAssistant1(Integer assistant1) {
        this.assistant1 = assistant1;
    }

    /**
     * @return the assistant2
     */
    public Integer getAssistant2() {
        return assistant2;
    }

    /**
     * @param assistant2 the assistant2 to set
     */
    public void setAssistant2(Integer assistant2) {
        this.assistant2 = assistant2;
    }

    /**
     * @return the assistant3
     */
    public Integer getAssistant3() {
        return assistant3;
    }

    /**
     * @param assistant3 the assistant3 to set
     */
    public void setAssistant3(Integer assistant3) {
        this.assistant3 = assistant3;
    }

    /**
     * @return the assistant4
     */
    public Integer getAssistant4() {
        return assistant4;
    }

    /**
     * @param assistant4 the assistant4 to set
     */
    public void setAssistant4(Integer assistant4) {
        this.assistant4 = assistant4;
    }

    /**
     * @return the timeStart
     */
    public String getTimeStart() {
        return timeStart;
    }

    /**
     * @param timeStart the timeStart to set
     */
    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    /**
     * @return the room
     */
    public String getRoom() {
        return room;
    }

    /**
     * @param room the room to set
     */
    public void setRoom(String room) {
        this.room = room;
    }

    /**
     * @return the assistant1Name
     */
    public String getAssistant1Name() {
        return assistant1Name;
    }

    /**
     * @param assistant1Name the assistant1Name to set
     */
    public void setAssistant1Name(String assistant1Name) {
        this.assistant1Name = assistant1Name;
    }

    /**
     * @return the assistant2Name
     */
    public String getAssistant2Name() {
        return assistant2Name;
    }

    /**
     * @param assistant2Name the assistant2Name to set
     */
    public void setAssistant2Name(String assistant2Name) {
        this.assistant2Name = assistant2Name;
    }

    /**
     * @return the assistant3Name
     */
    public String getAssistant3Name() {
        return assistant3Name;
    }

    /**
     * @param assistant3Name the assistant3Name to set
     */
    public void setAssistant3Name(String assistant3Name) {
        this.assistant3Name = assistant3Name;
    }

    /**
     * @return the assistant4Name
     */
    public String getAssistant4Name() {
        return assistant4Name;
    }

    /**
     * @param assistant4Name the assistant4Name to set
     */
    public void setAssistant4Name(String assistant4Name) {
        this.assistant4Name = assistant4Name;
    }

    /**
     * @return the instructorName
     */
    public String getInstructorName() {
        return instructorName;
    }

    /**
     * @param instructorName the instructorName to set
     */
    public void setInstructorName(String instructorName) {
        this.instructorName = instructorName;
    }

    /**
     * @return the instructorRecordId
     */
    public Integer getInstructorRecordId() {
        return instructorRecordId;
    }

    /**
     * @param instructorRecordId the instructorRecordId to set
     */
    public void setInstructorRecordId(Integer instructorRecordId) {
        this.instructorRecordId = instructorRecordId;
    }


  

   
}