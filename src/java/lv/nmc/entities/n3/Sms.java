/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.entities.n3;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author jm
 */
@XStreamAlias("Sms")
public class Sms {

    private String msisdn, message, completed_time;
    private Integer batch_id, status, source_id, unique_id, sms_id, notified_m, registered_m, hidden_m, age_limit = 0;
    private String name_surname, person_code;
    private String username, password, sender;
    private final Integer msg_class = 1, want_report = 1, allow_concat_text_sms = 1, concat_text_sms_max_parts = 2;

    private String document_type, document_ident, valid_till, phone, course_name, date_start, reoccurence, ccode, comment, status_verbose, event;
    private Integer document_id, user_id, course_id, monthes = 6;
    private Boolean notified, registered, hidden;
    private String nota_target, nota_criteria, nota_type = "email", sys_name, email_subject, send_to,
            mail_from = "robots@novikontas.lv", reply_to, preview, nota_filter;

    /**
     * @return the msisdn
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * @param msisdn the msisdn to set
     */
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    /**
     * @return the message_text
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message_text to set
     */
    public void setMessage(String message) {
        this.message = message;

        if (null != message) {
            this.preview = this.message.substring(0, 128);
            this.preview = this.preview + "...";
        }

    }

    /**
     * @return the batch_id
     */
    public Integer getBatch_id() {
        return batch_id;
    }

    /**
     * @param batch_id the batch_id to set
     */
    public void setBatch_id(Integer batch_id) {
        this.batch_id = batch_id;
    }

    /**
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return the source_id
     */
    public Integer getSource_id() {
        return source_id;
    }

    /**
     * @param source_id the source_id to set
     */
    public void setSource_id(Integer source_id) {
        this.source_id = source_id;
    }

    /**
     * @return the unique_id
     */
    public Integer getUnique_id() {
        return unique_id;
    }

    /**
     * @param unique_id the unique_id to set
     */
    public void setUnique_id(Integer unique_id) {
        this.unique_id = unique_id;
    }

    /**
     * @return the name_surname
     */
    public String getName_surname() {
        return name_surname;
    }

    /**
     * @param name_surname the name_surname to set
     */
    public void setName_surname(String name_surname) {
        this.name_surname = name_surname;
    }

    /**
     * @return the person_code
     */
    public String getPerson_code() {
        return person_code;
    }

    /**
     * @param person_code the person_code to set
     */
    public void setPerson_code(String person_code) {
        this.person_code = person_code;
    }

    /**
     * @return the completed_time
     */
    public String getCompleted_time() {
        return completed_time;
    }

    /**
     * @param completed_time the completed_time to set
     */
    public void setCompleted_time(String completed_time) {
        this.completed_time = completed_time;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the sender
     */
    public String getSender() {
        return sender;
    }

    /**
     * @return the msg_class
     */
    public Integer getMsg_class() {
        return msg_class;
    }

    /**
     * @return the want_report
     */
    public Integer getWant_report() {
        return want_report;
    }

    /**
     * @return the allow_concat_text_sms
     */
    public Integer getAllow_concat_text_sms() {
        return allow_concat_text_sms;
    }

    /**
     * @return the concat_text_sms_max_parts
     */
    public Integer getConcat_text_sms_max_parts() {
        return concat_text_sms_max_parts;
    }

    /**
     * @return the document_type
     */
    public String getDocument_type() {
        return document_type;
    }

    /**
     * @param document_type the document_type to set
     */
    public void setDocument_type(String document_type) {
        this.document_type = document_type;
    }

    /**
     * @return the document_ident
     */
    public String getDocument_ident() {
        return document_ident;
    }

    /**
     * @param document_ident the document_ident to set
     */
    public void setDocument_ident(String document_ident) {
        this.document_ident = document_ident;
    }

    /**
     * @return the valid_till
     */
    public String getValid_till() {
        return valid_till;
    }

    /**
     * @param valid_till the valid_till to set
     */
    public void setValid_till(String valid_till) {
        this.valid_till = valid_till;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the course_name
     */
    public String getCourse_name() {
        return course_name;
    }

    /**
     * @param course_name the course_name to set
     */
    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    /**
     * @return the date_start
     */
    public String getDate_start() {
        return date_start;
    }

    /**
     * @param date_start the date_start to set
     */
    public void setDate_start(String date_start) {
        this.date_start = date_start;
    }

    /**
     * @return the document_id
     */
    public Integer getDocument_id() {
        return document_id;
    }

    /**
     * @param document_id the document_id to set
     */
    public void setDocument_id(Integer document_id) {
        this.document_id = document_id;
    }

    /**
     * @return the user_id
     */
    public Integer getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
        //this.source_id = user_id;
    }

    /**
     * @return the sms_id
     */
    public Integer getSms_id() {
        return sms_id;
    }

    /**
     * @param sms_id the sms_id to set
     */
    public void setSms_id(Integer sms_id) {
        this.sms_id = sms_id;
        this.source_id = sms_id;
    }

    /**
     * @return the course_id
     */
    public Integer getCourse_id() {
        return course_id;
    }

    /**
     * @param course_id the course_id to set
     */
    public void setCourse_id(Integer course_id) {
        this.course_id = course_id;
    }

    /**
     * @return the monthes
     */
    public Integer getMonthes() {
        return monthes;
    }

    /**
     * @param monthes the monthes to set
     */
    public void setMonthes(Integer monthes) {
        this.monthes = monthes;
    }

    /**
     * @return the reoccurence
     */
    public String getReoccurence() {
        return reoccurence;
    }

    /**
     * @param reoccurence the reoccurence to set
     */
    public void setReoccurence(String reoccurence) {
        this.reoccurence = reoccurence;
    }

    /**
     * @return the ccode
     */
    public String getCcode() {
        return ccode;
    }

    /**
     * @param ccode the ccode to set
     */
    public void setCcode(String ccode) {
        this.ccode = ccode;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return the status_verbose
     */
    public String getStatus_verbose() {
        return status_verbose;
    }

    /**
     * @param status_verbose the status_verbose to set
     */
    public void setStatus_verbose(String status_verbose) {
        this.status_verbose = status_verbose;
    }

    /**
     * @return the notified_m
     */
    public Integer getNotified_m() {
        return notified_m;
    }

    /**
     * @param notified_m the notified_m to set
     */
    public void setNotified_m(Integer notified_m) {
        this.notified_m = notified_m;
        if (1 == this.notified_m) {
            this.notified = true;
        } else {
            this.notified = false;
        }
    }

    /**
     * @return the notified
     */
    public Boolean getNotified() {
        return notified;
    }

    /**
     * @param notified the notified to set
     */
    public void setNotified(Boolean notified) {
        this.notified = notified;
        if (true == this.notified) {
            this.notified_m = 1;
        } else {
            this.notified_m = 0;
        }
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @return the registered
     */
    public Boolean getRegistered() {
        return registered;
    }

    /**
     * @param registered the registered to set
     */
    public void setRegistered(Boolean registered) {
        this.registered = registered;
        if (registered) {
            this.registered_m = 1;
            return;
        }
        this.registered = false;
    }

    /**
     * @return the hidden
     */
    public Boolean getHidden() {
        return hidden;
    }

    /**
     * @param hidden the hidden to set
     */
    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
        if (hidden) {
            this.hidden_m = 1;
            return;
        }
        this.hidden_m = 0;
    }

    /**
     * @return the registered_m
     */
    public Integer getRegistered_m() {
        return registered_m;
    }

    /**
     * @param registered_m the registered_m to set
     */
    public void setRegistered_m(Integer registered_m) {
        this.registered_m = registered_m;
        if (registered_m == 1) {
            this.registered = true;
            return;
        }
        this.registered = false;
    }

    /**
     * @return the hidden_m
     */
    public Integer getHidden_m() {
        return hidden_m;
    }

    /**
     * @param hidden_m the hidden_m to set
     */
    public void setHidden_m(Integer hidden_m) {
        this.hidden_m = hidden_m;
        if (hidden_m == 1) {
            this.hidden = true;
            return;
        }
        this.hidden = false;

    }

    /**
     * @return the nota_target
     */
    public String getNota_target() {
        return nota_target;
    }

    /**
     * @param nota_target the nota_target to set
     */
    public void setNota_target(String nota_target) {
        this.nota_target = nota_target;
    }

    /**
     * @return the nota_criteria
     */
    public String getNota_criteria() {
        return nota_criteria;
    }

    /**
     * @param nota_criteria the nota_criteria to set
     */
    public void setNota_criteria(String nota_criteria) {
        this.nota_criteria = nota_criteria;
    }

    /**
     * @return the nota_type
     */
    public String getNota_type() {
        return nota_type;
    }

    /**
     * @param nota_type the nota_type to set
     */
    public void setNota_type(String nota_type) {
        this.nota_type = nota_type;
    }

    /**
     * @return the sys_name
     */
    public String getSys_name() {
        return sys_name;
    }

    /**
     * @param sys_name the sys_name to set
     */
    public void setSys_name(String sys_name) {
        this.sys_name = sys_name;
    }

    /**
     * @return the email_subject
     */
    public String getEmail_subject() {
        return email_subject;
    }

    /**
     * @param email_subject the email_subject to set
     */
    public void setEmail_subject(String email_subject) {
        this.email_subject = email_subject;
    }

    /**
     * @return the send_to
     */
    public String getSend_to() {
        return send_to;
    }

    /**
     * @param send_to the send_to to set
     */
    public void setSend_to(String send_to) {
        this.send_to = send_to;
    }

    /**
     * @return the mail_from
     */
    public String getMail_from() {
        return mail_from;
    }

    /**
     * @param mail_from the mail_from to set
     */
    public void setMail_from(String mail_from) {
        this.mail_from = mail_from;
    }

    /**
     * @return the reply_to
     */
    public String getReply_to() {
        return reply_to;
    }

    /**
     * @param reply_to the reply_to to set
     */
    public void setReply_to(String reply_to) {
        this.reply_to = reply_to;
    }

    /**
     * @return the preview
     */
    public String getPreview() {
        return preview;
    }

    /**
     * @param preview the preview to set
     */
    public void setPreview(String preview) {
        this.preview = preview;
    }

    /**
     * @return the age_limit
     */
    public Integer getAge_limit() {
        return age_limit;
    }

    /**
     * @param age_limit the age_limit to set
     */
    public void setAge_limit(Integer age_limit) {
        this.age_limit = age_limit;
    }

    /**
     * @return the nota_filter
     */
    public String getNota_filter() {
        return nota_filter;
    }

    /**
     * @param nota_filter the nota_filter to set
     */
    public void setNota_filter(String nota_filter) {
        this.nota_filter = nota_filter;
    }
}
