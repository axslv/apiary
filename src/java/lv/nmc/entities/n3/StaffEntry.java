/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.entities.n3;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.math.BigDecimal;
import java.text.DecimalFormat;
/**
 *
 * @author jm
 */
@XStreamAlias("StaffEntry")
public class StaffEntry extends GroupEntry {
    private String username, password, salt, alias, nameSurname, personCode, instructorType, department, evalEnd, group;
    private Integer staffId, bonusId, rewardId, courseId,appCount;
    private Double appData;
    private String startAppaisalDate, endAppaisalDate, courseName, domain;
    private Integer special, active = 0, polleeSum;
    private Double bonus, reward;
    private String bonusStarts, rewardEnds, bonusEnds, rewardStarts;
    private String bonusRounded, message, event, passwordRe;
   
    
//private List<String> aliases =  new ArrayList();


    public StaffEntry() {
    }
        public StaffEntry(Integer staffEntry) {
            this.staffId = staffEntry;
    }
        
        
           public StaffEntry(Integer staffEntry, String event) {
            this.staffId = staffEntry;
            this.event = event;
    }
         
    
     public StaffEntry(Integer record, String bonusStarts, String bonusRounded) {
         this.bonusId = record;
         this.bonusStarts = bonusStarts;
         this.bonusRounded=bonusRounded;
    }  

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the salt
     */
    public String getSalt() {
        return salt;
    }

    /**
     * @param salt the salt to set
     */
    public void setSalt(String salt) {
        this.salt = salt;
    }


    /**
     * @return the staffId
     */
    public Integer getStaffId() {
        return staffId;
    }

    /**
     * @param staffId the staffId to set
     */
    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    /**
     * @return the alias
     */
    public String getAlias() {
        return alias;
    }

    /**
     * @param alias the alias to set
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /**
     * @return the special
     */
    public Integer getSpecial() {
        return special;
    }

    /**
     * @param special the special to set
     */
    public void setSpecial(Integer special) {
        this.special = special;
    }

    /**
     * @return the nameSurname
     */
    public String getNameSurname() {
        return nameSurname;
    }

    /**
     * @param nameSurname the nameSurname to set
     */
    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    /**
     * @return the personCode
     */
    public String getPersonCode() {
        return personCode;
    }

    /**
     * @param personCode the personCode to set
     */
    public void setPersonCode(String personCode) {
        this.personCode = personCode;
    }

    /**
     * @return the instructorType
     */
    public String getInstructorType() {
        return instructorType;
    }

    /**
     * @param instructorType the instructorType to set
     */
    public void setInstructorType(String instructorType) {
        this.instructorType = instructorType;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return the evalEnd
     */
    public String getEvalEnd() {
        return evalEnd;
    }

    /**
     * @param evalEnd the evalEnd to set
     */
    public void setEvalEnd(String evalEnd) {
        this.evalEnd = evalEnd;
    }

    /**
     * @return the active
     */
    public Integer getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Integer active) {
        this.active = active;
    }

    /**
     * @return the bonus
     */
    public Double getBonus() {
        return bonus;
    }

    /**
     * @param bonus the bonus to set
     */
    public void setBonus(Double bonus) {
        BigDecimal bd = BigDecimal.valueOf(bonus);
        BigDecimal ss = bd.setScale(2);
        this.bonus = ss.doubleValue();
        DecimalFormat df = new DecimalFormat(".00");
        bonusRounded = df.format(this.bonus);
    }
    
    public void setBonusRounded(String bonusRounded) {
        this.bonusRounded = bonusRounded;
    }
    
    /**
     * @return the bonusStarts
     */
    public String getBonusStarts() {
        return bonusStarts;
    }

    /**
     * @param bonusStarts the bonusStarts to set
     */
    public void setBonusStarts(String bonusStarts) {
        this.bonusStarts = bonusStarts;
    }

    /**
     * @return the bonusRounded
     */
    public String getBonusRounded() {
        return bonusRounded;
    }

    /**
     * @return the bonusId
     */
    public Integer getBonusId() {
        return bonusId;
    }

    /**
     * @param bonusId the bonusId to set
     */
    public void setBonusId(Integer bonusId) {
        this.bonusId = bonusId;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @return the passwordRe
     */
    public String getPasswordRe() {
        return passwordRe;
    }

    /**
     * @param passwordRe the passwordRe to set
     */
    public void setPasswordRe(String passwordRe) {
        this.passwordRe = passwordRe;
    }

    /**
     * @return the bonusEnds
     */
    public String getBonusEnds() {
        return bonusEnds;
    }

    /**
     * @param bonusEnds the bonusEnds to set
     */
    public void setBonusEnds(String bonusEnds) {
        this.bonusEnds = bonusEnds;
    }

    /**
     * @return the rewardId
     */
    public Integer getRewardId() {
        return rewardId;
    }

    /**
     * @param rewardId the rewardId to set
     */
    public void setRewardId(Integer rewardId) {
        this.rewardId = rewardId;
    }

    /**
     * @return the reward
     */
    public Double getReward() {
        return reward;
    }

    /**
     * @param reward the reward to set
     */
    public void setReward(Double reward) {
        this.reward = reward;
    }

    /**
     * @return the rewardStarts
     */
    public String getRewardStarts() {
        return rewardStarts;
    }

    /**
     * @param rewardStarts the rewardStarts to set
     */
    public void setRewardStarts(String rewardStarts) {
        this.rewardStarts = rewardStarts;
    }

    /**
     * @return the rewardEnds
     */
    public String getRewardEnds() {
        return rewardEnds;
    }

    /**
     * @param rewardEnds the rewardEnds to set
     */
    public void setRewardEnds(String rewardEnds) {
        this.rewardEnds = rewardEnds;
    }

    /**
     * @return the courseId
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * @param courseId the courseId to set
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    /**
     * @return the appData
     */
    public Double getAppData() {
        return appData;
    }

    /**
     * @param appData the appData to set
     */
    public void setAppData(Double appData) {
        this.appData = appData;
    }

    /**
     * @return the startDate
     */
   

    /**
     * @return the courseName
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * @param courseName the courseName to set
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /**
     * @return the startAppaisalDate
     */
    public String getStartAppaisalDate() {
        return startAppaisalDate;
    }

    /**
     * @param startAppaisalDate the startAppaisalDate to set
     */
    public void setStartAppaisalDate(String startAppaisalDate) {
        this.startAppaisalDate = startAppaisalDate;
    }

    /**
     * @return the endAppaisalDate
     */
    public String getEndAppaisalDate() {
        return endAppaisalDate;
    }

    /**
     * @param endAppaisalDate the endAppaisalDate to set
     */
    public void setEndAppaisalDate(String endAppaisalDate) {
        this.endAppaisalDate = endAppaisalDate;
    }

    /**
     * @return the appCount
     */
    public Integer getAppCount() {
        return appCount;
    }

    /**
     * @param appCount the appCount to set
     */
    public void setAppCount(Integer appCount) {
        this.appCount = appCount;
    }

    /**
     * @return the polleeSum
     */
    public Integer getPolleeSum() {
        return polleeSum;
    }

    /**
     * @param polleeSum the polleeSum to set
     */
    public void setPolleeSum(Integer polleeSum) {
        this.polleeSum = polleeSum;
    }

    /**
     * @return the domain
     */
    public String getDomain() {
        return domain;
    }

    /**
     * @param domain the domain to set
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     * @return the group
     */
    public String getGroup() {
        return group;
    }

    /**
     * @param group the group to set
     */
    public void setGroup(String group) {
        this.group = group;
    }

    /**
     * @return the polleeSum
     */
   

    


   
}
