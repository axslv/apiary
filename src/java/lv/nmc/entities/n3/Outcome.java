/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.entities.n3;

/**
 *
 * @author mj
 */
public class Outcome {
    private String date, username, courseName, department;
    private Double dayOutcome;
    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }


    /**
     * @return the courseName
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * @param courseName the courseName to set
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return the dayOutcome
     */
    public Double getDayOutcome() {
        return dayOutcome;
    }

    /**
     * @param dayOutcome the dayOutcome to set
     */
    public void setDayOutcome(Double dayOutcome) {
        this.dayOutcome = dayOutcome;
    }

    
}
