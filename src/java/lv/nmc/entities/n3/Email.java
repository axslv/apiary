/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.entities.n3;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author mj
 */
@XStreamAlias("Email")
public class Email {
    private String event="sendMail";
    private String sendTo="training@novikontas.lv", sendFrom="robots@novikontas.lv", emailSubject, emailText;
//server info


 
    public String getEvent() {
        return event;
    }

   
    public void setEvent(String event) {
        this.event = event;
    }


    public String getSendTo() {
        return sendTo;
    }


    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }


    public String getSendFrom() {
        return sendFrom;
    }


    public void setSendFrom(String sendFrom) {
        this.sendFrom = sendFrom;
    }


    public String getEmailSubject() {
        return emailSubject;
    }


    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }


    public String getEmailText() {
        return emailText;
    }


    public void setEmailText(String emailText) {
        this.emailText = emailText;
    }



  
   
}
