package lv.nmc.entities.n3;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

/**
 *
 * @author jm
 */
@XStreamAlias("DocumentEntry")
public class DocumentEntry implements Serializable {   
    private String event = "noEvent";
    private String documentNr, issuer, dateEnd, issueDate, validTill;
    private String docName;
    private Integer finalId, courseId, userId;

    public DocumentEntry() {
        
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @return the documentNr
     */
    public String getDocumentNr() {
        return documentNr;
    }

    /**
     * @param documentNr the documentNr to set
     */
    public void setDocumentNr(String documentNr) {
        this.documentNr = documentNr;
    }

    /**
     * @return the issuer
     */
    public String getIssuer() {
        return issuer;
    }

    /**
     * @param issuer the issuer to set
     */
    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    /**
     * @return the dateEnd
     */
    public String getDateEnd() {
        return dateEnd;
    }

    /**
     * @param dateEnd the dateEnd to set
     */
    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    /**
     * @return the docName
     */
    public String getDocName() {
        return docName;
    }

    /**
     * @param docName the docName to set
     */
    public void setDocName(String docName) {
        this.docName = docName;
    }

    /**
     * @return the finalId
     */
    public Integer getFinalId() {
        return finalId;
    }

    /**
     * @param finalId the finalId to set
     */
    public void setFinalId(Integer finalId) {
        this.finalId = finalId;
    }

    /**
     * @return the courseId
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * @param courseId the courseId to set
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    /**
     * @return the issueDate
     */
    public String getIssueDate() {
        return issueDate;
    }

    /**
     * @param issueDate the issueDate to set
     */
    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the validTill
     */
    public String getValidTill() {
        return validTill;
    }

    /**
     * @param validTill the validTill to set
     */
    public void setValidTill(String validTill) {
        this.validTill = validTill;
    }


}
