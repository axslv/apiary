/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.entities.n3.col;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
Each Client implements CollegeEntry
So, every college registration (except for usual studies) has 6 Semesters bound to Client
Each Semester is bound to Credits count
One Year = 6 semesters
Each client has OBLIGATE courses every YEAR
Last Semester probably is exam time
 */

@XStreamAlias("CollegeEntry")
public class CollegeEntry {
    private Integer courseId;
    private String courseName, nameSurname, personCode, domain, courseNum;
    private Integer s0,s1,s2,s3,s4,s5,s6, quiz, exam, semesterName;
    private String event;
    
    
    public CollegeEntry() {
        
    }

    /**
     * @return the courseId
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * @param courseId the courseId to set
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    /**
     * @return the courseName
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * @param courseName the courseName to set
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /**
     * @return the nameSurname
     */
    public String getNameSurname() {
        return nameSurname;
    }

    /**
     * @param nameSurname the nameSurname to set
     */
    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    /**
     * @return the personCode
     */
    public String getPersonCode() {
        return personCode;
    }

    /**
     * @param personCode the personCode to set
     */
    public void setPersonCode(String personCode) {
        this.personCode = personCode;
    }

    /**
     * @return the domain
     */
    public String getDomain() {
        return domain;
    }

    /**
     * @param domain the domain to set
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

  
    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @return the courseNum
     */
    public String getCourseNum() {
        return courseNum;
    }

    /**
     * @param courseNum the courseNum to set
     */
    public void setCourseNum(String courseNum) {
        this.courseNum = courseNum;
    }

    /**
     * @return the s0
     */
    public Integer getS0() {
        return s0;
    }

    /**
     * @param s0 the s0 to set
     */
    public void setS0(Integer s0) {
        this.s0 = s0;
    }

    /**
     * @return the s1
     */
    public Integer getS1() {
        return s1;
    }

    /**
     * @param s1 the s1 to set
     */
    public void setS1(Integer s1) {
        this.s1 = s1;
    }

    /**
     * @return the s2
     */
    public Integer getS2() {
        return s2;
    }

    /**
     * @param s2 the s2 to set
     */
    public void setS2(Integer s2) {
        this.s2 = s2;
    }

    /**
     * @return the s3
     */
    public Integer getS3() {
        return s3;
    }

    /**
     * @param s3 the s3 to set
     */
    public void setS3(Integer s3) {
        this.s3 = s3;
    }

    /**
     * @return the s4
     */
    public Integer getS4() {
        return s4;
    }

    /**
     * @param s4 the s4 to set
     */
    public void setS4(Integer s4) {
        this.s4 = s4;
    }

    /**
     * @return the s5
     */
    public Integer getS5() {
        return s5;
    }

    /**
     * @param s5 the s5 to set
     */
    public void setS5(Integer s5) {
        this.s5 = s5;
    }

    /**
     * @return the s6
     */
    public Integer getS6() {
        return s6;
    }

    /**
     * @param s6 the s6 to set
     */
    public void setS6(Integer s6) {
        this.s6 = s6;
    }

    /**
     * @return the quiz
     */
    public Integer getQuiz() {
        return quiz;
    }

    /**
     * @param quiz the quiz to set
     */
    public void setQuiz(Integer quiz) {
        this.quiz = quiz;
    }

    /**
     * @return the exam
     */
    public Integer getExam() {
        return exam;
    }

    /**
     * @param exam the exam to set
     */
    public void setExam(Integer exam) {
        this.exam = exam;
    }

    /**
     * @return the semesterName
     */
    public Integer getSemesterName() {
        return semesterName;
    }

    /**
     * @param semesterName the semesterName to set
     */
    public void setSemesterName(Integer semesterName) {
        this.semesterName = semesterName;
    }
    

    
}
