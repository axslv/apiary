package lv.nmc.entities;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.Serializable;

/**
 *
 * @author jm
 */

public class ClientEntry implements Serializable {

    private Integer userId;
    private String nameSurname, rank, personCode, birthday,
            marineBookId, phone, nConnect, address, email, company, comments, prefLang, contractNr, secPhone, ccode;
    private Integer level;
    private Integer vipL = 0;
    private Integer skipL = 0;
    private boolean vip = false;
    private boolean skip = false;
    private String event = "noEvent";

    public ClientEntry() {
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the nameSurname
     */
    public String getNameSurname() {
        return nameSurname;
    }

    /**
     * @param nameSurname the nameSurname to set
     */
    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    /**
     * @return the rank
     */
    public String getRank() {
        return rank;
    }

    /**
     * @param rank the rank to set
     */
    public void setRank(String rank) {
        this.rank = rank;
    }

    /**
     * @return the personCode
     */
    public String getPersonCode() {
        return personCode;
    }

    /**
     * @param personCode the personCode to set
     */
    public void setPersonCode(String personCode) {
        this.personCode = personCode;
    }

    /**
     * @return the birthday
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     * @param birthday the birthday to set
     */
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    /**
     * @return the marineBookId
     */
    public String getMarineBookId() {
        return marineBookId;
    }

    /**
     * @param marineBookId the marineBookId to set
     */
    public void setMarineBookId(String marineBookId) {
        this.marineBookId = marineBookId;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the nConnect
     */
    public String getnConnect() {
        return nConnect;
    }

    /**
     * @param nConnect the nConnect to set
     */
    public void setnConnect(String nConnect) {
        this.nConnect = nConnect;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(String company) {
        this.company = company;
    }

    /**
     * @return the comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * @return the prefLang
     */
    public String getPrefLang() {
        return prefLang;
    }

    /**
     * @param prefLang the prefLang to set
     */
    public void setPrefLang(String prefLang) {
        this.prefLang = prefLang;
    }

    /**
     * @return the level
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * @param level the level to set
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    /**
     * @return the vipL
     */
    public Integer getVipL() {
        return vipL;
    }

    /**
     * @param vipL the vipL to set
     */
    public void setVipL(Integer vipL) {
        this.vipL = vipL;
        if (vipL == 1) {
            this.vip = true;
        }
    }

    /**
     * @return the skipL
     */
    public Integer getSkipL() {
        return skipL;
    }

    /**
     * @param skipL the skipL to set
     */
    public void setSkipL(Integer skipL) {
        this.skipL = skipL;
        if (skipL == 1) {
            this.skip = true;
        }
    }

    /**
     * @return the vip
     */
    public boolean isVip() {
        return vip;
    }

    /**
     * @param vip the vip to set
     */
    public void setVip(boolean vip) {
        this.vip = vip;
        if (vip) {
            this.vipL = 1;
        }
    }

    /**
     * @return the skip
     */
    public boolean isSkip() {
        return skip;
    }

    /**
     * @param skip the skip to set
     */
    public void setSkip(boolean skip) {
        this.skip = skip;
        if (skip) {
            this.skipL = 1;
        }
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @return the contractNr
     */
    public String getContractNr() {
        return contractNr;
    }

    /**
     * @param contractNr the contractNr to set
     */
    public void setContractNr(String contractNr) {
        this.contractNr = contractNr;
    }

    /**
     * @return the secPhone
     */
    public String getSecPhone() {
        return secPhone;
    }

    /**
     * @param secPhone the secPhone to set
     */
    public void setSecPhone(String secPhone) {
        this.secPhone = secPhone;
    }

    /**
     * @return the ccode
     */
    public String getCcode() {
        return ccode;
    }

    /**
     * @param ccode the ccode to set
     */
    public void setCcode(String ccode) {
        this.ccode = ccode;
    }

  
}
