/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.entities.grid;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author jm
 */
@XStreamAlias("GridEntry")
public class GridEntry {
    private Integer fileId, rootId;
    private String url;
    private String readCap, writeCap, verifyCap;
    private String accessTime, checkTime;
    private String fileName, rootName, shortName, fileType, instructor, event, parentName;
    private Boolean insertRecord = false;
    
    public GridEntry() {
        
    }
    
    public GridEntry(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the readCap
     */
    public String getReadCap() {
        return readCap;
    }

    /**
     * @param readCap the readCap to set
     */
    public void setReadCap(String readCap) {
        this.readCap = readCap;
    }

    /**
     * @return the writeCap
     */
    public String getWriteCap() {
        return writeCap;
    }

    /**
     * @param writeCap the writeCap to set
     */
    public void setWriteCap(String writeCap) {
        this.writeCap = writeCap;
    }

    /**
     * @return the verifyCap
     */
    public String getVerifyCap() {
        return verifyCap;
    }

    /**
     * @param verifyCap the verifyCap to set
     */
    public void setVerifyCap(String verifyCap) {
        this.verifyCap = verifyCap;
    }

    /**
     * @return the accessTime
     */
    public String getAccessTime() {
        return accessTime;
    }

    /**
     * @param accessTime the accessTime to set
     */
    public void setAccessTime(String accessTime) {
        this.accessTime = accessTime;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
        String[] fns = fileName.split("/");
        int i = fns.length-1;
        this.shortName = fns[i];
        StringBuilder sb = new StringBuilder();
        for (int s =0; s < i; s++) {
            sb.append(fns[s]);
            sb.append("/");
        }
        this.parentName = sb.toString();
        
    }

    /**
     * @return the instructor
     */
    public String getInstructor() {
        return instructor;
    }

    /**
     * @param instructor the instructor to set
     */
    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }

    /**
     * @return the fileId
     */
    public Integer getFileId() {
        return fileId;
    }

    /**
     * @param fileId the fileId to set
     */
    public void setFileId(Integer fileId) {
        this.fileId = fileId;
    }

    /**
     * @return the checkTime
     */
    public String getCheckTime() {
        return checkTime;
    }

    /**
     * @param checkTime the checkTime to set
     */
    public void setCheckTime(String checkTime) {
        this.checkTime = checkTime;
    }

    /**
     * @return the fileType
     */
    public String getFileType() {
        return fileType;
    }

    /**
     * @param fileType the fileType to set
     */
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @return the shortName
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * @param shortName the shortName to set
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /**
     * @return the rootName
     */
    public String getRootName() {
        return rootName;
    }

    /**
     * @param rootName the rootName to set
     */
    public void setRootName(String rootName) {
        this.rootName = rootName;
    }

    /**
     * @return the rootId
     */
    public Integer getRootId() {
        return rootId;
    }

    /**
     * @param rootId the rootId to set
     */
    public void setRootId(Integer rootId) {
        this.rootId = rootId;
    }

    /**
     * @return the parentName
     */
    public String getParentName() {
        return parentName;
    }

    /**
     * @param parentName the parentName to set
     */
    public void setParentName(String parentName) {
        this.parentName = parentName;
    }



    /**
     * @return the insertRecord
     */
    public Boolean getInsertRecord() {
        return insertRecord;
    }

    /**
     * @param insertRecord the insertRecord to set
     */
    public void setInsertRecord(Boolean insertRecord) {
        this.insertRecord = insertRecord;
    }
    
    
    
}
