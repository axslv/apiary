/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.entities.form;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author jm
 */
@XStreamAlias("FormData")

public class FormData {

    private String instructorName, groupName, dateEnd, courseName;
    private Integer answerId, questionId, rootId, registrationId;
    private String answerText, questionText, answerIds, questionIds;



    /**
     * @return the instructorName
     */
    public String getInstructorName() {
        return instructorName;
    }

    /**
     * @param instructorName the instructorName to set
     */
    public void setInstructorName(String instructorName) {
        this.instructorName = instructorName;
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the dateEnd
     */
    public String getDateEnd() {
        return dateEnd;
    }

    /**
     * @param dateEnd the dateEnd to set
     */
    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    /**
     * @return the courseName
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * @param courseName the courseName to set
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /**
     * @return the answerId
     */
    public Integer getAnswerId() {
        return answerId;
    }

    /**
     * @param answerId the answerId to set
     */
    public void setAnswerId(Integer answerId) {
        this.answerId = answerId;
    }

    /**
     * @return the questionId
     */
    public Integer getQuestionId() {
        return questionId;
    }

    /**
     * @param questionId the questionId to set
     */
    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    /**
     * @return the rootId
     */
    public Integer getRootId() {
        return rootId;
    }

    /**
     * @param rootId the rootId to set
     */
    public void setRootId(Integer rootId) {
        this.rootId = rootId;
    }

    /**
     * @return the answerText
     */
    public String getAnswerText() {
        return answerText;
    }

    /**
     * @param answerText the answerText to set
     */
    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    /**
     * @return the questionText
     */
    public String getQuestionText() {
        return questionText;
    }

    /**
     * @param questionText the questionText to set
     */
    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    /**
     * @return the answerIds
     */
    public String getAnswerIds() {
        return answerIds;
    }

    /**
     * @param answerIds the answerIds to set
     */
    public void setAnswerIds(String answerIds) {
        this.answerIds = answerIds;
    }

    /**
     * @return the questionIds
     */
    public String getQuestionIds() {
        return questionIds;
    }

    /**
     * @param questionIds the questionIds to set
     */
    public void setQuestionIds(String questionIds) {
        this.questionIds = questionIds;
    }

    /**
     * @return the registrationId
     */
    public Integer getRegistrationId() {
        return registrationId;
    }

    /**
     * @param registrationId the registrationId to set
     */
    public void setRegistrationId(Integer registrationId) {
        this.registrationId = registrationId;
    }




   
   
}

