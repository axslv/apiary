/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.entities;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jm
 */
@XStreamAlias("GroupEntry")
public class GroupEntry implements Serializable {

    private Integer courseId, groupId, regPrintedL, finPrintedL, certPrintedL;
    private boolean regPrinted, finPrinted, certPrinted;
    private String groupName, courseName, domain, department, dateStart, dateEnd;
    private String event = "noEvent";
    private List<Integer> courseIdsList = new ArrayList();
    

    public GroupEntry() {
    }

    /**
     * @return the courseId
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * @param courseId the courseId to set
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    /**
     * @return the groupId
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * @param groupId the groupId to set
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the domain
     */
    public String getDomain() {
        return domain;
    }

    /**
     * @param domain the domain to set
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return the dateStart
     */
    public String getDateStart() {
        return dateStart;
    }

    /**
     * @param dateStart the dateStart to set
     */
    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    /**
     * @return the dateEnd
     */
    public String getDateEnd() {
        return dateEnd;
    }

    /**
     * @param dateEnd the dateEnd to set
     */
    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @return the courseIdsList
     */
    public List<Integer> getCourseIdsList() {
        return courseIdsList;
    }

    /**
     * @return the courseName
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * @param courseName the courseName to set
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /**
     * @param courseIdsList the courseIdsList to set
     */
    public void setCourseIdsList(List<Integer> courseIdsList) {
        this.courseIdsList = courseIdsList;
    }

    /**
     * @return the regPrintedL
     */
    public Integer getRegPrintedL() {
        return regPrintedL;
    }

    /**
     * @param regPrintedL the regPrintedL to set
     */
    public void setRegPrintedL(Integer regPrintedL) {
        this.regPrintedL = regPrintedL;
        this.regPrinted = false;
        
        if (regPrintedL.equals(1)) {
            this.regPrinted = true;
        }
    }

    /**
     * @return the finPrintedL
     */
    public Integer getFinPrintedL() {
        return finPrintedL;
    }

    /**
     * @param finPrintedL the finPrintedL to set
     */
    public void setFinPrintedL(Integer finPrintedL) {
        this.finPrintedL = finPrintedL;
        this.finPrinted = false;
        
        if (finPrintedL.equals(1)) {
            this.finPrinted = true;
        }
    }

    /**
     * @return the certPrintedL
     */
    public Integer getCertPrintedL() {
        return certPrintedL;
    }

    /**
     * @param certPrintedL the certPrintedL to set
     */
    public void setCertPrintedL(Integer certPrintedL) {
        this.certPrintedL = certPrintedL;
        this.certPrinted = false;
        
        if (certPrintedL.equals(1)) {
            this.certPrinted = true;
        }
    }

    /**
     * @return the regPrinted
     */
    public boolean isRegPrinted() {
        return regPrinted;
    }

    /**
     * @param regPrinted the regPrinted to set
     */
    public void setRegPrinted(boolean regPrinted) {
        this.regPrinted = regPrinted;
        this.regPrintedL = 0;
        if (regPrinted) {
            this.regPrintedL = 1;
        }
    }

    /**
     * @return the finPrinted
     */
    public boolean isFinPrinted() {
        return finPrinted;
    }

    /**
     * @param finPrinted the finPrinted to set
     */
    public void setFinPrinted(boolean finPrinted) {
        this.finPrinted = finPrinted;
        this.finPrintedL = 0;
        if (finPrinted) {
            this.finPrintedL = 1;
        }
    }

    /**
     * @return the certPrinted
     */
    public boolean isCertPrinted() {
        return certPrinted;
    }

    /**
     * @param certPrinted the certPrinted to set
     */
    public void setCertPrinted(boolean certPrinted) {
        this.certPrinted = certPrinted;
        this.certPrintedL = 0;
        if (certPrinted) {
            this.certPrintedL = 1;
        }
    }


  

   
}