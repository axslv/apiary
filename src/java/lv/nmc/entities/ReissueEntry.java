/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.entities;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

/**
 *
 * @author mj
 */
@XStreamAlias("ReissueEntry")
public class ReissueEntry implements Serializable {

    private String nameSurname, personCode, seamansBook, dateIssued, certNr, oldDateIssued, oldCertNr, oldIssuer, groupName;
    private Integer courseId, recordCount;
    private Integer limit = 10;
    private Integer offset = 0;

    /**
     * @return the nameSurname
     */
    public String getNameSurname() {
        return nameSurname;
    }

    /**
     * @param nameSurname the nameSurname to set
     */
    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    /**
     * @return the personCode
     */
    public String getPersonCode() {
        return personCode;
    }

    /**
     * @param personCode the personCode to set
     */
    public void setPersonCode(String personCode) {
        this.personCode = personCode;
    }

    /**
     * @return the seamansBook
     */
    public String getSeamansBook() {
        return seamansBook;
    }

    /**
     * @param seamansBook the seamansBook to set
     */
    public void setSeamansBook(String seamansBook) {
        this.seamansBook = seamansBook;
    }

    /**
     * @return the dateIssued
     */
    public String getDateIssued() {
        return dateIssued;
    }

    /**
     * @param dateIssued the dateIssued to set
     */
    public void setDateIssued(String dateIssued) {
        this.dateIssued = dateIssued;
    }

    /**
     * @return the certNr
     */
    public String getCertNr() {
        return certNr;
    }

    /**
     * @param certNr the certNr to set
     */
    public void setCertNr(String certNr) {
        this.certNr = certNr;
    }

    /**
     * @return the oldDateIssued
     */
    public String getOldDateIssued() {
        return oldDateIssued;
    }

    /**
     * @param oldDateIssued the oldDateIssued to set
     */
    public void setOldDateIssued(String oldDateIssued) {
        this.oldDateIssued = oldDateIssued;
    }

    /**
     * @return the oldCertNr
     */
    public String getOldCertNr() {
        return oldCertNr;
    }

    /**
     * @param oldCertNr the oldCertNr to set
     */
    public void setOldCertNr(String oldCertNr) {
        this.oldCertNr = oldCertNr;
    }

    /**
     * @return the oldIssuer
     */
    public String getOldIssuer() {
        return oldIssuer;
    }

    /**
     * @param oldIssuer the oldIssuer to set
     */
    public void setOldIssuer(String oldIssuer) {
        this.oldIssuer = oldIssuer;
    }

    /**
     * @return the courseId
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * @param courseId the courseId to set
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    /**
     * @return the offset
     */
    public Integer getOffset() {
        return offset;
    }

    /**
     * @param offset the offset to set
     */
    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    /**
     * @return the limit
     */
    public Integer getLimit() {
        return limit;
    }

    /**
     * @param limit the limit to set
     */
    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    /**
     * @return the recordCount
     */
    public Integer getRecordCount() {
        return recordCount;
    }

    /**
     * @param recordCount the recordCount to set
     */
    public void setRecordCount(Integer recordCount) {
        this.recordCount = recordCount;
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
