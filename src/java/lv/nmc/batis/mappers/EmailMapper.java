/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers;

import java.util.List;
import lv.nmc.entities.EmailEntry;



/**
 *
 * @author jm
 */
public interface EmailMapper {
    List<EmailEntry> fetchMailingSettings(EmailEntry ee);
   
}
