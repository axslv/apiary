/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers;

import java.util.List;
import lv.nmc.entities.StaffEntry;

/**
 *
 * @author jm
 */
public interface StaffMapper {

    List<StaffEntry> noEvent(StaffEntry se);
    
    List<StaffEntry> loginSpecialUser(StaffEntry se);

    List<StaffEntry> selectSpecialUser(StaffEntry se);

    List<StaffEntry> aliasesByName(StaffEntry se);

    List<StaffEntry> checkAlias(StaffEntry se);

    List<StaffEntry> allInstructorsList(StaffEntry se);

    List<StaffEntry> activeInstructorsList(StaffEntry se);

    List<StaffEntry> inactiveInstructorsList(StaffEntry se);

    List<StaffEntry> instructorFullDetails(StaffEntry se);

    List<StaffEntry> instructorEvalType(StaffEntry se);

    List<StaffEntry> instructorBonusList(StaffEntry se);

    List<StaffEntry> instructorRewardList(StaffEntry se);

    List<StaffEntry> empInstructorsList(StaffEntry se);

    List<StaffEntry> guestInstructorsList(StaffEntry se);
    
    void _updateStaticBonuses(StaffEntry se);
    
    void _updateStaticRewards(StaffEntry se);

    void changeInsActive(StaffEntry se);

    void changeInsType(StaffEntry se);

    void _updateInstructor(StaffEntry se);

    void closeEval(StaffEntry se);

    void _updateAddBonus(StaffEntry se);

    void _updateAddReward(StaffEntry se);

    void _deleteDeleteBonus(StaffEntry se);

    void _deleteDeleteReward(StaffEntry se);

    void _updatePassword(StaffEntry se);

    List<StaffEntry> userByLoginPassword(StaffEntry se);
}
