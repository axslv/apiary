/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers;

import java.util.List;
import lv.nmc.entities.CourseEntry;
import lv.nmc.entities.CourseIncomeQuery;
import lv.nmc.entities.Outcome;


/**
 *
 * @author jm
 */
public interface GraphMapper {

    List<CourseEntry> selectCourseIncome(CourseIncomeQuery cq);

    List<CourseEntry> selectCourseIncomeFull(CourseIncomeQuery cq);
    
    List<CourseEntry> selectCourseIncomeDpt(CourseIncomeQuery cq);
    
    List<CourseEntry> selectCourseIncomeFullDpt(CourseIncomeQuery cq);
    
    List<Outcome> totalOutcome(CourseIncomeQuery o);
    
    List<Outcome> totalOutcomeDpt(CourseIncomeQuery o);
}
