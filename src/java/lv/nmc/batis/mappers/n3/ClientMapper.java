/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers.n3;

import java.util.List;
import lv.nmc.entities.n3.ClientEntry;

/**
 *
 * @author jm
 */
public interface ClientMapper {
    
    List<ClientEntry> selectAll(ClientEntry ce);
    
    List<ClientEntry> clientDetailsByPersonCode(ClientEntry ce);
    
    List<ClientEntry> clientDetailsByPersonCodeAndName(ClientEntry ce);

    List<ClientEntry> clientDetails(ClientEntry ce);
    
    List<ClientEntry> idByPersonCode(ClientEntry ce);
    
    List<ClientEntry> searchUser(ClientEntry ce);
    
    void _updateUserProfile(ClientEntry ce);
    
    void _updatePrefLang(ClientEntry ce);
    
    void _updatePersonCode(ClientEntry ce);
    
    void _updateInsertClient(ClientEntry ce);
    
    void deleteFuckers(ClientEntry ce);
}
