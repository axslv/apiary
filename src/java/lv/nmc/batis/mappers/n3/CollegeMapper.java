/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers.n3;

import java.util.List;
import lv.nmc.entities.n3.col.CollegeEntry;

public interface CollegeMapper {

    List<CollegeEntry> courseMap(CollegeEntry ce);

    List<CollegeEntry> testFire(CollegeEntry ce);
    
    List<CollegeEntry> checkPointsRecord(CollegeEntry ce);

    void _updateCourseNum(CollegeEntry ce);

    void _insertExam(CollegeEntry ce);

    void _deleteExam(CollegeEntry ce);

    void _insertQuiz(CollegeEntry ce);

    void _deleteQuiz(CollegeEntry ce);
    
    void _updateTotalPoints(CollegeEntry ce);
    
    void _updateTotalPointsS0(CollegeEntry ce);
    
    void _updateTotalPointsS1(CollegeEntry ce);
    
    void _updateTotalPointsS2(CollegeEntry ce);
    
    void _updateTotalPointsS3(CollegeEntry ce);
    
    void _updateTotalPointsS4(CollegeEntry ce);
    
    void _updateTotalPointsS5(CollegeEntry ce);
    
    void _updateTotalPointsS6(CollegeEntry ce);
    
    void _insertTotalPoints(CollegeEntry ce);

}
