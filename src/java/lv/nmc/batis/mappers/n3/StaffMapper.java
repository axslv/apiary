/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers.n3;

import java.util.List;
import lv.nmc.entities.n3.StaffEntry;

/**
 *
 * @author jm
 */
public interface StaffMapper {

    List<StaffEntry> noEvent(StaffEntry se);
    
    List<StaffEntry> loginSpecialUser(StaffEntry se);

    List<StaffEntry> selectSpecialUser(StaffEntry se);

    List<StaffEntry> aliasesByName(StaffEntry se);

    List<StaffEntry> checkAlias(StaffEntry se);

    List<StaffEntry> allInstructorsList(StaffEntry se);

    List<StaffEntry> activeInstructorsList(StaffEntry se);

    List<StaffEntry> inactiveInstructorsList(StaffEntry se);

    List<StaffEntry> instructorFullDetails(StaffEntry se);

    List<StaffEntry> instructorEvalType(StaffEntry se);

    List<StaffEntry> instructorBonusList(StaffEntry se);

    List<StaffEntry> instructorRewardList(StaffEntry se);

    List<StaffEntry> empInstructorsList(StaffEntry se);

    List<StaffEntry> guestInstructorsList(StaffEntry se);
    
    List<StaffEntry> getAppaisalByCourseByInstructorByDate(StaffEntry se);
   
    List<StaffEntry> fetchInstructorsByCourseIdByDateAppNotZero(StaffEntry se);
    
    List<StaffEntry> getAppaisalCountByCourseByInstructorByDate(StaffEntry se);
    
    List<StaffEntry> getPolleeCountByCourseByInstructorByDateNotZero(StaffEntry se);
    
     List<StaffEntry> allInstructorsListNotZero(StaffEntry se);
     
     List<StaffEntry> getAppaisalByCourseByDate(StaffEntry se);
     
     List<StaffEntry> getAppaisalCountByCourseByDate(StaffEntry se);
     
     List<StaffEntry> getPolleeCountByCourseByDateNotZero(StaffEntry se);
     
      List<StaffEntry> getAppaisalByDepartmentByDate(StaffEntry se);
     
      List<StaffEntry> getAppaisalCountByDepartmentByDate(StaffEntry se);
      
      List<StaffEntry> getPolleeCountByDepartmentByDateNotZero(StaffEntry se);
      
      List<StaffEntry> getAppaisalByDomainByDate(StaffEntry se);
     
      List<StaffEntry> getAppaisalCountByDomainByDate(StaffEntry se);
      
      List<StaffEntry> getPolleeCountByDomainByDateNotZero(StaffEntry se);
     
     
     
     
    
    
    
   
    
    void _updateStaticBonuses(StaffEntry se);
    
    void _updateStaticRewards(StaffEntry se);

    void changeInsActive(StaffEntry se);

    void changeInsType(StaffEntry se);

    void _updateInstructor(StaffEntry se);

    void closeEval(StaffEntry se);

    void _updateAddBonus(StaffEntry se);

    void _updateAddReward(StaffEntry se);

    void _deleteDeleteBonus(StaffEntry se);

    void _deleteDeleteReward(StaffEntry se);

    void _updatePassword(StaffEntry se);

    List<StaffEntry> userByLoginPassword(StaffEntry se);
}
