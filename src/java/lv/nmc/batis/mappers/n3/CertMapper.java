/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers.n3;

import java.util.List;
import lv.nmc.entities.CertEntry;

public interface CertMapper {
    List<CertEntry> checkCert(CertEntry ce);
  List<CertEntry> checkReissue(CertEntry ce);

}
