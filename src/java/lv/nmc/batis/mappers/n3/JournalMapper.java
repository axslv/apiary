/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers.n3;

import lv.nmc.entities.n3.JournalEntry;

public interface JournalMapper {    
   void _updateInsertRecord(JournalEntry je);
   void _updateUpdateRecord(JournalEntry je);
   void _updateInsertGroupRecord(JournalEntry je);
   void _insertJournal(JournalEntry je);
}
