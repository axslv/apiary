/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers.n3;

import java.util.List;
import lv.nmc.entities.n3.Payment;

/**
 *
 * @author jm
 */
public interface PaymentsMapper {

    List<Payment> collegeOne(Payment pt);

    List<Payment> advancesByInterval(Payment pt);

    List<Payment> calcFukkenShit(Payment pt);

    List<Payment> calcFukkenShitAdv(Payment pt);

    List<Payment> byPaymentDateBankInterval(Payment pt);

    List<Payment> byCommentInterval(Payment pt);

    List<Payment> byCourseInterval(Payment pt);

    List<Payment> byBillNrInterval(Payment pt);

    List<Payment> byCompanyInterval(Payment pt);

    List<Payment> selectCert(Payment pt);

    List<Payment> byPaymentDate(Payment pt);

    List<Payment> detailsByMultipleIds(List<Integer> pt);

    List<Payment> byPaymentDateInterval(Payment pt);

    List<Payment> byDateStart(Payment pt);

    List<Payment> byDateStartInterval(Payment pt);

    List<Payment> byDateEndInterval(Payment pt);

    List<Payment> advancesByUserId(Payment pt);

    List<Payment> byNameSurname(Payment pt);

    List<Payment> byPersonCode(Payment pt);

    List<Payment> byBillNr(Payment pt);

    List<Payment> byDateEnd(Payment pt);

    List<Payment> byComment(Payment pt);

    List<Payment> byCompany(Payment pt);

    List<Payment> byCourse(Payment pt);

    List<Payment> paymentDetails(Payment pt);

    List<Payment> selectBanks(Payment pt);
    
    void _updateRegCompany(Payment pt);
    
    void _updateCompany(Payment pt);

    void _updateDeactivatePayment(Payment pt);

    void _updateVessel(Payment pt);

    void _updateAux(Payment pt);

    void _updateInsertCompany(Payment pt);

    void _updateCertGiven(Payment pt);

    void _updateInsertPayment(Payment pt);

    void _updateChangePaymentDetails(Payment pt);

    void _updateChangeBill(Payment pt);

    void _updateChangePaidBill(Payment pt);

    void _updateChangeCompany(Payment pt);

    void _updateChangePrice(Payment pt);

    void _updateRemovePaidRecord(Payment pt);

    void _updatePaymentComment(Payment pt);

    void _updateActivatePayment(Payment pt);

    void _updateChangeBank(Payment pt);

    void _updateChangeVoucher(Payment pt);

    void _updateChangePaidComment(Payment pt);

    void _updatePayDate(Payment pt);

    void _updateInsertAdvance(Payment pt);

    void _updateDeleteAdvance(Payment pt);

    void _updateDeleteCrewComment(Payment pt);

    void _updateInsertCrewComment(Payment pt);

    // void _updateDeleteAdvanceByV(Payment pt);
    List<Payment> selectForCron(Payment pt);

    List<Payment> selectUniqueAdvance(Payment pt);

}
