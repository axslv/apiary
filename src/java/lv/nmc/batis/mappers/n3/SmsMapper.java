/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers.n3;

import java.util.List;
import lv.nmc.entities.n3.Sms;
import lv.nmc.entities.n3.SmsLists;

/**
 *
 * @author jm
 */
public interface SmsMapper {

    List<Sms> selectReports(Sms sms);
    
    List<Sms> selectHiddenReports(Sms sms);
    
    List<Sms> selectFilteredReports(Sms sms);    

    void _updateNotified(Sms sms);
    
    void _updateHideSms(Sms sms);
    
    void _updateRegisterSms(Sms sms);

    void _updateComment(Sms sms);
    
    //-----------------------------------------------------------------
    
    List<Sms> selectNotifications(Sms sms);
    
    
    void _updateNota(Sms sms);
    
    void _insertEmailNota(Sms ss);
    
    List<Sms> selectByRank(List<String> Sms);
    
    List<Sms> selectByCountry(List<String> lst);
    
    List<Sms> selectByRankAndCountry(SmsLists sl);
    
}
