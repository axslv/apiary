/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers.n3;

import java.util.List;
import lv.nmc.entities.n3.GroupEntry;

/**
 *
 * @author jm
 */
public interface GroupMapper {

    List<GroupEntry> fetchMovableGroups(GroupEntry ge);

    List<GroupEntry> groupByDateStart(GroupEntry ge);

    List<GroupEntry> fetchGroupsByCourses(GroupEntry ge);

    List<GroupEntry> fetchSchedule(GroupEntry ge);

    List<GroupEntry> lastGroup(GroupEntry ge);

    List<GroupEntry> fetchInstructorsAssigned(GroupEntry ge);
    
    void _updateInsertGroup(GroupEntry ge);

    void _updateRegReqPrinted(GroupEntry ge);

    void _updateFinProtPrinted(GroupEntry ge);

    void _updateCertPrinted(GroupEntry ge);

    void _updateChangeInsAssistants1(GroupEntry ge);

    void _updateChangeInsAssistants2(GroupEntry ge);

    void _updateChangeInsAssistants3(GroupEntry ge);

    void _updateChangeInsAssistants4(GroupEntry ge);

    void _updateInsertInstructors(GroupEntry ge);

    void _updateChangeGroupDates(GroupEntry ge);

    void _updateChangeGroupName(GroupEntry ge);

    void _updateChangeRegDates(GroupEntry ge);

    void _updateChangePreregDates(GroupEntry ge);

    void _updateRemoveInsAss(GroupEntry ge);
    
    
}
