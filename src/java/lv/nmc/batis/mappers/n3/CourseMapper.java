/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers.n3;

import java.util.List;
import lv.nmc.entities.n3.CourseEntry;
import lv.nmc.entities.n3.CourseIncomeQuery;

/**
 *
 * @author jm
 */
public interface CourseMapper {

    List<CourseEntry> selectTemplates(CourseEntry ce);

    List<CourseEntry> courseDocuments(CourseEntry ce);

    List<CourseEntry> allCourseDocuments(CourseEntry ce);

    List<CourseEntry> courseDetails(CourseEntry ce);

    List<CourseEntry> domainByCourse(CourseEntry ce);

    List<CourseEntry> fetchDomains(CourseEntry ce);

    List<CourseEntry> courseDetailsByAbbr(CourseEntry ce);

    List<CourseEntry> fetchDptByDomain(CourseEntry ce);

    List<CourseEntry> coursesByDptFull(CourseEntry ce);

    List<CourseEntry> coursesByDpt(CourseEntry ce);

    List<CourseEntry> coursesByDepartments(CourseIncomeQuery cq);

    List<CourseEntry> courseIdsByNames(CourseIncomeQuery cq);

    List<CourseEntry> coursesByDomain(CourseEntry cq);

    List<CourseEntry> allCourses(CourseEntry cq);

    void _updateInsertCourseDocument(CourseEntry ce);

    void _deleteDeleteCourseDocument(CourseEntry ce);

    /* List<DocumentTable> courseDocuments(Integer courseId);
    
     List<DocumentTable> courseDocumentsByCids(MultipleCourseIds mpc);
    
     List<DomainTable> fetchDomains(Integer id);
    
     void insertCourse(CourseEntry ce);
    
     void updateCourse(CourseEntry ce);
    
     List<UserHistory> courseLink(Integer courseId);
    
     List<CourseEntry> selectCourseLength(Integer courseId);
    
     List<CourseEntry> coursesListByDomain(String domain);
    
     List<GroupEntity> groupsListByDomain(GroupQuery qe);
    
     List<GroupEntity> groupsListByCid(GroupQuery qe);
    
     void registerGroup(Integer groupId);
    
     void certGroup(Integer groupId);
    
     void finalizeGroup(Integer groupId);
    
     void renameGroup(GroupEntity ge);
    
     void updateGroupDetails(GroupEntity ge);
    
     void setDeadline(GroupEntity ge);
    
     void setRegDateByDate(GroupEntity ge);
    
     void setRegDateById(GroupEntity ge);
    
     void updateCoursePrices(CourseEntry ce);
    
     void deleteInstructorsGroup(Integer groupId);
     */
    void _updateChangeCourseDetails(CourseEntry ce);

    void _updateInsertCourse(CourseEntry ce);
}
