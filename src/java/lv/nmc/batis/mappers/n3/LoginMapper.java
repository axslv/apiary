/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers.n3;

import java.util.List;
import lv.nmc.entities.n3.StaffEntry;

public interface LoginMapper {
    //TASK RELATED

    List<StaffEntry> checkLogin(StaffEntry te);   
    
    List<StaffEntry> selectGroups(StaffEntry te);
}
