/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers.n3;

import java.util.List;
import lv.nmc.entities.n3.RegistrationEntry;

/**
 *
 * @author jm
 */
public interface RegistrationMapper {
    
    void _updateInsertExam(RegistrationEntry re);
    
    void _updateDocumentType(RegistrationEntry ge);
    
    void _updateDeleteDocumentType(RegistrationEntry ge);
    
    void _insertDocumentType(RegistrationEntry ge);
    
    void _updateValidTill(RegistrationEntry ge);
    
    void _updateIssuer(RegistrationEntry ge);
    
    void _updateDocNum(RegistrationEntry ge);
    
    void _updateIssueDate(RegistrationEntry ge);           
    
    void _updateDeleteDocument(RegistrationEntry ge);
    
    void _insertDocumentRecord(RegistrationEntry ge);
    
    void _updateInsertRegistration(RegistrationEntry ge);
    
    List<RegistrationEntry> selectAllDocuments(RegistrationEntry ge);
    
    List<RegistrationEntry> selectUserDocuments(RegistrationEntry ge);
    
    List<RegistrationEntry> certInfo(RegistrationEntry ge);
    
    List<RegistrationEntry> checkCertificate(RegistrationEntry ge);
    
    List<RegistrationEntry> paymentsByUserId(RegistrationEntry ge);

    List<RegistrationEntry> checkExamPassed(RegistrationEntry ge);
    
    List<RegistrationEntry> checkPaymentsCompleted(RegistrationEntry ge);

    List<RegistrationEntry> fetchRegisteredClients(RegistrationEntry ge);

    List<RegistrationEntry> fetchCompanies(RegistrationEntry ge);

    List<RegistrationEntry> checkFutureRegistrations(RegistrationEntry ge);

    List<RegistrationEntry> paymentDetails(RegistrationEntry ge);

    List<RegistrationEntry> previousPayments(RegistrationEntry ge);

    List<RegistrationEntry> selectPreregistrations(RegistrationEntry ge);

    List<RegistrationEntry> searchPreregistered(RegistrationEntry ge);

    List<RegistrationEntry> selectDelays(RegistrationEntry ge);

    List<RegistrationEntry> checkPreRegistrations(RegistrationEntry re);
    
    List<RegistrationEntry> checkClientDocuments(RegistrationEntry re);
    
    List<RegistrationEntry> findPayment(RegistrationEntry re);
    
    void _updateErasePayment(RegistrationEntry re);
    
    void _updateRestorePayment(RegistrationEntry re);
    
    void _updateSaveSupervisor(RegistrationEntry re);
    
    void _updateDeleteCert(RegistrationEntry re);

    void _updateBanUser(RegistrationEntry re);
   
    void _updateGiveCertificate(RegistrationEntry re);

    void _updateUnBanUser(RegistrationEntry re);

    void _updateDeleteRegistration(RegistrationEntry re);

    void _updateInsertPaymentReg(RegistrationEntry re);

    void _updateRegisterUser(RegistrationEntry re);

    void _deleteCrewComment(RegistrationEntry re);

    void _insertCrewComment(RegistrationEntry re);

    void _deleteTmpDiscount(RegistrationEntry re);

    void _insertTmpDiscount(RegistrationEntry re);

    void _updateCertGiven(RegistrationEntry re);

    void _updateRegisterPayment(RegistrationEntry re);

    void _updatePaymentsInfo(RegistrationEntry re);

    void _updateInsertPayment(RegistrationEntry re);

    void _updateChangeRegdate(RegistrationEntry re);

    void _updateSaveRegistration(RegistrationEntry re);

    void _insertInitialCrew(RegistrationEntry re);

    void _insertInitialDiscount(RegistrationEntry re);

    void _deletePreregistration(RegistrationEntry re);

    void _updateRegisterDiscount(RegistrationEntry re);

    void _updateUnenrollClient(RegistrationEntry re);

    void _updateDeletePayment(RegistrationEntry re);

    void _updateMoveToGroup(RegistrationEntry re);

    void _updateMergeDupes(RegistrationEntry re);
    
    void _updateDeleteExam(RegistrationEntry re);
    
    void _updateChangePaymentsCompany(RegistrationEntry re);
    
}
