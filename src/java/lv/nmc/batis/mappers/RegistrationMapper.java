/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers;

import java.util.List;
import lv.nmc.entities.RegistrationEntry;

/**
 *
 * @author jm
 */
public interface RegistrationMapper {
    
    List<RegistrationEntry> paymentsByUserId(RegistrationEntry ge);
    
    List<RegistrationEntry> fetchPaidSum(RegistrationEntry ge);
    
    List<RegistrationEntry> selectPaidRecords(RegistrationEntry ge);
    
    List<RegistrationEntry> paymentDetailsById(RegistrationEntry ge);

    List<RegistrationEntry> fetchRegisteredClients(RegistrationEntry ge);

    List<RegistrationEntry> fetchCompanies(RegistrationEntry ge);
    
    List<RegistrationEntry> checkFutureRegistrations(RegistrationEntry ge);
    
    List<RegistrationEntry> checkPastRegistrations(RegistrationEntry ge);
    
    List<RegistrationEntry> paymentDetails(RegistrationEntry ge);
    
    List<RegistrationEntry> previousPayments(RegistrationEntry ge);
    
    List<RegistrationEntry> prePaymentsByUserId(RegistrationEntry ge);
    
    List<RegistrationEntry> prePaymentsByUserDetails(RegistrationEntry re);
    
    void _updateUnRegisterPayment(RegistrationEntry pe);
    
    void _updateInsertAdvance(RegistrationEntry re);

    void _deleteCrewComment(RegistrationEntry re);

    void _insertCrewComment(RegistrationEntry re);
    
     void _deleteTmpDiscount(RegistrationEntry re);

    void _insertTmpDiscount(RegistrationEntry re);   

    void _updateCertGiven(RegistrationEntry re);
    
    void _updateRegisterPayment(RegistrationEntry ку);
    
    void _updatePaymentsInfo(RegistrationEntry re);
    
    void _updateInsertPayment(RegistrationEntry re);
}
