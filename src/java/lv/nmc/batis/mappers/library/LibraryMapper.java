/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers.library;

import java.util.List;
import lv.nmc.entities.BookEntry;


public interface LibraryMapper {
    
    List<BookEntry> bookDetailsById(BookEntry be);
    
    List<BookEntry> booksByStudent(BookEntry be);
    
    void _updateReturnBook(BookEntry be);
    
    void _updateGiveBook(BookEntry be);
    
    void _updateAddBook(BookEntry be);
    
    void _updateSaveBook(BookEntry be);
    
    
    
}
