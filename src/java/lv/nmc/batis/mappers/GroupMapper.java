/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers;

import java.util.List;
import lv.nmc.entities.GroupEntry;

/**
 *
 * @author jm
 */
public interface GroupMapper {

    List<GroupEntry> fetchGroupsByCourses(GroupEntry ge);

    void _updateRegReqPrinted(GroupEntry ge);

    void _updateFinProtPrinted(GroupEntry ge);

    void _updateCertPrinted(GroupEntry ge);
}
