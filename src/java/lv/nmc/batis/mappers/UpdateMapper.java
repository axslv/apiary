/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers;

import java.util.List;
import lv.nmc.entities.UpdateEntry;

/**
 *
 * @author jm
 */
public interface UpdateMapper {
    List<UpdateEntry> appDetails(UpdateEntry ue);
}
