/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers;

import java.util.List;
import lv.nmc.entities.PaymentEntry;

/**
 *
 * @author jm
 */
public interface PaymentMapper {
    
    List<PaymentEntry> selectPaidDelayed(PaymentEntry pm);
    
    List<PaymentEntry>possibleDates(PaymentEntry pm);
    
   
}
