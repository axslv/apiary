/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers.form;

import java.util.List;
import lv.nmc.entities.form.*;

/**
 *
 * @author jm
 */
public interface FormMapper {

    List<FormData> headerDetails(FormData fd);
    
    List<FormData> questionDetails(FormData fd);
    
    List<FormData> previousAnswers(FormData fd);
    
    void insertAnswers(List<FormData> ls);
    
    void insertText(FormData fd);
          
}
