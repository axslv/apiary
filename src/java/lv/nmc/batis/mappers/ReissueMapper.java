/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers;


import java.util.List;
import lv.nmc.entities.ReissueEntry;

/**
 *
 * @author jm
 */
public interface ReissueMapper {
    
    List<ReissueEntry> certByName(ReissueEntry re);
    
    List<ReissueEntry> selectCourses(ReissueEntry re);

    List<ReissueEntry> selectReissued(ReissueEntry re);
    
    void _updateAddReissueRecord(ReissueEntry re);
    
    void _updateAddCertificate(ReissueEntry re);
    
    void _updateUnOrphan(ReissueEntry re);
    
    void _updateChangeIssueDate(ReissueEntry re);
    
    List<ReissueEntry> recordCount(ReissueEntry re);
    
    List<ReissueEntry>  distinctGroups(ReissueEntry re); 
    
    List<ReissueEntry>  lastGroup(ReissueEntry re);
    
    List<ReissueEntry>  newUsers(ReissueEntry re); 

    List<ReissueEntry>  selectLastCert(ReissueEntry re); 
}
