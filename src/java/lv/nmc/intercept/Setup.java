/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.intercept;

import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.controller.ExecutionContext;
import net.sourceforge.stripes.controller.Interceptor;
import net.sourceforge.stripes.controller.Intercepts;
import net.sourceforge.stripes.controller.LifecycleStage;

@Intercepts(LifecycleStage.HandlerResolution)
public class Setup implements Interceptor {

    /**
     * Intercepts execution and checks that the user has appropriate
     * permissions.
     */
    @Override
    public Resolution intercept(ExecutionContext ctx) throws Exception {
        
      /*  if (ctx.getActionBeanContext().getEventName()) {
            
        }*/
       

        Resolution resolution = ctx.proceed();
        return resolution;

    }

}
