/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lv.nmc.xext;
import javax.mail.PasswordAuthentication;
/**
 *import javax.mail.Authenticator;z
 * @author emma
 */
 class SMTPAuthenticator extends javax.mail.Authenticator {
        private String username;
        private String password;

        public SMTPAuthenticator(String username, String password) {
            this.username = username;
            this.password = password;
        }

        @Override
        public PasswordAuthentication getPasswordAuthentication() {
           return new PasswordAuthentication(this.username,this.password);
        }
    }
