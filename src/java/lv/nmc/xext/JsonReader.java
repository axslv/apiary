/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.xext;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;
import com.thoughtworks.xstream.io.json.JsonWriter;
import java.io.Writer;

/**
 *
 * @author mj
 */
public class JsonReader {

    private XStream xs = new XStream(new JettisonMappedXmlDriver());
    private final Class cls;

    public JsonReader(Class cls) {
        this.cls = cls;
        xs = new XStream(new JsonHierarchicalStreamDriver() {
            @Override
            public HierarchicalStreamWriter createWriter(Writer writer) {
                return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
            }
        });
    }

    public String object2xml(Object ob) {
        xs.processAnnotations(cls);
        return xs.toXML(ob);
    }

}
