/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.xext;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import com.thoughtworks.xstream.io.xml.StaxDriver;

/**
 *
 * @author mj
 */
public class XWriter {
    private XStream xs = new XStream(new StaxDriver());
    private final Class cls;
    private Object cast;
    
    public XWriter(Class cls) {
        this.cls = cls;
    }
    
    public void xml2Object(String ob, String type) {
        
        if (type.equals("json")) {
            xs = new XStream(new JettisonMappedXmlDriver());
           // System.out.println("i'm json");
        }
        
        xs.processAnnotations(cls);
        
        Object gvna = xs.fromXML(ob);
        cast = cls.cast(gvna);
       
        
    }

    /**
     * @return the cast
     */
    public Object getCast() { 
        return cast;
    }
    
}
