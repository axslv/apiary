/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.interceptors;

import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.controller.ExecutionContext;
import net.sourceforge.stripes.controller.Interceptor;
import net.sourceforge.stripes.controller.Intercepts;
import net.sourceforge.stripes.controller.LifecycleStage;

@Intercepts(LifecycleStage.RequestInit)
public class SXInterceptor implements Interceptor {

    protected boolean loggedIn = false;
    protected Resolution local;

    @Override
    public Resolution intercept(ExecutionContext ctx) throws Exception {
        Resolution resolution = ctx.proceed();

        
        if (ctx.getActionBeanContext().getRequest().getRequestURI().equals("/apiary/validate/")) {
            this.local = resolution;
            return this.local;
        }

        String version = ctx.getActionBeanContext().getRequest().getParameter("version");
        //System.out.println("Version: " + version);

        if (null == version) {
            return new RedirectResolution("/index.jsp");
        }

        if (!ctx.getActionBeanContext().getServletContext().getInitParameter("n3.version").equals(version)) {
            return new RedirectResolution("/index.jsp");
        }

        this.local = resolution;
        return this.local;

    }

}
