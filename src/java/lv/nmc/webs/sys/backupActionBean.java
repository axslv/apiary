/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.webs.sys;

import java.lang.reflect.InvocationTargetException;
import lv.nmc.dao.sys.BackupDao;
import lv.nmc.entities.sys.BackupEntry;
import lv.nmc.xext.XReader;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import org.apache.commons.beanutils.BeanUtils;

/**
 * Reflects lv.nmc.entities.StaffEntry
 *
 */
@UrlBinding("/sys/BackupService.rs")
public class backupActionBean implements ActionBean, ValidationErrorHandler {

    private ActionBeanContext context;
    private String xmlOut = "";
    private BackupEntry backupEntry = new BackupEntry();
    private String event = "noEvent";

    @DefaultHandler
    @HandlesEvent("dbRun")
    public Resolution executeSelect() {
        prepareBean();
        BackupDao sd = new BackupDao();

        if (event.contains("_update")) {
            sd.executeUpdate(event, backupEntry);
        } else if (event.contains("_delete")) {
            sd.executeUpdate(event, backupEntry);
        } else if (event.contains("_insert")) {
            sd.executeUpdate(event, backupEntry);
        } else {
            sd.executeSelect(event, backupEntry);
        }

        setXmlOut(new XReader(BackupEntry.class).object2xml(sd.getResults()));
        return new ForwardResolution("/WEB-INF/views/sys/BackupEntry.jsp");

    }

    protected void prepareBean() {
        if (!"dbRun".equals(context.getEventName())) {
            return;
        }

        try {
            BeanUtils.populate(backupEntry, context.getRequest().getParameterMap());
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc!");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        }

    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors ve) throws Exception {
        return new ForwardResolution("/WEB-INF/views/n3/special.jsp");
    }

    /**
     * @return the context
     */
    @Override
    public ActionBeanContext getContext() {
        return context;
    }

    /**
     * @param context the context to set
     */
    @Override
    public void setContext(ActionBeanContext context) {
        this.context = context;
    }

    /**
     * @return the xmlOut
     */
    public String getXmlOut() {
        return xmlOut;
    }

    /**
     * @return the staffEntry
     */
    public BackupEntry getStaffEntry() {
        return backupEntry;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @param xmlOut the xmlOut to set
     */
    public void setXmlOut(String xmlOut) {
        this.xmlOut = xmlOut;
    }

    /**
     * @return the courseEntry
     */
    public BackupEntry getBackupEntry() {
        return backupEntry;
    }

    /**
     * @param courseEntry the courseEntry to set
     */
    public void setBackupEntry(BackupEntry courseEntry) {
        this.backupEntry = courseEntry;
    }

}
