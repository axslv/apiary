/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.webs.n3;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import lv.nmc.batis.mappers.n3.SmsMapper;
import lv.nmc.dao.n3.NregDbFactory;
import lv.nmc.dao.n3.SmsDao;
import lv.nmc.entities.n3.Sms;
import lv.nmc.entities.n3.SmsLists;

import lv.nmc.xext.XReader;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.ibatis.session.SqlSession;

/**
 * Reflects lv.nmc.entities.StaffEntry
 *
 */
@UrlBinding("/n3/SmsService.cs")
public class smsEntryActionBean implements ActionBean, ValidationErrorHandler {

    private ActionBeanContext context;
    private String xmlOut = "";
    private Sms sms = new Sms();
    private String event = "noEvent";
    private Integer sms_id, notified_m;
    private String comment;

    @DefaultHandler
    @HandlesEvent("dbRun")
    public Resolution executeQuery() {
        prepareBean();
        SmsDao sd = new SmsDao();

        if (event.contains("_update")) {
            sd.executeUpdate(event, sms);
        } else if (event.contains("_delete")) {
            sd.executeUpdate(event, sms);
        } else if (event.contains("_insert")) {
            sd.executeUpdate(event, sms);
        } else {
            sd.executeSelect(event, sms);
        }

        setXmlOut(new XReader(Sms.class).object2xml(sd.getResults()));
        return new ForwardResolution("/WEB-INF/views/n3/smsEntry.jsp");

    }

    protected void prepareBean() {
        if (!"dbRun".equals(context.getEventName())) {
            return;
        }

        try {
            BeanUtils.populate(sms, context.getRequest().getParameterMap());
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc!");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        }

    }

    private List<String> ranks = new ArrayList<>();
    private List<String> ccodes = new ArrayList<>();
    private String list, list1;

    @HandlesEvent("byRank")
    public Resolution execMethod() {
        SqlSession session = NregDbFactory.getSqlSessionFactory().openSession();
        try {
            SmsMapper pm = session.getMapper(SmsMapper.class);
            List<Sms> ppc = new ArrayList<Sms>();

            String[] ids = getList().split(",");

            if (ids.length == 0) {
                return new ForwardResolution("/WEB-INF/views/n3/smsEntry.jsp");
            }

            for (int i = 0; i <= ids.length - 1; i++) {
                ranks.add(ids[i].trim());
            }

            ppc = pm.selectByRank(ranks);

            setXmlOut(new XReader(Sms.class).object2xml(ppc));
            // System.out.println(list);
        } finally {
            session.close();
        }

        return new ForwardResolution("/WEB-INF/views/n3/smsEntry.jsp");
    }

    @HandlesEvent("byRankAndCountry")
    public Resolution execRankAndCountry() {
        SqlSession session = NregDbFactory.getSqlSessionFactory().openSession();

        try {
            SmsMapper pm = session.getMapper(SmsMapper.class);
            List<Sms> ppc = new ArrayList<Sms>();

            String[] ids = getList().split(",");
            String[] ccds = getList1().split(",");

            if (ids.length == 0) {
                return new ForwardResolution("/WEB-INF/views/n3/smsEntry.jsp");
            }

            for (int i = 0; i <= ids.length - 1; i++) {
                ranks.add(ids[i].trim());
            }

            if (ccds.length == 0) {
                return new ForwardResolution("/WEB-INF/views/n3/smsEntry.jsp");
            }

            for (int i = 0; i <= ccds.length - 1; i++) {
                getCcodes().add(ccds[i].trim());
            }
            SmsLists sl = new SmsLists();
            sl.setRanks(ranks);
            sl.setCcodes(ccodes);

            ppc = pm.selectByRankAndCountry(sl);

            setXmlOut(new XReader(Sms.class).object2xml(ppc));
            // System.out.println(list);
        } finally {
            session.close();
        }

        return new ForwardResolution("/WEB-INF/views/n3/smsEntry.jsp");
    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors ve) throws Exception {
        return new ForwardResolution("/WEB-INF/views/n3/special.jsp");
    }

    /**
     * @return the context
     */
    @Override
    public ActionBeanContext getContext() {
        return context;
    }

    /**
     * @param context the context to set
     */
    @Override
    public void setContext(ActionBeanContext context) {
        this.context = context;
    }

    /**
     * @return the xmlOut
     */
    public String getXmlOut() {
        return xmlOut;
    }

    /**
     * @return the staffEntry
     */
    public Sms getStaffEntry() {
        return sms;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @param xmlOut the xmlOut to set
     */
    public void setXmlOut(String xmlOut) {
        this.xmlOut = xmlOut;
    }

    /**
     * @return the courseEntry
     */
    public Sms getSms() {
        return sms;
    }

    /**
     * @param courseEntry the courseEntry to set
     */
    public void setSms(Sms courseEntry) {
        this.sms = courseEntry;
    }

    /**
     * @return the sms_id
     */
    public Integer getSms_id() {
        return sms_id;
    }

    /**
     * @param sms_id the sms_id to set
     */
    public void setSms_id(Integer sms_id) {
        this.sms_id = sms_id;
    }

    /**
     * @return the notified_m
     */
    public Integer getNotified_m() {
        return notified_m;
    }

    /**
     * @param notified_m the notified_m to set
     */
    public void setNotified_m(Integer notified_m) {
        this.notified_m = notified_m;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return the ranks
     */
    public List<String> getRanks() {
        return ranks;
    }

    /**
     * @return the list
     */
    public String getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(String list) {
        this.list = list;
    }

    /**
     * @return the ccodes
     */
    public List<String> getCcodes() {
        return ccodes;
    }

    /**
     * @return the list1
     */
    public String getList1() {
        return list1;
    }

    /**
     * @param list1 the list1 to set
     */
    public void setList1(String list1) {
        this.list1 = list1;
    }

}
