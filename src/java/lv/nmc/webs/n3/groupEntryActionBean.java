/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.webs.n3;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import lv.nmc.dao.n3.GroupDao;
import lv.nmc.entities.n3.GroupEntry;
import lv.nmc.xext.XReader;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import org.apache.commons.beanutils.BeanUtils;

/**
 * Reflects lv.nmc.entities.StaffEntry
 * 
 */
@UrlBinding("/n3/GroupService.cs")
public class groupEntryActionBean implements ActionBean, ValidationErrorHandler {

    private ActionBeanContext context;
    private String xmlOut = "";
    private String event = "noEvent";
    private GroupEntry groupEntry = new GroupEntry();
    
    

    @DefaultHandler
    @HandlesEvent("dbRun")
    public Resolution executeSelect() {        
        prepareBean();
        GroupDao sd = new GroupDao();        
        if (event.contains("_update")) {
            sd.executeUpdate(event, getGroupEntry());
    //        System.out.println(event);
        } else if (event.contains("_delete")) {
            sd.executeUpdate(event, getGroupEntry());
        //    setXmlOut(new XReader(String.class).object2xml());
        } else {
            sd.executeSelect(event, getGroupEntry());
            setXmlOut(new XReader(GroupEntry.class).object2xml(sd.getResults()));
        }
   
        
        return new ForwardResolution("/WEB-INF/views/n3/groupEntry.jsp");

    }

    protected void prepareBean() {
        
        HashMap<String, String> filteredParams = new HashMap<String, String>();
        Set<String> params = context.getRequest().getParameterMap().keySet();
        List<Integer> cids = new ArrayList<Integer>();
        
        for (String param : params) {
            if (param.contains("List")) {
                cids.add(Integer.parseInt(context.getRequest().getParameter(param)));
                continue;
            }
            
            filteredParams.put(param, context.getRequest().getParameter(param));
        }
        
        try {
            BeanUtils.populate(getGroupEntry(), filteredParams);
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc!");
        } catch (InvocationTargetException ex) {
            ex.printStackTrace();
            System.out.println("Invocation target exc in AB");
        }
        
        this.groupEntry.setCourseIdsList(cids);

    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors ve) throws Exception {
        return new ForwardResolution("/WEB-INF/views/n3/special.jsp");
    }

    /**
     * @return the context
     */
    @Override
    public ActionBeanContext getContext() {
        return context;
    }

    /**
     * @param context the context to set
     */
    @Override
    public void setContext(ActionBeanContext context) {
        this.context = context;
    }

    /**
     * @return the xmlOut
     */
    public String getXmlOut() {
        return xmlOut;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

  
    /**
     * @param xmlOut the xmlOut to set
     */
    public void setXmlOut(String xmlOut) {
        this.xmlOut = xmlOut;
    }

    /**
     * @return the groupEntry
     */
    public GroupEntry getGroupEntry() {
        return groupEntry;
    }


}
