/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.webs.n3;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import lv.nmc.batis.mappers.n3.ClientMapper;
import lv.nmc.batis.mappers.n3.GroupMapper;
import lv.nmc.batis.mappers.n3.PaymentsMapper;
import lv.nmc.batis.mappers.n3.RegistrationMapper;
import lv.nmc.dao.n3.NregDbFactory;
import lv.nmc.dao.n3.PaymentDao;
import lv.nmc.entities.n3.ClientEntry;
import lv.nmc.entities.n3.Payment;
import lv.nmc.entities.n3.RegistrationEntry;
import lv.nmc.xext.XReader;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.ibatis.session.SqlSession;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

@UrlBinding("/n3/PaymentService.cs")
public class paymentsActionBean implements ActionBean, ValidationErrorHandler {

    private ActionBeanContext context;
    private String xmlOut = "";
    private Payment payment = new Payment();
    private String event = "noEvent";
    private List<Integer> paymentIds = new ArrayList<Integer>();
    private String list = "";

    @DefaultHandler
    @HandlesEvent("dbRun")
    public Resolution executeSelect() {
        // System.out.println(paymentIds.get(1));
        prepareBean();
        PaymentDao sd = new PaymentDao();
        //context.getRequest().getSession().setAttribute("pageNum", );

        if (event.contains("_update")) {
            sd.executeUpdate(event, payment);
        } else if (event.contains("_delete")) {
            sd.executeUpdate(event, payment);
        } else {
            sd.executeSelect(event, payment);
        }

        setXmlOut(new XReader(Payment.class).object2xml(sd.getResults()));
        return new ForwardResolution("/WEB-INF/views/n3/payment.jsp");

    }

    @HandlesEvent("detailsByMultipleIdsString")
    public Resolution byLongString() {
        // System.out.println(list);
        SqlSession session = NregDbFactory.getSqlSessionFactory().openSession();

        try {
            PaymentsMapper pm = session.getMapper(PaymentsMapper.class);
            List<Payment> ppc = new ArrayList<Payment>();

            String[] ids = list.split(",");

            if (ids.length == 0) {
                return new ForwardResolution("/WEB-INF/views/n3/payment.jsp");
            }

            for (int i = 0; i <= ids.length - 1; i++) {
                paymentIds.add(Integer.parseInt(ids[i]));
            }

            ppc = pm.detailsByMultipleIds(paymentIds);
            setXmlOut(new XReader(Payment.class).object2xml(ppc));
            // System.out.println(list);
        } finally {
            session.close();
        }

        return new ForwardResolution("/WEB-INF/views/n3/payment.jsp");
    }

    @HandlesEvent("detailsByMultipleIds")
    public Resolution execMethod() {
        SqlSession session = NregDbFactory.getSqlSessionFactory().openSession();

        try {
            PaymentsMapper pm = session.getMapper(PaymentsMapper.class);
            List<Payment> ppc = new ArrayList<Payment>();
            ppc = pm.detailsByMultipleIds(paymentIds);
            setXmlOut(new XReader(Payment.class).object2xml(ppc));
        } finally {
            session.close();
        }

        return new ForwardResolution("/WEB-INF/views/n3/payment.jsp");
    }

    protected void prepareBean() {
        if (!"dbRun".equals(context.getEventName())) {
            return;
        }

        try {
            BeanUtils.populate(payment, context.getRequest().getParameterMap());
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc!");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        }

    }

    @HandlesEvent("processAdvances")
    public Resolution processAdvances() {
        SqlSession session = NregDbFactory.getSqlSessionFactory().openSession();
        List<Payment> advance = new ArrayList<Payment>();

        try {
            PaymentsMapper pm = session.getMapper(PaymentsMapper.class);
            List<Payment> ppc = new ArrayList<Payment>();
            ppc = pm.selectForCron(new Payment());

            for (Payment pt : ppc) {
                pt.setActive(1);
                advance = pm.selectUniqueAdvance(pt);

                if (advance.isEmpty()) {
                    continue;
                }

            }

            setXmlOut(new XReader(Payment.class).object2xml(ppc));
        } finally {
            session.close();
        }

        return new ForwardResolution("/WEB-INF/views/n3/payment.jsp");
    }

  //  DateTimeFormatter dtf = DateTimeFormat.forPattern("d.MM.Y");
    DateTimeFormatter dtfd = DateTimeFormat.forPattern("d.MM.Y.");
    DateTimeFormatter dtf = DateTimeFormat.forPattern("d.MM.Y"); 
    DateTimeFormatter sqlDate = DateTimeFormat.forPattern("Y-M-d");
    DateTimeFormatter pdate = DateTimeFormat.forPattern("M/d/Y");

    private Integer courseId;
    // 223 for 1.kurs nav
    // 271 for 1.kurs eng

    
    //273 for 2.kurs eng
    //201 for 2.kurs nav
    
    
//274 for 3.kurs eng    
//225 for 3.kurs nav
    
   // 80c 
    
    @HandlesEvent("college15defewfef45")
    public Resolution collegePeople() throws Exception {
        SqlSession session = NregDbFactory.getSqlSessionFactory().openSession();
        List<Payment> college = new ArrayList<>();

        try {
            PaymentsMapper pm = session.getMapper(PaymentsMapper.class);
            RegistrationMapper rm = session.getMapper(RegistrationMapper.class);
            ClientMapper cm = session.getMapper(ClientMapper.class);
            GroupMapper gm = session.getMapper(GroupMapper.class);
            Integer userId = 0;

            college = pm.collegeOne(new Payment());
            

            for (Payment pt : college) {
                
                // 1. Check user id
                
                
                ClientEntry checkClient = new ClientEntry();
                RegistrationEntry re = new RegistrationEntry();
                checkClient.setNameSurname(pt.getNameSurname());
                checkClient.setPersonCode(pt.getPersonCode());
                checkClient.setRank("");
                
                cm.deleteFuckers(checkClient);
                continue;
                

              /*  List<ClientEntry> ls = cm.clientDetailsByPersonCodeAndName(checkClient);
                
                if (!ls.isEmpty()) {
                    re.setUserId(ls.get(0).getUserId()); // client already inserted
                } else {
                    ClientEntry ce = new ClientEntry(); //Client not inserted, create new client entry
                    ce.setNameSurname(pt.getNameSurname()); //
                    ce.setPersonCode(pt.getPersonCode());
                    ce.setContractNum(pt.getCertNr());
                    cm._updateInsertClient(ce); //commit insertion
                    session.commit();
                    re.setUserId(ce.getUserId());
                } //userId found
                
                // 2. Check group
                StringBuilder b = new StringBuilder(pt.getDateStart());
                DateTime dt = new DateTime();
                
                try {
                    dt = DateTime.parse(b.toString(), dtfd); //dateStart. ebanie tochki i krivoj format ot unterov iz akvariuma
                } catch (IllegalArgumentException eee) {
                    dt = DateTime.parse(b.toString(), dtf);
                } 
                
                //b.replace(pt.getDateStart().lastIndexOf("."), pt.getDateStart().lastIndexOf(".") + 1, ""); //remove dot from date start
                DateTime de = new DateTime();
                
                try {
                    de = DateTime.parse(b.toString(), dtfd);
                } catch (IllegalArgumentException eee) {
                    b.append(".");
                    de = DateTime.parse(b.toString(), dtf); //dateEnd. ebanie tochki i krivoj format ot unterov iz akvariuma 
                }
                
                //b = new StringBuilder(pt.getDateEnd());
                //b.replace(pt.getDateEnd().lastIndexOf("."), pt.getDateEnd().lastIndexOf(".") + 1, ""); //remove dot from date end                


                
                GroupEntry ge = new GroupEntry();                
                ge.setCourseId(pt.getCourseId());                
                ge.setDateStart(dt.toString(sqlDate));
                ge.setDateEnd(de.toString(sqlDate));
                
                re.setCourseId(pt.getCourseId());
                re.setDateStart(dt.toString(sqlDate));
                re.setDateEnd(de.toString(sqlDate));
                
                List<GroupEntry> group = new ArrayList<GroupEntry>();
                group = gm.groupByDateStart(ge);
                
                if (group.isEmpty()) {
                    gm._updateInsertGroup(ge);
                    session.commit();
                    re.setGroupId(ge.getGroupId());
                } else {
                    re.setGroupId(group.get(0).getGroupId());
                }
                
                rm._updateInsertRegistration(re);
                session.commit();
                
                
                //create payment record
                Payment cpay = new Payment();                
                re.setDateEnd(de.toString(sqlDate));
                re.setPrice(pt.getDisplayPrice());
                re.setCompany("pats");
                
                rm._updateInsertPaymentReg(re);
                rm._insertTmpDiscount(re);
                session.commit();
                
                if (null == pt.getPaymentDate().trim() || pt.getPaymentDate().trim().isEmpty()) {
                    continue;
                }
                
                DateTime payDate = DateTime.parse(pt.getPaymentDate(), pdate); //paymentDate               
                BigDecimal paid = new BigDecimal(0.00);
                BigDecimal debt = new BigDecimal(pt.getDisplayPrice().split(" ")[0].trim());
                
                cpay.setUserId(re.getUserId());
                cpay.setGroupId(re.getGroupId());
                cpay.setCourseId(re.getCourseId());
                cpay.setRegistrationId(re.getRegistrationId());
                cpay.setDateStart(re.getDateStart());
                cpay.setDateEnd(de.toString(sqlDate));
                cpay.setPaidSum(pt.getDisplayPrice());
                cpay.setPaymentId(re.getPaymentId());
                cpay.setPaymentDate(payDate.toString(sqlDate));
                cpay.setBank("BANK");
                
                pm._updateInsertPayment(cpay);
                
                if (cpay.getPaidSum().equals(pt.getDisplayPrice())) {
                    pm._updateDeactivatePayment(cpay);
                }
                
                session.commit();*/
     
            }

        } finally {
            session.close();
        }

        return new ForwardResolution("/WEB-INF/views/n3/special.jsp");
    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors ve) throws Exception {
        return new ForwardResolution("/WEB-INF/views/n3/special.jsp");
    }

    /**
     * @return the context
     */
    @Override
    public ActionBeanContext getContext() {
        return context;
    }

    /**
     * @param context the context to set
     */
    @Override
    public void setContext(ActionBeanContext context) {
        this.context = context;
    }

    /**
     * @return the xmlOut
     */
    public String getXmlOut() {
        return xmlOut;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @param xmlOut the xmlOut to set
     */
    public void setXmlOut(String xmlOut) {
        this.xmlOut = xmlOut;
    }

    /**
     * @return the paymentIds
     */
    public List<Integer> getPaymentIds() {
        return paymentIds;
    }

    /**
     * @return the list
     */
    public String getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(String list) {
        this.list = list;
    }

    /**
     * @return the courseId
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * @param courseId the courseId to set
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

}
