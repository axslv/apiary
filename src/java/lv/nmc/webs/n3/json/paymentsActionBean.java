/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.webs.n3.json;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import lv.nmc.batis.mappers.n3.PaymentsMapper;
import lv.nmc.dao.n3.NregDbFactory;
import lv.nmc.dao.n3.PaymentDao;
import lv.nmc.entities.n3.Payment;
import lv.nmc.xext.JsonReader;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.ibatis.session.SqlSession;

@UrlBinding("/n3/PaymentService.json")
public class paymentsActionBean implements ActionBean, ValidationErrorHandler {

    private ActionBeanContext context;
    private String xmlOut = "";
    private Payment payment = new Payment();
    private String event = "noEvent";
    private List<Integer> paymentIds = new ArrayList<Integer>();
    private String list = "";

    @DefaultHandler
    @HandlesEvent("dbRun")
    public Resolution executeSelect() {
       // System.out.println(paymentIds.get(1));
        prepareBean();
        PaymentDao sd = new PaymentDao();
        //context.getRequest().getSession().setAttribute("pageNum", );
        

        if (event.contains("_update")) {
            sd.executeUpdate(event, payment);
        } else if (event.contains("_delete")) {
            sd.executeUpdate(event, payment);
        } else {
            sd.executeSelect(event, payment);
        }

        setXmlOut(new JsonReader(Payment.class).object2xml(sd.getResults()));
        return new ForwardResolution("/WEB-INF/views/n3/payment.jsp");

    }
    
    @HandlesEvent("detailsByMultipleIdsString")
    public Resolution byLongString() {
        SqlSession session = NregDbFactory.getSqlSessionFactory().openSession(); 
        
        try {
            PaymentsMapper pm = session.getMapper(PaymentsMapper.class);
            List<Payment> ppc = new ArrayList<Payment>();
            String[] ids = list.split(",");
            
            if (ids.length == 0) {
                return new ForwardResolution("/WEB-INF/views/n3/payment.jsp");
            }
            
            for (int i = 0; i< ids.length-1; i++) {
                paymentIds.add(Integer.parseInt(ids[i]));
            }
            
            ppc = pm.detailsByMultipleIds(paymentIds);
            setXmlOut(new JsonReader(Payment.class).object2xml(ppc));
        } finally {
            session.close();
        }
        
        return new ForwardResolution("/WEB-INF/views/n3/payment.jsp");
    }
    
    @HandlesEvent("detailsByMultipleIds")
    public Resolution execMethod() {
        SqlSession session = NregDbFactory.getSqlSessionFactory().openSession(); 
        
        try {
            PaymentsMapper pm = session.getMapper(PaymentsMapper.class);
            List<Payment> ppc = new ArrayList<Payment>();
            ppc = pm.detailsByMultipleIds(paymentIds);
            setXmlOut(new JsonReader(Payment.class).object2xml(ppc));
        } finally {
            session.close();
        }
        
        return new ForwardResolution("/WEB-INF/views/n3/payment.jsp");
    }

    protected void prepareBean() {
        if (!"dbRun".equals(context.getEventName())) {
            return;
        }

        try {
            BeanUtils.populate(payment, context.getRequest().getParameterMap());
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc!");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        }

    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors ve) throws Exception {
        return new ForwardResolution("/WEB-INF/views/n3/special.jsp");
    }

    /**
     * @return the context
     */
    @Override
    public ActionBeanContext getContext() {
        return context;
    }

    /**
     * @param context the context to set
     */
    @Override
    public void setContext(ActionBeanContext context) {
        this.context = context;
    }

    /**
     * @return the xmlOut
     */
    public String getXmlOut() {
        return xmlOut;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @param xmlOut the xmlOut to set
     */
    public void setXmlOut(String xmlOut) {
        this.xmlOut = xmlOut;
    }

    /**
     * @return the paymentIds
     */
    public List<Integer> getPaymentIds() {
        return paymentIds;
    }

    /**
     * @return the list
     */
    public String getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(String list) {
        this.list = list;
    }

}
