/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.webs.n3.json;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import lv.nmc.dao.n3.CourseDao;
import lv.nmc.entities.n3.CourseEntry;
import lv.nmc.entities.n3.CourseIncomeQuery;
import lv.nmc.xext.JsonReader;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import org.apache.commons.beanutils.BeanUtils;

/**
 * Reflects lv.nmc.entities.StaffEntry
 * 
 */
@UrlBinding("/n3/CourseService.json")

public class courseEntryActionBean implements ActionBean, ValidationErrorHandler {

    private ActionBeanContext context;
    private String xmlOut = "";
    private CourseEntry courseEntry = new CourseEntry();
    private String event = "noEvent";
        private List<Integer> courseId = new ArrayList<Integer>();
    private List<String> courseNames = new ArrayList<String>();
    private List<String> departmentNames = new ArrayList<String>();
    private String domain;
    
    
        
    @HandlesEvent("coursesByDepartments")
        public Resolution coursesByDepartments() {  
        List<CourseEntry> ls = new ArrayList<CourseEntry>();
        CourseIncomeQuery cq = new CourseIncomeQuery();
        cq.setDomain(getDomain());
        cq.setDepartments(getDepartmentNames());
        try {
            ls = CourseDao.coursesByDepartments(cq);
            setXmlOut(new JsonReader(CourseEntry.class).object2xml(ls));            
        } catch (Exception ex) {
            Logger.getLogger(courseEntryActionBean.class.getName()).log(Level.SEVERE, null, ex);
        }      
   
      //  setXmlOut(new JsonReader(StaffEntry.class).object2xml(sd.getResults()));
        return new ForwardResolution("/WEB-INF/views/n3/json/courseEntry.jsp");

    }
    

    @DefaultHandler
    @HandlesEvent("dbRun")
    public Resolution executeSelect() {        
        prepareBean();
        CourseDao sd = new CourseDao();
        
        if (event.contains("_update")) {
            sd.executeUpdate(event, courseEntry);
        } else if (event.contains("_delete")) {
            sd.executeUpdate(event, courseEntry);
        } else {
            sd.executeSelect(event, courseEntry);
        }
   
        setXmlOut(new JsonReader(CourseEntry.class).object2xml(sd.getResults()));
        return new ForwardResolution("/WEB-INF/views/n3/json/courseEntry.jsp");

    }

    protected void prepareBean() {        
        if (!"dbRun".equals(context.getEventName())) {
            return;
        }
        
        try {
            BeanUtils.populate(courseEntry, context.getRequest().getParameterMap());
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc!");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        }

    }


    @Override
    public Resolution handleValidationErrors(ValidationErrors ve) throws Exception {
        return new ForwardResolution("/WEB-INF/views/n3/json/special.jsp");
    }

    /**
     * @return the context
     */
    @Override
    public ActionBeanContext getContext() {
        return context;
    }

    /**
     * @param context the context to set
     */
    @Override
    public void setContext(ActionBeanContext context) {
        this.context = context;
    }

    /**
     * @return the xmlOut
     */
    public String getXmlOut() {
        return xmlOut;
    }

    /**
     * @return the staffEntry
     */
    public CourseEntry getStaffEntry() {
        return courseEntry;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

  
    /**
     * @param xmlOut the xmlOut to set
     */
    public void setXmlOut(String xmlOut) {
        this.xmlOut = xmlOut;
    }

    /**
     * @return the courseEntry
     */
    public CourseEntry getCourseEntry() {
        return courseEntry;
    }

    /**
     * @param courseEntry the courseEntry to set
     */
    public void setCourseEntry(CourseEntry courseEntry) {
        this.courseEntry = courseEntry;
    }

    /**
     * @return the courseNames
     */
    public List<String> getCourseNames() {
        return courseNames;
    }

    /**
     * @param courseNames the courseNames to set
     */
    public void setCourseNames(List<String> courseNames) {
        this.courseNames = courseNames;
    }

    /**
     * @return the departmentNames
     */
    public List<String> getDepartmentNames() {
        return departmentNames;
    }

    /**
     * @param departmentNames the departmentNames to set
     */
    public void setDepartmentNames(List<String> departmentNames) {
        this.departmentNames = departmentNames;
    }

    /**
     * @return the domain
     */
    public String getDomain() {
        return domain;
    }

    /**
     * @param domain the domain to set
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

   

}
