/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.webs.n3;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import lv.nmc.dao.n3.CollegeDao;
import lv.nmc.entities.n3.col.CollegeEntry;
import lv.nmc.entities.n3.col.Semester;
import lv.nmc.util.SemesterFabric;
import lv.nmc.xext.XReader;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import org.apache.commons.beanutils.BeanUtils;

/**
 * Reflects lv.nmc.entities.StaffEntry
 *
 */
@UrlBinding("/n3/CollegeService.cs")
public class collegeEntryActionBean implements ActionBean, ValidationErrorHandler {

    private ActionBeanContext context;
    private String xmlOut = "";
    private CollegeEntry courseEntry = new CollegeEntry();
    private String event;

    
    @HandlesEvent("courseMap")
    public Resolution courseMap() {
        
        
        
        CollegeEntry col = new CollegeEntry();
        XStream xs = new XStream(new StaxDriver());
        xs.processAnnotations(CollegeEntry.class);
        this.xmlOut = xs.toXML(col);
        
        return new ForwardResolution("/WEB-INF/views/n3/collegeEntry.jsp");

    }
    
    @HandlesEvent("fetchClientMap")
    public Resolution clientMap() {
        CollegeEntry col = new CollegeEntry();
        XStream xs = new XStream(new StaxDriver());
        xs.processAnnotations(CollegeEntry.class);
        this.xmlOut = xs.toXML(col);
        
        return new ForwardResolution("/WEB-INF/views/n3/collegeEntry.jsp");

    }

    @HandlesEvent("somevent")
    public Resolution testfire() {
        List<Semester> semesters = SemesterFabric.generateSemesters("2017-09-01", 1);
        
        XStream xs = new XStream(new StaxDriver());
        xs.processAnnotations(Semester.class);
        this.xmlOut = xs.toXML(semesters);
        
        return new ForwardResolution("/WEB-INF/views/n3/collegeEntry.jsp");

    }

    @DefaultHandler
    @HandlesEvent("dbRun")
    public Resolution executeSelect() {
        prepareBean();
        CollegeDao sd = new CollegeDao();

        if (event.contains("_update")) {
            sd.executeUpdate(event, courseEntry);
        } else if (event.contains("_delete")) {
            sd.executeUpdate(event, courseEntry);
        } else {
            sd.executeSelect(event, courseEntry);
        }

        setXmlOut(new XReader(CollegeEntry.class).object2xml(sd.getResults()));
        return new ForwardResolution("/WEB-INF/views/n3/collegeEntry.jsp");

    }

    protected void prepareBean() {
        if (!"dbRun".equals(context.getEventName())) {
            return;
        }

        try {
            BeanUtils.populate(courseEntry, context.getRequest().getParameterMap());
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc!");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc.");
            ex.printStackTrace();
        }

    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors ve) throws Exception {
        return new ForwardResolution("/WEB-INF/views/n3/special.jsp");
    }

    /**
     * @return the context
     */
    @Override
    public ActionBeanContext getContext() {
        return context;
    }

    /**
     * @param context the context to set
     */
    @Override
    public void setContext(ActionBeanContext context) {
        this.context = context;
    }

    /**
     * @return the xmlOut
     */
    public String getXmlOut() {
        return xmlOut;
    }

    /**
     * @return the staffEntry
     */
    public CollegeEntry getStaffEntry() {
        return courseEntry;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @param xmlOut the xmlOut to set
     */
    public void setXmlOut(String xmlOut) {
        this.xmlOut = xmlOut;
    }

    /**
     * @return the courseEntry
     */
    public CollegeEntry getCollegeEntry() {
        return courseEntry;
    }

    /**
     * @param courseEntry the courseEntry to set
     */
    public void setCollegeEntry(CollegeEntry courseEntry) {
        this.courseEntry = courseEntry;
    }


}
