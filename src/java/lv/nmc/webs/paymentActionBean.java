/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.webs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import lv.nmc.batis.mappers.PaymentMapper;
import lv.nmc.dao.MainDbFactory;
import lv.nmc.entities.PaymentEntry;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import org.apache.ibatis.session.SqlSession;
import org.joda.time.DateTime;
import org.joda.time.Months;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Reflects lv.nmc.entities.StaffEntry
 *
 */
@UrlBinding("/incremental.html")
public class paymentActionBean implements ActionBean, ValidationErrorHandler {

    private ActionBeanContext context;
    private List<PaymentEntry> paymentsList = new ArrayList<PaymentEntry>();
    HashMap<String, PaymentEntry> paysMap = new HashMap<String, PaymentEntry>();
    HashMap<String, PaymentEntry> datesMap = new HashMap<String, PaymentEntry>();
    private HashMap<String, Set<PaymentEntry>> uberMap = new HashMap<String, Set<PaymentEntry>>();
    private final DateTimeFormatter sqlDate = DateTimeFormat.forPattern("yyyy-MM-dd");
    private Set<String> possibleDates = new TreeSet<String>();

    @DefaultHandler
    @HandlesEvent("showIncome")
    public Resolution showIncomeGraph() {
        SqlSession session = MainDbFactory.getSqlSessionFactory().openSession();
        try {
            PaymentMapper pm = session.getMapper(PaymentMapper.class);          
            
            List<PaymentEntry> posDates = pm.possibleDates(new PaymentEntry());
            
            for (PaymentEntry ppe : posDates) {                
                DateTime payDate = sqlDate.parseDateTime(ppe.getDateEnd());
                StringBuilder sb = new StringBuilder().append(payDate.getYear()).append(".").append(payDate.getMonthOfYear());
                possibleDates.add(sb.toString());
            }  
            
                       
            
           

        } finally {
            session.close();
        }

        return new ForwardResolution("/WEB-INF/views/incremental.jsp");

    }

    @Override
    public ActionBeanContext getContext() {
        return context;
    }

    @Override
    public void setContext(ActionBeanContext context) {
        this.context = context;
    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors ve) throws Exception {
        return new ForwardResolution("/WEB-INF/views/special.jsp");
    }

    /**
     * @return the possibleDates
     */
    public Set<String> getPossibleDates() {
        return possibleDates;
    }

    /**
     * @return the uberMap
     */
    public HashMap<String, Set<PaymentEntry>> getUberMap() {
        return uberMap;
    }
}
