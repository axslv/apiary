/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.webs;

import java.util.ArrayList;
import java.util.List;
import lv.nmc.batis.mappers.n3.ClientMapper;
import lv.nmc.batis.migrate.MigrationMapper;
import lv.nmc.dao.PostgreFactory;
import lv.nmc.dao.n3.NregDbFactory;
import lv.nmc.entities.UpdateEntry;
import lv.nmc.entities.n3.ClientEntry;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import org.apache.ibatis.session.SqlSession;

/**
 * Reflects lv.nmc.entities.StaffEntry
 *
 */
@UrlBinding("/{$event}/migrate.html")
public class migrateActionBean implements ActionBean, ValidationErrorHandler {

    private ActionBeanContext context;
    private String xmlOut = "";
    private String event = "noEvent";
    private UpdateEntry regEntry = new UpdateEntry();

    @DefaultHandler
    @HandlesEvent("dbRun")
    public Resolution executeSelect() {

        return new ForwardResolution("/WEB-INF/views/migrate.jsp");

    }

    private List<ClientEntry> myClients = new ArrayList<ClientEntry>();
    int i = 1;

    @HandlesEvent("transfer_clients")
    public Resolution transferClients() {
        SqlSession mysql = NregDbFactory.getSqlSessionFactory().openSession();
        SqlSession postgre = PostgreFactory.getSqlSessionFactory().openSession();

        try {
            ClientMapper cm = mysql.getMapper(ClientMapper.class);
            MigrationMapper mm = postgre.getMapper(MigrationMapper.class);
            myClients = cm.selectAll(new ClientEntry());

            for (ClientEntry ce : myClients) {
                i = ce.getUserId();
                mm.insertClient(ce);
            }

            postgre.commit();
        } catch (Exception ee) {
            ee.printStackTrace();
            System.out.println("Failed record num: " + i);
        } finally {
            mysql.close();
            postgre.close();
        }

        return new ForwardResolution("/WEB-INF/views/migrate.jsp");

    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors ve) throws Exception {
        return new ForwardResolution("/WEB-INF/views/special.jsp");
    }

    /**
     * @return the context
     */
    @Override
    public ActionBeanContext getContext() {
        return context;
    }

    /**
     * @param context the context to set
     */
    @Override
    public void setContext(ActionBeanContext context) {
        this.context = context;
    }

    /**
     * @return the xmlOut
     */
    public String getXmlOut() {
        return xmlOut;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @param xmlOut the xmlOut to set
     */
    public void setXmlOut(String xmlOut) {
        this.xmlOut = xmlOut;
    }

    /**
     * @return the regEntry
     */
    public UpdateEntry getRegEntry() {
        return regEntry;
    }
}
