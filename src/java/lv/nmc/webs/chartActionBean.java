/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.webs;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import lv.nmc.batis.mappers.GraphMapper;
import lv.nmc.dao.MainDbFactory;
import lv.nmc.entities.CourseEntry;
import lv.nmc.entities.CourseIncomeQuery;
import lv.nmc.entities.Outcome;
import lv.nmc.xext.XReader;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.SimpleError;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import org.apache.ibatis.session.SqlSession;
import org.joda.time.DateTime;

/**
 * Reflects lv.nmc.entities.StaffEntry
 *
 */
@UrlBinding("/ChartService.rs")
public class chartActionBean implements ActionBean, ValidationErrorHandler {

    private ActionBeanContext context;
    private String xmlOut = "";
    private Integer skip = 0;
    private List<Integer> courseIds = new ArrayList<Integer>();
    private String dateStart, dateEnd, domain = "";
    private String tiOut;
    private List<CourseEntry> clistLvl = new ArrayList<CourseEntry>();
    private String incomeType = "real";
    private HashMap<String, BigDecimal> sums = new HashMap<String, BigDecimal>();
    private HashMap<Integer, BigDecimal> iSums = new HashMap<Integer, BigDecimal>();

    

    @DefaultHandler
    @HandlesEvent("showIncome")
    public Resolution showIncomeGraph() {
        
        List<Integer> courseIdss = new ArrayList<Integer>();

        if (null == dateStart || null == dateEnd) {
            context.getValidationErrors().add("i1ncorrectDates", new SimpleError("Please, recheck Date Start or Date End"));
            return new ForwardResolution("/WEB-INF/views/messages/error.jsp");
        }

        CourseIncomeQuery cq = new CourseIncomeQuery();
        cq.setDateStart(dateStart);
        cq.setDateEnd(dateEnd);

        for (Integer ii : courseIds) {
            if (null != ii) {
                courseIdss.add(ii);
            }
        }

        cq.setCourseIds(courseIdss);

        DateTime dt = new DateTime();


        SqlSession session = MainDbFactory.getSqlSessionFactory().openSession();

        try {
            GraphMapper gm = session.getMapper(GraphMapper.class);
            if (incomeType.equals("real")) {
                clistLvl = gm.selectCourseIncome(cq);
            } else {
                clistLvl = gm.selectCourseIncomeFull(cq);
            }

        } finally {
            session.close();
        }

        sums = processGraphData();

        xmlOut = new XReader(HashMap.class).object2xml(sums);

        return new ForwardResolution("/WEB-INF/views/charts.jsp");
    }

    @HandlesEvent("showIncomeOutcome")
    public Resolution showIncomeOutcome() {
        realIncome();


        List<Outcome> outcomes = new ArrayList<Outcome>();
        CourseIncomeQuery cq = new CourseIncomeQuery();
        HashMap<String, BigDecimal> results = new HashMap<String, BigDecimal>();
        cq.setDateStart(dateStart);
        cq.setDateEnd(dateEnd);
        cq.setCourseIds(courseIds);

        SqlSession session = MainDbFactory.getSqlSessionFactory().openSession();

        try {
            GraphMapper gm = session.getMapper(GraphMapper.class);
            outcomes = gm.totalOutcome(cq);
        } finally {
            session.close();
        }

        for (Outcome o : outcomes) {
            if (results.containsKey(o.getCourseName())) {
                BigDecimal bd = results.get(o.getCourseName());
                bd.setScale(2, RoundingMode.HALF_UP);
                results.put(o.getCourseName(), bd.add(new BigDecimal(o.getDayOutcome())));
            } else {
                BigDecimal bd = new BigDecimal(o.getDayOutcome());
                bd.setScale(2, RoundingMode.HALF_UP);
                results.put(o.getCourseName(), bd);
            }
        }

        Set<String> ks = sums.keySet();

        for (String str : ks) {
            BigDecimal income = sums.get(str);
            BigDecimal outcome = results.get(str);


            income.setScale(2, RoundingMode.HALF_UP);

            outcome.setScale(2, RoundingMode.HALF_UP);
            results.put(str, income.subtract(outcome));
        }


        xmlOut = new XReader(HashMap.class).object2xml(results);

        return new ForwardResolution("/WEB-INF/views/charts.jsp");
    }

    @HandlesEvent("showIncomeOutcomeDpt")
    public Resolution showIncomeOutcomeDpt() {
        fullIncome();


        List<Outcome> outcomes = new ArrayList<Outcome>();
        CourseIncomeQuery cq = new CourseIncomeQuery();
        HashMap<String, BigDecimal> results = new HashMap<String, BigDecimal>();
        cq.setDateStart(dateStart);
        cq.setDateEnd(dateEnd);
        cq.setCourseIds(courseIds);

        SqlSession session = MainDbFactory.getSqlSessionFactory().openSession();

        try {
            GraphMapper gm = session.getMapper(GraphMapper.class);
            outcomes = gm.totalOutcomeDpt(cq);
        } finally {
            session.close();
        }

        for (Outcome o : outcomes) {
            if (results.containsKey(o.getCourseName())) {
                BigDecimal bd = results.get(o.getCourseName());
                bd.setScale(2, RoundingMode.HALF_UP);
                results.put(o.getCourseName(), bd.add(new BigDecimal(o.getDayOutcome())));
            } else {
                BigDecimal bd = new BigDecimal(o.getDayOutcome());
                bd.setScale(2, RoundingMode.HALF_UP);
                results.put(o.getCourseName(), bd);
            }
        }

        Set<String> ks = sums.keySet();

        for (String str : ks) {
            BigDecimal income = sums.get(str);
            BigDecimal outcome = results.get(str);


            income.setScale(2, RoundingMode.HALF_UP);

            outcome.setScale(2, RoundingMode.HALF_UP);
            results.put(str, income.subtract(outcome));
        }


        xmlOut = new XReader(HashMap.class).object2xml(results);

        return new ForwardResolution("/WEB-INF/views/charts.jsp");
    }

    /*  private void realIncomeDpt() {
    List<Integer> courseIdss = new ArrayList<Integer>();
    
    CourseIncomeQuery cq = new CourseIncomeQuery();
    cq.setDateStart(dateStart);
    cq.setDateEnd(dateEnd);
    
    for (Integer ii : courseIds) {
    if (null != ii) {
    courseIdss.add(ii);
    }
    }
    
    cq.setCourseIds(courseIdss);
    DateTime dt = new DateTime();
    SqlSession session = MainDbFactory.getSqlSessionFactory().openSession();
    
    try {
    GraphMapper gm = session.getMapper(GraphMapper.class);
    clistLvl = gm.selectCourseIncomeDpt(cq);
    } finally {
    session.close();
    }
    
    sums = processGraphData();  
    }*/
    private void realIncome() {

        List<Integer> courseIdss = new ArrayList<Integer>();

        CourseIncomeQuery cq = new CourseIncomeQuery();
        cq.setDateStart(dateStart);
        cq.setDateEnd(dateEnd);
        cq.setCourseIds(courseIds);

        SqlSession session = MainDbFactory.getSqlSessionFactory().openSession();

        try {
            GraphMapper gm = session.getMapper(GraphMapper.class);
            clistLvl = gm.selectCourseIncome(cq);
        } finally {
            session.close();
        }

        sums = processGraphData();
    }

    private void fullIncome() {

        List<Integer> courseIdss = new ArrayList<Integer>();

        CourseIncomeQuery cq = new CourseIncomeQuery();
        cq.setDateStart(dateStart);
        cq.setDateEnd(dateEnd);
        cq.setCourseIds(courseIds);

        SqlSession session = MainDbFactory.getSqlSessionFactory().openSession();

        try {
            GraphMapper gm = session.getMapper(GraphMapper.class);
            clistLvl = gm.selectCourseIncomeFull(cq);
        } finally {
            session.close();
        }

        sums = processGraphData();
    }

    private HashMap processGraphData() {
        sums.clear();
        String commentPrice = "";
        for (CourseEntry ci : clistLvl) {


            if (null == ci.getTjComment()) {
                ci.setTjComment("");

            }

            if (ci.getBillPrice().equals("---")) {
                continue;
            }

            if (!ci.getTjComment().trim().isEmpty()) {
                try {
                    commentPrice = ci.getTjComment().split("LVL")[0].trim().replace(",", ".");
                    Double pr = Double.parseDouble(commentPrice);
                    ci.setBillPrice(BigDecimal.valueOf(pr).setScale(2, RoundingMode.HALF_UP).toString());

                } catch (Exception eee) {
                    System.out.println(ci.getTjComment() + " " + ci.getPaymentId());
                    skip++;
                }
                continue;

            } else if (ci.getTjComment().trim().isEmpty() && "full".equals(incomeType)) {
                String prc = ci.getBillPrice().replace(",", ".");
                Double pr = Double.parseDouble("0.00");

                if (prc.contains("EUR")) {
                    prc = prc.replaceAll(" EUR", "");
                    BigDecimal eur = BigDecimal.valueOf(Double.parseDouble(prc));
                    eur = eur.multiply(BigDecimal.valueOf(Double.parseDouble("0.702804")));
                    ci.setBillPrice(eur.toString());
                } else if (prc.contains("USD")) {
                    prc = prc.replaceAll(" USD", "");
                    BigDecimal usd = BigDecimal.valueOf(Double.parseDouble(prc));
                    usd = usd.multiply(BigDecimal.valueOf(Double.parseDouble("0.572000")));
                    ci.setBillPrice(usd.toString());
                }

            } else if (ci.getTjComment().trim().isEmpty() && "real".equals(incomeType)) {
                String prc = ci.getBillPrice().replace(",", ".");
                Double pr = Double.parseDouble("0.00");

                if (prc.contains("EUR")) {
                    prc = prc.replaceAll(" EUR", "");
                    BigDecimal eur = BigDecimal.valueOf(Double.parseDouble(prc));
                    eur = eur.multiply(BigDecimal.valueOf(Double.parseDouble("0.702804")));
                    ci.setBillPrice(eur.toString());
                } else if (prc.contains("USD")) {
                    prc = prc.replaceAll(" USD", "");
                    BigDecimal usd = BigDecimal.valueOf(Double.parseDouble(prc));
                    usd = usd.multiply(BigDecimal.valueOf(Double.parseDouble("0.572000")));
                    ci.setBillPrice(usd.toString());
                } else {
                    ci.setBillPrice(ci.getBillPrice().replace(",", "."));
                }
                continue;
            }
        }


        for (CourseEntry ci : clistLvl) {
            if (ci.getBillPrice().equals("---")) {
                continue;
            }

            Double price = 0.00;
            try {
                price = Double.parseDouble(ci.getBillPrice().replaceAll(" LVL", "").replace(",", "."));
            } catch (Exception e) {
                System.out.println(ci.getTjComment() + " " + ci.getCourseName());
                skip++;
                continue;
            }


            if (sums.containsKey(ci.getCourseName())) {
                BigDecimal add = sums.get(ci.getCourseName()).add(BigDecimal.valueOf(price));
                sums.put(ci.getCourseName(), add);
                iSums.put(ci.getCourseId(), add);
            } else {
                sums.put(ci.getCourseName(), BigDecimal.valueOf(price).setScale(2, RoundingMode.HALF_UP));
                iSums.put(ci.getCourseId(), BigDecimal.valueOf(price).setScale(2, RoundingMode.HALF_UP));
            }



        }

        return sums;
    }

    private void realIncomeDpt() {

        List<Integer> courseIdss = new ArrayList<Integer>();

        CourseIncomeQuery cq = new CourseIncomeQuery();
        cq.setDateStart(dateStart);
        cq.setDateEnd(dateEnd);
        cq.setCourseIds(courseIds);

        SqlSession session = MainDbFactory.getSqlSessionFactory().openSession();

        try {
            GraphMapper gm = session.getMapper(GraphMapper.class);
            clistLvl = gm.selectCourseIncomeDpt(cq);
        } finally {
            session.close();
        }

        sums = processGraphDptData();



    }

    @HandlesEvent("showIncomeDpt")
    public Resolution showIncomeGraphDpt() {

        List<Integer> courseIdss = new ArrayList<Integer>();

        if (null == dateStart || null == dateEnd) {
            context.getValidationErrors().add("incorrectDates", new SimpleError("Please, recheck Date Start or Date End"));
            return new ForwardResolution("/WEB-INF/views/messages/error.jsp");
        }

        CourseIncomeQuery cq = new CourseIncomeQuery();
        cq.setDateStart(dateStart);
        cq.setDateEnd(dateEnd);

        for (Integer ii : courseIds) {
            if (null != ii) {
                courseIdss.add(ii);

            }
            cq.setCourseIds(courseIdss);
            DateTime dt = new DateTime();

            SqlSession session = MainDbFactory.getSqlSessionFactory().openSession();
            try {
                GraphMapper gm = session.getMapper(GraphMapper.class);
                if (incomeType.equals("real")) {
                    clistLvl = gm.selectCourseIncomeDpt(cq);
                } else {
                    clistLvl = gm.selectCourseIncomeFullDpt(cq);

                }
            } finally {
                session.close();
            }

            sums = processGraphDptData();
            xmlOut = new XReader(HashMap.class).object2xml(sums);
        }
        return new ForwardResolution("/WEB-INF/views/charts.jsp");
    }

    private HashMap processGraphDptData() {
        sums.clear();

        String commentPrice = "";
        for (CourseEntry ci : getClistLvl()) {

            if (null == ci.getTjComment()) {
                ci.setTjComment("");

            }

            if (ci.getBillPrice().equals("---")) {
                continue;
            }

            if (!ci.getTjComment().trim().isEmpty()) {
                try {
                    commentPrice = ci.getTjComment().replace(",", ".").split("LVL")[0].trim();
                    Double pr = Double.parseDouble(commentPrice);
                    ci.setBillPrice(BigDecimal.valueOf(pr).setScale(2, RoundingMode.HALF_UP).toString());

                } catch (Exception eee) {
                    System.out.println(eee.getMessage() + " " + commentPrice);
                }
                continue;

            } else if (ci.getTjComment().trim().isEmpty() && "full".equals(getIncomeType())) {
                String prc = ci.getBillPrice().replace(",", ".");
                Double pr = Double.parseDouble("0.00");

                if (prc.contains("EUR")) {
                    prc = prc.replaceAll(" EUR", "");
                    BigDecimal eur = BigDecimal.valueOf(Double.parseDouble(prc));
                    eur = eur.multiply(BigDecimal.valueOf(Double.parseDouble("0.702804")));
                    ci.setBillPrice(eur.toString());
                } else if (prc.contains("USD")) {
                    prc = prc.replaceAll(" USD", "");
                    BigDecimal usd = BigDecimal.valueOf(Double.parseDouble(prc));
                    usd = usd.multiply(BigDecimal.valueOf(Double.parseDouble("0.572000")));
                    ci.setBillPrice(usd.toString());
                }

            } else if (ci.getTjComment().trim().isEmpty() && "real".equals(getIncomeType())) {
                String prc = ci.getBillPrice().replace(",", ".");
                Double pr = Double.parseDouble("0.00");

                if (prc.contains("EUR")) {
                    prc = prc.replaceAll(" EUR", "");
                    BigDecimal eur = BigDecimal.valueOf(Double.parseDouble(prc));
                    eur = eur.multiply(BigDecimal.valueOf(Double.parseDouble("0.702804")));
                    ci.setBillPrice(eur.toString());
                } else if (prc.contains("USD")) {
                    prc = prc.replaceAll(" USD", "");
                    BigDecimal usd = BigDecimal.valueOf(Double.parseDouble(prc));
                    usd = usd.multiply(BigDecimal.valueOf(Double.parseDouble("0.572000")));
                    ci.setBillPrice(usd.toString());
                }
                continue;
            }
        }


        for (CourseEntry ci : getClistLvl()) {
            if (ci.getBillPrice().equals("---")) {
                continue;
            }

            Double price = 0.00;
            try {
                price = Double.parseDouble(ci.getBillPrice().replaceAll(" LVL", "").replace(",", "."));
            } catch (Exception e) {
                skip++;
                System.out.println(ci.getTjComment() + " " + ci.getPaymentId());
                continue;
            }


            if (getSums().containsKey(ci.getDepartment())) {
                BigDecimal add = getSums().get(ci.getDepartment()).add(BigDecimal.valueOf(price));
                getSums().put(ci.getDepartment(), add);
            } else {
                getSums().put(ci.getDepartment(), BigDecimal.valueOf(price).setScale(2, RoundingMode.HALF_UP));
            }



        }


        //     Set<String> keys = sums.keySet();

        /*     for (String key : keys) {
        dataset.addValue(sums.get(key).setScale(2, RoundingMode.HALF_UP), "Price, LVL", key);
        }*/

        return getSums();
    }

    @HandlesEvent("showOutcome")
    public Resolution showOutcome() {
        List<Outcome> outcomes = new ArrayList<Outcome>();
        CourseIncomeQuery cq = new CourseIncomeQuery();
        HashMap<String, BigDecimal> results = new HashMap<String, BigDecimal>();
        cq.setDateStart(dateStart);
        cq.setDateEnd(dateEnd);
        cq.setCourseIds(courseIds);

        SqlSession session = MainDbFactory.getSqlSessionFactory().openSession();

        try {
            GraphMapper gm = session.getMapper(GraphMapper.class);
            outcomes = gm.totalOutcome(cq);
        } finally {
            session.close();
        }

        for (Outcome o : outcomes) {
            if (results.containsKey(o.getCourseName())) {
                BigDecimal bd = results.get(o.getCourseName());
                bd.setScale(2, RoundingMode.HALF_UP);
                results.put(o.getCourseName(), bd.add(new BigDecimal(o.getDayOutcome())));
            } else {
                BigDecimal bd = new BigDecimal(o.getDayOutcome());
                bd.setScale(2, RoundingMode.HALF_UP);
                results.put(o.getCourseName(), bd);
            }
        }
        xmlOut = new XReader(HashMap.class).object2xml(results);
        return new ForwardResolution("/WEB-INF/views/charts.jsp");
    }

    @HandlesEvent("showOutcomeDpt")
    public Resolution showOutcomeDpt() {
        List<Outcome> outcomes = new ArrayList<Outcome>();
        CourseIncomeQuery cq = new CourseIncomeQuery();
        HashMap<String, BigDecimal> results = new HashMap<String, BigDecimal>();
        cq.setDateStart(dateStart);
        cq.setDateEnd(dateEnd);
        cq.setCourseIds(courseIds);

        SqlSession session = MainDbFactory.getSqlSessionFactory().openSession();

        try {
            GraphMapper gm = session.getMapper(GraphMapper.class);
            outcomes = gm.totalOutcomeDpt(cq);
        } finally {
            session.close();
        }

        for (Outcome o : outcomes) {
            if (results.containsKey(o.getCourseName())) {
                BigDecimal bd = results.get(o.getCourseName());
                bd.setScale(2, RoundingMode.HALF_UP);
                results.put(o.getCourseName(), bd.add(new BigDecimal(o.getDayOutcome())));
            } else {
                BigDecimal bd = new BigDecimal(o.getDayOutcome());
                bd.setScale(2, RoundingMode.HALF_UP);
                results.put(o.getCourseName(), bd);
            }
        }
        xmlOut = new XReader(HashMap.class).object2xml(results);
        return new ForwardResolution("/WEB-INF/views/charts.jsp");
    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors ve) throws Exception {
        return new ForwardResolution("/WEB-INF/views/special.jsp");
    }

    /**
     * @return the context
     */
    @Override
    public ActionBeanContext getContext() {
        return context;
    }

    /**
     * @param context the context to set
     */
    @Override
    public void setContext(ActionBeanContext context) {
        this.context = context;
    }

    /**
     * @return the xmlOut
     */
    public String getXmlOut() {
        return xmlOut;
    }

    /**
     * @param xmlOut the xmlOut to set
     */
    public void setXmlOut(String xmlOut) {
        this.xmlOut = xmlOut;
    }

    /**
     * @return the skip
     */
    public Integer getSkip() {
        return skip;
    }

    /**
     * @return the courseIds
     */
    public List<Integer> getCourseIds() {
        return courseIds;
    }

    /**
     * @param courseIds the courseIds to set
     */
    public void setCourseIds(List<Integer> courseIds) {
        this.courseIds = courseIds;
    }

    /**
     * @return the dateStart
     */
    public String getDateStart() {
        return dateStart;
    }

    /**
     * @param dateStart the dateStart to set
     */
    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    /**
     * @return the dateEnd
     */
    public String getDateEnd() {
        return dateEnd;
    }

    /**
     * @param dateEnd the dateEnd to set
     */
    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    /**
     * @return the clistLvl
     */
    public List<CourseEntry> getClistLvl() {
        return clistLvl;
    }

    /**
     * @param clistLvl the clistLvl to set
     */
    public void setClistLvl(List<CourseEntry> clistLvl) {
        this.clistLvl = clistLvl;
    }

    /**
     * @return the incomeType
     */
    public String getIncomeType() {
        return incomeType;
    }

    /**
     * @param incomeType the incomeType to set
     */
    public void setIncomeType(String incomeType) {
        this.incomeType = incomeType;
    }

    /**
     * @return the sums
     */
    public HashMap<String, BigDecimal> getSums() {
        return sums;
    }

    /**
     * @param sums the sums to set
     */
    public void setSums(HashMap<String, BigDecimal> sums) {
        this.sums = sums;
    }
}
