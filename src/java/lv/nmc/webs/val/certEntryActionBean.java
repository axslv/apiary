/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.webs.val;

import java.util.ArrayList;
import java.util.List;
import lv.nmc.batis.mappers.n3.CertMapper;
import lv.nmc.dao.MainDbFactory;
import lv.nmc.entities.CertEntry;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import org.apache.ibatis.session.SqlSession;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

@UrlBinding("/validate/index.html")
public class certEntryActionBean implements ActionBean, ValidationErrorHandler {

    private ActionBeanContext context;
    private String certNr="00-000/00", dateEnd="01/01/1970", birthDate;
    private List<CertEntry> clist = new ArrayList<CertEntry>();
    private String certState = "NOT Confirmed";
    private CertEntry cert = new CertEntry();
    private boolean re = false;
    private DateTimeFormatter userDate = DateTimeFormat.forPattern("d/M/y");
    private DateTimeFormatter sqlDate = DateTimeFormat.forPattern("y-M-d");

    @DefaultHandler
    @HandlesEvent("start")
    public Resolution show() {

        return new ForwardResolution("/WEB-INF/views/validation/index.jsp");
    }

    @HandlesEvent("checkCert")
    public Resolution checkCertificate() {
        
        if (null == dateEnd) {
            return new ForwardResolution("/WEB-INF/views/validation/status.jsp");
        }
        
        SqlSession session = MainDbFactory.getSqlSessionFactory().openSession();
        DateTime dt = userDate.parseDateTime(dateEnd);
        dateEnd = dt.toString(sqlDate);

        try {
            CertMapper cm = session.getMapper(CertMapper.class);
            CertEntry ce = new CertEntry();
            ce.setCertNr(certNr);
            ce.setIssueDate(dateEnd);
            clist = cm.checkCert(ce);

            if (!clist.isEmpty()) {
                certState = "Confirmed";
                cert = clist.get(0);
                processBd();

            } else if (clist.isEmpty()) {
                clist = cm.checkReissue(ce);

                if (!clist.isEmpty()) {
                    certState = "Confirmed";
                    cert = clist.get(0);
                    re = true;
                    processBd();
                }
            }

        } finally {
            session.close();
        }

        if (re) {
            return new ForwardResolution("/WEB-INF/views/validation/statusRe.jsp");
        }

        return new ForwardResolution("/WEB-INF/views/validation/status.jsp");
    }

    private void processBd() {
        DateTimeFormatter bdate = DateTimeFormat.forPattern("ddMMyy");
       // System.out.println(clist.get(0));
        String[] bds = clist.get(0).getPersonCode().split("-");
        try {            
            birthDate = bdate.parseDateTime(bds[0]).toString(userDate);
        } catch (IllegalArgumentException ee) {
            birthDate = bds[0];
        }

    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors ve) throws Exception {
        return new ForwardResolution("/WEB-INF/views/special.jsp");
    }

    /**
     * @return the context
     */
    @Override
    public ActionBeanContext getContext() {
        return context;
    }

    /**
     * @param context the context to set
     */
    @Override
    public void setContext(ActionBeanContext context) {
        this.context = context;
    }

    /**
     * @return the certNr
     */
    public String getCertNr() {
        return certNr;
    }

    /**
     * @param certNr the certNr to set
     */
    public void setCertNr(String certNr) {
        this.certNr = certNr;
    }

    /**
     * @return the dateEnd
     */
    public String getDateEnd() {
        return dateEnd;
    }

    /**
     * @param dateEnd the dateEnd to set
     */
    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    /**
     * @return the certState
     */
    public String getCertState() {
        return certState;
    }

    /**
     * @return the cert
     */
    public CertEntry getCert() {
        return cert;
    }

    /**
     * @return the birthDate
     */
    public String getBirthDate() {
        return birthDate;
    }

}
