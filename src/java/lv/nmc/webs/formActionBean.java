/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.webs;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lv.nmc.batis.mappers.form.FormMapper;
import lv.nmc.dao.FormDao;
import lv.nmc.dao.MainDbFactory;

import lv.nmc.entities.form.FormData;
import lv.nmc.xext.XReader;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.SimpleError;
import net.sourceforge.stripes.validation.Validate;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.ibatis.session.SqlSession;

/**
 * Reflects lv.nmc.entities.StaffEntry
 *
 */
@UrlBinding("/AppaisalForm/{registrationId}.html")   ///AppaisalForm/{registrationId}.html

public class formActionBean implements ActionBean {

    private ActionBeanContext context;
    @Validate(required=true)
    private Integer registrationId;
    private List<FormData> fd = new ArrayList<FormData>();
    private String event= "noEvent", xmlOut;
    private FormData courseEntry = new FormData();
    private List<Integer> integers = new ArrayList<>();
    private String json = "";
    private String questionIds, answerIds, answerText;
    private List<String> answers, questions;
    
    


    @DefaultHandler
    @HandlesEvent("view")
    public Resolution executeSelect() {   
        
        
        prepareBean();
        FormDao sd = new FormDao();
        
        if (getEvent().contains("_update")) {
            sd.executeUpdate(getEvent(), courseEntry);
        } else if (getEvent().contains("_delete")) {
            sd.executeUpdate(getEvent(), courseEntry);
        } else {
            sd.executeSelect(getEvent(), courseEntry);
        }

        setXmlOut(new XReader(FormData.class, "json").object2xml(sd.getResults()));
        return new ForwardResolution("/WEB-INF/views/form/appaisal.jsp");
    }
    
    @HandlesEvent("insertAnswers")
    public Resolution insertAnswers() {
      
            
            questions = Arrays.asList(getQuestionIds().split(","));
            answers = Arrays.asList(getAnswerIds().split(","));
            
            if (answers.size() != questions.size()) {
                context.getValidationErrors().add("sizeMismatch", new SimpleError("List mismatch"));
                return new ForwardResolution("/WEB-INF/views/form/appaisal.jsp");
            }
            
            

            
            for (int i =0; i<=questions.size()-1; i++) {
                FormData ff = new FormData();
                ff.setQuestionId(Integer.parseInt(questions.get(i)));
                ff.setAnswerId(Integer.parseInt(answers.get(i)));
                ff.setRegistrationId(getRegistrationId());    
                ff.setAnswerText(getAnswerText());
                fd.add(ff);                
            }
            
            SqlSession ss = MainDbFactory.getSqlSessionFactory().openSession();
            
            try {
                FormMapper fm = ss.getMapper(FormMapper.class);
                fm.insertAnswers(fd);
                
                if (null != answerText) {
                    FormData ff = new FormData();
                    ff.setRegistrationId(registrationId);
                    ff.setAnswerText(answerText);
                    fm.insertText(ff);
                }
                
                ss.commit();
            } finally {
                ss.close();
            }
            return new ForwardResolution("/WEB-INF/views/form/appaisal.jsp");
            
        
    }
    
    
        protected void prepareBean() {        
        if (!"view".equals(context.getEventName())) {
            return;
        }
        
        try {
            BeanUtils.populate(courseEntry, context.getRequest().getParameterMap());
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc!");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        }
    }
        
 
    /**
     * @return the context
     */
    @Override
    public ActionBeanContext getContext() {
        return context;
    }

    /**
     * @param context the context to set
     */
    @Override
    public void setContext(ActionBeanContext context) {
        this.context = context;
    }

    /**
     * @return the registrationId
     */
    public Integer getRegistrationId() {
        return registrationId;
    }

    /**
     * @param registrationId the registrationId to set
     */
    public void setRegistrationId(Integer registrationId) {
        this.registrationId = registrationId;
    }

    /**
     * @return the fd
     */
    public List<FormData> getFd() {
        return fd;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @return the xmlOut
     */
    public String getXmlOut() {
        return xmlOut;
    }

    /**
     * @param xmlOut the xmlOut to set
     */
    public void setXmlOut(String xmlOut) {
        this.xmlOut = xmlOut;
    }

    /**
     * @return the integers
     */
    public List<Integer> getIntegers() {
        return integers;
    }

    /**
     * @return the json
     */
    public String getJson() {
        return json;
    }

    /**
     * @param json the json to set
     */
    public void setJson(String json) {
        this.json = json;
    }

    /**
     * @return the questionIds
     */
    public String getQuestionIds() {
        return questionIds;
    }

    /**
     * @param questionIds the questionIds to set
     */
    public void setQuestionIds(String questionIds) {
        this.questionIds = questionIds;
    }

    /**
     * @return the answerIds
     */
    public String getAnswerIds() {
        return answerIds;
    }

    /**
     * @param answerIds the answerIds to set
     */
    public void setAnswerIds(String answerIds) {
        this.answerIds = answerIds;
    }

    /**
     * @return the answerText
     */
    public String getAnswerText() {
        return answerText;
    }

    /**
     * @param answerText the answerText to set
     */
    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

   

   

}
