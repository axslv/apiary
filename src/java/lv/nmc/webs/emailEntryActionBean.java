/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.webs;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.PasswordAuthentication;
import lv.nmc.entities.n3.Email;
import lv.nmc.xext.SendMail;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

/**
 * Reflects lv.nmc.entities.StaffEntry
 *
 */
@UrlBinding("/mail/service.html")
public class emailEntryActionBean implements ActionBean, ValidationErrorHandler {

    private ActionBeanContext context;
    private String xmlOut = "";
    private String event = "noEvent";
    private String emailSubject, emailText, sendTo, sendFrom = "robots@novikontas.lv", hostname = "novikontas.lv";

    @DefaultHandler
    @HandlesEvent("sendMail")
    public Resolution sendMail() {

        try {
            performSend();
            XStream xs = new XStream(new StaxDriver());
            xs.processAnnotations(Email.class);
            this.xmlOut = xs.toXML(new Email());
            //context.getMessages().add(new SimpleMessage("Message sent"));
        } catch (Exception ex) {
            Logger.getLogger(emailEntryActionBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return new ForwardResolution("/WEB-INF/views/mailEntry.jsp");

    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors ve) throws Exception {
        return new ForwardResolution("/WEB-INF/views/special.jsp");
    }

    private void performSend() throws Exception {

        SendMail sm = new SendMail(sendTo, emailText);
        sm.setMailFrom("training@novikontas.lv");
        sm.setSubject(emailSubject);
        sm.sendAuth();
        
    }

    /**
     * @return the context
     */
    @Override
    public ActionBeanContext getContext() {
        return context;
    }

    /**
     * @param context the context to set
     */
    @Override
    public void setContext(ActionBeanContext context) {
        this.context = context;
    }

    /**
     * @return the xmlOut
     */
    public String getXmlOut() {
        return xmlOut;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @param xmlOut the xmlOut to set
     */
    public void setXmlOut(String xmlOut) {
        this.xmlOut = xmlOut;
    }

    /**
     * @return the emailSubject
     */
    public String getEmailSubject() {
        return emailSubject;
    }

    /**
     * @param emailSubject the emailSubject to set
     */
    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    /**
     * @return the emailText
     */
    public String getEmailText() {
        return emailText;
    }

    /**
     * @param emailText the emailText to set
     */
    public void setEmailText(String emailText) {
        this.emailText = emailText;
    }

    /**
     * @return the sendTo
     */
    public String getSendTo() {
        return sendTo;
    }

    /**
     * @param sendTo the sendTo to set
     */
    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }

    /**
     * @return the sendFrom
     */
    public String getSendFrom() {
        return sendFrom;
    }

    /**
     * @param sendFrom the sendFrom to set
     */
    public void setSendFrom(String sendFrom) {
        this.sendFrom = sendFrom;
    }

    /**
     * @return the hostname
     */
    public String getHostname() {
        return hostname;
    }

    /**
     * @param hostname the hostname to set
     */
    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    private class SMTPAuthenticator extends javax.mail.Authenticator {

        private String hostname, username, password;

        public SMTPAuthenticator() {

            this.username = "robots@novikontas.lv";
            this.password = "808080cbabaa";
            this.hostname = "novikontas.lv";

        }

        @Override
        public PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(this.username, this.password);
        }

        /**
         * @return the hostname
         */
        public String getHostname() {
            return hostname;
        }

        /**
         * @param hostname the hostname to set
         */
        public void setHostname(String hostname) {
            this.hostname = hostname;
        }
    }
}
