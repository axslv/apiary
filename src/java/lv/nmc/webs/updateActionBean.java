/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.webs;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Set;
import lv.nmc.dao.UpdateDao;
import lv.nmc.entities.UpdateEntry;
import lv.nmc.xext.XReader;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import org.apache.commons.beanutils.BeanUtils;

/**
 * Reflects lv.nmc.entities.StaffEntry
 * 
 */
@UrlBinding("/UpdateService.xml")
public class updateActionBean implements ActionBean, ValidationErrorHandler {

    private ActionBeanContext context;
    private String xmlOut = "";
    private String event = "noEvent";
    private UpdateEntry regEntry = new UpdateEntry();

    @DefaultHandler
    @HandlesEvent("dbRun")
    public Resolution executeSelect() {
        prepareBean();
        UpdateDao sd = new UpdateDao();
        if (event.contains("_update")) {
            sd.executeUpdate(event, getRegEntry());
        } else if (event.contains("_delete")) {
            sd.executeUpdate(event, getRegEntry());
        } else {
            sd.executeSelect(event, getRegEntry());
        }

        setXmlOut(new XReader(UpdateEntry.class).object2xml(sd.getResults()));
        return new ForwardResolution("/WEB-INF/views/updateEntry.jsp");

    }

    protected void prepareBean() {

        HashMap<String, String> filteredParams = new HashMap<String, String>();
        Set<String> params = context.getRequest().getParameterMap().keySet();


        for (String param : params) {

            filteredParams.put(param, context.getRequest().getParameter(param));
        }

        try {
            BeanUtils.populate(getRegEntry(), filteredParams);
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc!");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        }



    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors ve) throws Exception {
        return new ForwardResolution("/WEB-INF/views/special.jsp");
    }

    /**
     * @return the context
     */
    @Override
    public ActionBeanContext getContext() {
        return context;
    }

    /**
     * @param context the context to set
     */
    @Override
    public void setContext(ActionBeanContext context) {
        this.context = context;
    }

    /**
     * @return the xmlOut
     */
    public String getXmlOut() {
        return xmlOut;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @param xmlOut the xmlOut to set
     */
    public void setXmlOut(String xmlOut) {
        this.xmlOut = xmlOut;
    }

    /**
     * @return the regEntry
     */
    public UpdateEntry getRegEntry() {
        return regEntry;
    }
}
