/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.webs;

import java.lang.reflect.InvocationTargetException;
import lv.nmc.dao.StaffDao;
import lv.nmc.entities.StaffEntry;
import lv.nmc.xext.XReader;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import org.apache.commons.beanutils.BeanUtils;

/**
 * Reflects lv.nmc.entities.StaffEntry
 * 
 */
@UrlBinding("/StaffService.rs")
public class staffEntryActionBean implements ActionBean, ValidationErrorHandler {

    private ActionBeanContext context;
    private String xmlOut = "";
    private StaffEntry staffEntry = new StaffEntry();    
    
    private Integer staffId;
    
    @Validate(required = true)
    private String event = "noEvent";
    
      
    

    @DefaultHandler
    @HandlesEvent("dbRun")
    public Resolution executeSelect() {      
        
        prepareBean();
        StaffDao sd = new StaffDao();
        
        if (event.contains("_update")) {
            sd.executeUpdate(event, staffEntry);
        } else if (event.contains("_delete")) {
            sd.executeUpdate(event, staffEntry);
        } else {
            sd.executeSelect(event, staffEntry);
        }
   
        xmlOut = (new XReader(StaffEntry.class).object2xml(sd.getResults()));
        return new ForwardResolution("/WEB-INF/views/special.jsp");

    }

    protected void prepareBean() {
        try {
            BeanUtils.populate(staffEntry, context.getRequest().getParameterMap());
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal access exc!");
        } catch (InvocationTargetException ex) {
            System.out.println("Invocation target exc");
            ex.printStackTrace();
        }

    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors ve) throws Exception {
        return new ForwardResolution("/WEB-INF/views/special.jsp");
    }

    /**
     * @return the context
     */
    @Override
    public ActionBeanContext getContext() {
        return context;
    }

    /**
     * @param context the context to set
     */
    @Override
    public void setContext(ActionBeanContext context) {
        this.context = context;
    }

    /**
     * @return the xmlOut
     */
    public String getXmlOut() {
        return xmlOut;
    }

    /**
     * @return the staffEntry
     */
    public StaffEntry getStaffEntry() {
        return staffEntry;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @return the staffId
     */
    public Integer getStaffId() {
        return staffId;
    }

    /**
     * @param staffId the staffId to set
     */
    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

  




}
