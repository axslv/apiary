/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.util;

import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.Authenticator;

import java.util.Properties;
import java.util.ResourceBundle;

/**
 *
 * @author emma
 */
public class SendMail {

    private String smtpHost = "";
    private String smtpUser = "";
    private String smtpPassword = "";
    private String mailFrom;
    private String sendTo = "", replyTo="training@novikontas.lv";
    private String msg = "";
    private String subject = "";
    private Integer rowId, uid;
    private String email = "";
    private ResourceBundle rb = ResourceBundle.getBundle("resources.settings");

    public SendMail(String sendTo, String msg) {
        this.mailFrom = rb.getString("mailFrom");
        this.sendTo = sendTo;
        this.msg = msg;
        this.smtpHost = rb.getString("smtpHost");
        this.smtpUser = rb.getString("smtpUser");
        this.smtpPassword = rb.getString("smtpPassword");
        this.subject = rb.getString("defaultEmailSubject");

    }

    public void sendEmail() {
        Boolean test = Boolean.valueOf(rb.getObject("smtpAuth").toString());
        if (test.booleanValue() == false) {
            try {
                send();
                System.out.println("email sent");
            } catch (Exception f) {
                System.out.println("Email sending failed");
            }
        } else {
            try {
                sendAuth();
                System.out.println("email sent");
            } catch (Exception f) {
                System.out.println("Email sending failed");
            }
        }
    }

    public void sendAuth() throws Exception {
        //  System.out.println("sending auth email");
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", getSmtpHost());
        props.put("mail.smtp.auth", "true");

        Authenticator auth = new SMTPAuthenticator(this.smtpUser, this.smtpPassword);

        Session mailSession = Session.getInstance(props, auth);
        // uncomment for debugging infos to stdout
        // mailSession.setDebug(true);
        Transport transport = mailSession.getTransport();

        MimeMessage message = new MimeMessage(mailSession);
        message.setContent(getMsg(), "text/html; charset=UTF-8");
        message.setHeader("Content-Type", "text/html; charset=UTF-8");
        
        message.setSubject(this.subject);
        message.setFrom(new InternetAddress(getMailFrom()));

        message.setReplyTo(new javax.mail.Address[]{
            new javax.mail.internet.InternetAddress(replyTo)
        });

        message.addRecipient(Message.RecipientType.TO,
                new InternetAddress(this.sendTo));

        transport.connect();
        transport.sendMessage(message,
                message.getRecipients(Message.RecipientType.TO));
        transport.close();
    }

    public void send() throws Exception {
        // System.out.println("Sending free email");
        Properties props = new Properties();
        props.setProperty("mail.smtp.host", "" + this.smtpHost);
        Session session = Session.getDefaultInstance(props, null);
        Message message = new MimeMessage(session);
        message.setContent(getMsg(), "text/html");
        message.setFrom(new InternetAddress(mailFrom));
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(this.sendTo));
        message.setSubject(subject);
        message.setText(this.msg);
        Transport.send(message);
    }

    /**
     * @return the smtpHost
     */
    public String getSmtpHost() {
        return smtpHost;
    }

    /**
     * @param smtpHost the smtpHost to set
     */
    public void setSmtpHost(String smtpHost) {
        this.smtpHost = smtpHost;
    }

    /**
     * @return the smtpUser
     */
    public String getSmtpUser() {
        return smtpUser;
    }

    /**
     * @param smtpUser the smtpUser to set
     */
    public void setSmtpUser(String smtpUser) {
        this.smtpUser = smtpUser;
    }

    /**
     * @return the smtpPassword
     */
    public String getSmtpPassword() {
        return smtpPassword;
    }

    /**
     * @param smtpPassword the smtpPassword to set
     */
    public void setSmtpPassword(String smtpPassword) {
        this.smtpPassword = smtpPassword;
    }

    /**
     * @return the mailFrom
     */
    public String getMailFrom() {
        return mailFrom;
    }

    /**
     * @param mailFrom the mailFrom to set
     */
    public void setMailFrom(String mailFrom) {
        this.mailFrom = mailFrom;
    }

    /**
     * @return the sendTo
     */
    public String getSendTo() {
        return sendTo;
    }

    /**
     * @param sendTo the sendTo to set
     */
    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the rowId
     */
    public Integer getRowId() {
        return rowId;
    }

    /**
     * @param rowId the rowId to set
     */
    public void setRowId(Integer rowId) {
        this.rowId = rowId;
    }

    /**
     * @return the uid
     */
    public Integer getUid() {
        return uid;
    }

    /**
     * @param uid the uid to set
     */
    public void setUid(Integer uid) {
        this.uid = uid;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the replyTo
     */
    public String getReplyTo() {
        return replyTo;
    }

    /**
     * @param replyTo the replyTo to set
     */
    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }
}
