/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.util;

import java.util.ArrayList;
import java.util.List;
import lv.nmc.entities.n3.col.Semester;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class SemesterFabric {
    
    public static Semester addSemester(String dateStart, String dateEnd) {        
        return new Semester(dateStart, dateEnd);
    }
    
    public static List<Semester> generateSemesters(String dateStart, int cnt) {
        List<Semester> semesters = new ArrayList<>();
        
        DateTimeFormatter sqlDate = DateTimeFormat.forPattern("YYYY-MM-dd");
        DateTime dt = sqlDate.parseDateTime(dateStart);
        
        
        for (int i=0; i<=5; i++) {            
            Semester sema = new Semester(dt.toString(sqlDate), dt.plusMonths(2).toString(sqlDate));
            semesters.add(sema);
            dt = dt.plusMonths(2);
        }       
        return semesters;
    }
    
    
    
    
    
}
